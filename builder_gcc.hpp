/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#ifndef C4S_BUILDER_GCC_HPP
#define C4S_BUILDER_GCC_HPP

namespace c4s {

//! Builder for g++ in gcc
class builder_gcc : public builder
{
public:
    builder_gcc(const ntbs& name, BUILD_TYPE type, FILE* log)
        : builder(name, type, "g++", "g++", log)
    {
    }

    void set_debug_params(const char* params = nullptr);

    BUILD_STATUS build();
};

} // namespace c4s

#endif