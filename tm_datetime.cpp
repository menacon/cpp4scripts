/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#include <cstdio>
#include <cstring>
#include <ctime>

#include "ntbs.hpp"
#include "logger.hpp"
#include "tm_datetime.hpp"

using namespace std;

namespace c4s {

tm_datetime::tm_datetime(int year, int month)
{
    clear();
    dtv.tm_year = year - 1900;
    dtv.tm_mon = (month > 0 && month < 13) ? month - 1 : 1;
    dtv.tm_mday = 1;
}
bool tm_datetime::parse(const char* str, const char* lang)
{
    char* rv;
    clear();
    if (!str || str[0] == 0 || !strncmp("null", str, 4))
        return true;
    if (lang == nullptr || !strncmp(lang, "iso", 3))
        rv = strptime(str, "%Y-%m-%d", &dtv);
    else if (!strncmp(lang, "fi", 2))
        rv = strptime(str, "%d.%m.%Y", &dtv);
    else // "en" is default
        rv = strptime(str, "%m/%d/%Y", &dtv);
    if (rv)
        return true;
    return false;
}
bool tm_datetime::parseISO(const char* str)
{
    if (strlen(str) > 11) {
        if (strptime(str, "%Y-%m-%dT%H:%M:%S", &dtv))
            return true;
    } else if (strptime(str, "%Y-%m-%d", &dtv))
        return true;
    return false;
}

bool tm_datetime::parseSql(const char* result)
{
    char* dummy;

    clear();
    unsigned int len = strlen(result);
    if (len >= 10) {
        dtv.tm_year = strtol(result, &dummy, 10) - 1900;
        dtv.tm_mon = strtol(result + 5, &dummy, 10) - 1;
        dtv.tm_mday = strtol(result + 8, &dummy, 10);
        if (len >= 19) {
            dtv.tm_hour = strtol(result + 11, &dummy, 10);
            dtv.tm_min = strtol(result + 14, &dummy, 10);
            dtv.tm_sec = strtol(result + 17, &dummy, 10);
            dtv.tm_isdst = -1;
        }
        return true;
    }
    CS_PRINT_NOTE("tm_datetime::parseSql - Incorrect date format.");
    return false;
}

// -------------------------------------------------------------------------------------------------
void tm_datetime::setDay1()
{
    dtv.tm_mday = 1;
    dtv.tm_hour = 0;
    dtv.tm_min = 0;
    dtv.tm_sec = 0;
}

void tm_datetime::setLastDay()
{
    dtv.tm_mon++;
    dtv.tm_mday = 0;
    std::mktime(&dtv);
}

void tm_datetime::now()
{
    time_t now = time(0);
    localtime_r(&now, &dtv);
}
void tm_datetime::thisMonth()
{
    now();
    setDay1();
}
void tm_datetime::nextMonth(int m_offset)
{
    dtv.tm_mon += m_offset;
    std::mktime(&dtv);
}
// -------------------------------------------------------------------------------------------------
int tm_datetime::compare(tm_datetime& tg)
{
    if (dtv.tm_year == 0 && tg.dtv.tm_year == 0)
        return 0;
    if (dtv.tm_year == 0)
        return 1;
    if (tg.dtv.tm_year == 0)
        return -1;

    time_t origin = mktime(&dtv);
    time_t target = mktime(&tg.dtv);
    if (origin > target)
        return 1;
    else if (origin < target)
        return -1;
    return 0;
}
// -------------------------------------------------------------------------------------------------
int tm_datetime::compare(time_t orig)
{
    if (dtv.tm_year == 0)
        return 1;
    time_t myt = mktime(&dtv);
    if (myt < orig)
        return -1;
    if (myt > orig)
        return 1;
    return 0;
}
// -------------------------------------------------------------------------------------------------
const char* tm_datetime::print(const char* lang)
{
    tstr[0] = 0;
    if (dtv.tm_year == 0) {
        return tstr;
    }
    if (lang == nullptr || !strncmp(lang, "iso", 3))
        snprintf(
            tstr, sizeof(tstr), "%d-%02d-%02d", dtv.tm_year + 1900, dtv.tm_mon + 1, dtv.tm_mday);
    else if (!strncmp(lang, "fi", 2))
        snprintf(
            tstr, sizeof(tstr), "%02d.%02d.%d", dtv.tm_mday, dtv.tm_mon + 1, dtv.tm_year + 1900);
    else // "en" is default
        snprintf(
            tstr, sizeof(tstr), "%02d/%02d/%d", dtv.tm_mon + 1, dtv.tm_mday, dtv.tm_year + 1900);
    return tstr;
}
// -------------------------------------------------------------------------------------------------
const char* tm_datetime::printShort(const char* lang)
{
    tstr[0] = 0;
    if (dtv.tm_year == 0)
        return tstr;
    if (lang == nullptr || !strncmp(lang, "iso", 3))
        snprintf(tstr, sizeof(tstr), "%02d-%02d", dtv.tm_mon + 1, dtv.tm_mday);
    else if (!strncmp(lang, "fi", 2))
        snprintf(tstr, sizeof(tstr), "%02d.%02d.", dtv.tm_mday, dtv.tm_mon + 1);
    else // "en" is default
        snprintf(tstr, sizeof(tstr), "%02d/%02d", dtv.tm_mon + 1, dtv.tm_mday);
    return tstr;
}
// -------------------------------------------------------------------------------------------------
const char* tm_datetime::printTime(const char* lang)
{
    tstr[0] = 0;
    if (dtv.tm_year == 0) {
        return tstr;
    }
    if (lang == nullptr || !strncmp(lang, "iso", 3))
        snprintf(tstr,
                 sizeof(tstr),
                 "%d-%02d-%02d %02d:%02d:%02d",
                 dtv.tm_year + 1900,
                 dtv.tm_mon + 1,
                 dtv.tm_mday,
                 dtv.tm_hour,
                 dtv.tm_min,
                 dtv.tm_sec);
    else if (!strncmp(lang, "fi", 2))
        snprintf(tstr,
                 sizeof(tstr),
                 "%02d.%02d.%d %02d:%02d:%02d",
                 dtv.tm_mday,
                 dtv.tm_mon + 1,
                 dtv.tm_year + 1900,
                 dtv.tm_hour,
                 dtv.tm_min,
                 dtv.tm_sec);
    else // "en" is default
        snprintf(tstr,
                 sizeof(tstr),
                 "%02d/%02d/%d %02d:%02d:%02d",
                 dtv.tm_mon + 1,
                 dtv.tm_mday,
                 dtv.tm_year + 1900,
                 dtv.tm_hour,
                 dtv.tm_min,
                 dtv.tm_sec);

    return tstr;
}
// -------------------------------------------------------------------------------------------------
const char* tm_datetime::printTimeShort(const char* lang)
{
    tstr[0] = 0;
    if (dtv.tm_year == 0) {
        return tstr;
    }
    if (lang == nullptr || !strncmp(lang, "iso", 3))
        snprintf(tstr,
                 sizeof(tstr),
                 "%02d-%02d %02d:%02d",
                 dtv.tm_mon + 1,
                 dtv.tm_mday,
                 dtv.tm_hour,
                 dtv.tm_min);
    else if (!strncmp(lang, "fi", 2))
        snprintf(tstr,
                 sizeof(tstr),
                 "%02d.%02d. %02d:%02d",
                 dtv.tm_mday,
                 dtv.tm_mon + 1,
                 dtv.tm_hour,
                 dtv.tm_min);
    else // "en" is default
        snprintf(tstr,
                 sizeof(tstr),
                 "%02d/%02d %02d:%02d",
                 dtv.tm_mon + 1,
                 dtv.tm_mday,
                 dtv.tm_hour,
                 dtv.tm_min);
    return tstr;
}
// -------------------------------------------------------------------------------------------------
const char* tm_datetime::printHourMinutes()
{
    snprintf(tstr, sizeof(tstr), "%02d:%02d", dtv.tm_hour, dtv.tm_min);
    return tstr;
}
// -------------------------------------------------------------------------------------------------
const char* tm_datetime::printYear()
{
    if (dtv.tm_year != 0)
        snprintf(tstr, sizeof(tstr), "%4d", dtv.tm_year + 1900);
    else
        tstr[0] = 0;
    return tstr;
}
// -------------------------------------------------------------------------------------------------
const char* tm_datetime::printISODate()
{
    snprintf(tstr, sizeof(tstr), "%4d-%02d-%02d", dtv.tm_year + 1900, dtv.tm_mon + 1, dtv.tm_mday);
    return tstr;
}
const char* tm_datetime::printDBDate()
{
    if (dtv.tm_mday == 0) strcpy(tstr, "null");
    else snprintf(tstr, sizeof(tstr), "'%4d-%02d-%02d'", dtv.tm_year + 1900, dtv.tm_mon + 1, dtv.tm_mday);
    return tstr;
}
// -------------------------------------------------------------------------------------------------
const char* tm_datetime::printISOTimestamp()
{
    snprintf(tstr,
             sizeof(tstr),
             "%4d-%02d-%02dT%02d:%02d:%02d",
             dtv.tm_year + 1900,
             dtv.tm_mon + 1,
             dtv.tm_mday,
             dtv.tm_hour,
             dtv.tm_min,
             dtv.tm_sec);
    return tstr;
}
const char* tm_datetime::printDBTimestamp()
{
    if (dtv.tm_mday == 0) strcpy(tstr, "null");
    else snprintf(tstr,
                  sizeof(tstr),
                  "'%4d-%02d-%02dT%02d:%02d:%02d'",
                  dtv.tm_year + 1900,
                  dtv.tm_mon + 1,
                  dtv.tm_mday,
                  dtv.tm_hour,
                  dtv.tm_min,
                  dtv.tm_sec);
    return tstr; // '2024-03-02T10:11:30'
}

} // namespace c4s