/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#include "libconfig.hpp"
#include "exception.hpp"
#include "ntbs.hpp"
#include "path.hpp"
#include "file.hpp"
#include "util.hpp"

namespace c4s {

file::file(const c4s::path& _fname) :
    fname(_fname)
{
    handle = fopen(fname.get_ccpath(), (fname.exists() ? "r+" : "w+"));
    if (!handle)
        throw rterror("file::file - unable to open: %s", fname.get_ccpath());
}
// -------------------------------------------------------------------------------------------------
size_t file::read_into(ntbs buffer, size_t count) const
{
    buffer.realloc(count, false);
    size_t len = fread(buffer.get(), 1, count, handle);
    buffer[len] = 0;
    buffer.reset();
    return len;
}
// -------------------------------------------------------------------------------------------------
size_t file::write_from(const c4s::ntbs buffer, size_t count) const
{
    if (count)
        return fwrite(buffer.get(), 1, min_size(count, buffer.length()), handle);
    return fwrite(buffer.get(), 1, buffer.length(), handle);
}
size_t file::append_from(const c4s::ntbs buffer, size_t count) const
{
    fseek(handle, 0, SEEK_END);
    return write_from(buffer, count);
}
// -------------------------------------------------------------------------------------------------
uint64_t file::fnv_hash64(uint64_t salt) const
{
    char buffer[0x4000];
    uint64_t hash = salt;

    size_t br = 0;
    do {
        br = fread(buffer, 1, sizeof(buffer), handle);
        if (br)
            hash = c4s::fnv_str_hash64(buffer, br, hash);
    } while (br);
    return hash;
}

// -------------------------------------------------------------------------------------------------
/**  Uses Boyer-Moore algorithm to search for a text in a given stream. Search begins from the
     current position. If match is found the file pointer is positioned to the start of the
     needle. On error an exception is thrown.

  \param needle String that should be found
  \retval bool True if needle was found, false if not.
*/
bool file::search(const ntbs& needle) const
{
    const size_t BMAX = 0x2000;
    char buffer[BMAX];
    long tg, total_offset;
    // streamsize tg;
    size_t br, boffset, overlap = 0;
    size_t nsize = needle.length();
    if (nsize >= BMAX)
        throw rterror("file::search - size of search text exceeds internal read buffer size.");
    tg = ftell(handle);
    if (tg < 0)
        throw rterror("file::search - unable to get file position information.");
    total_offset = tg;
    do {
        br = fread(buffer + overlap, 1, BMAX - overlap, handle);
        if (search_bmh((unsigned char*)buffer,
                       br + overlap,
                       (unsigned char*)needle.get(),
                       nsize,
                       &boffset))
        {
            fseek(handle, total_offset + boffset, SEEK_SET);
            return true;
        }
        total_offset += br;
        memcpy(buffer, buffer + BMAX - nsize, nsize);
        if (!overlap) {
            overlap = nsize;
            total_offset -= nsize;
        }
    } while (!feof(handle));
    return false;
}

// -------------------------------------------------------------------------------------------------
/** All instances of the search text are replaced.
 * File handle will be closed after this function is called.
  \param search String to search for
  \param replace Text that will be written instead of search string.
  \param backup If true the original file will be backed up.
  \retval int Number of replacements done.
 */
int file::search_replace(const ntbs& search, const ntbs& replace, bool backup)
{
    char *btr, buffer[0x4000];
    int count = 0;
    size_t reread = 0, find_pos, br;

    if (search.size() > sizeof(buffer))
        throw rterror(
            "file::search_replace - Search string size exceeds the internal buffer size.");

    path copy_p(fname);
    copy_p.base_append("~tmp");
    FILE *copy_h = fopen(copy_p.get_ccpath(), "wb");
    do {
        br = fread(buffer + reread, 1, sizeof(buffer) - reread, handle);
        btr = buffer;
        do {
            if (search_bmh((unsigned char*)btr,
                           (SIZE_T)br,
                           (unsigned char*)search.get(),
                           search.size(),
                           &find_pos)) {
                count++;
                fwrite(btr, 1, find_pos, copy_h);
                fwrite(replace.get(), 1, replace.length(), copy_h);
                btr += find_pos + search.length();
                br -= find_pos + search.length();
            } else {
                fwrite(btr, 1, br, copy_h);
                br = 0;
            }
        } while (br > 0);
    } while (!feof(copy_h));
    fclose(copy_h);
    fclose(handle);
    handle = 0;

    if (count)
        backup_rename(copy_p, backup);
    return count;
}

// -------------------------------------------------------------------------------------------------
void  file::backup_rename(path& copy_p, bool backup)
{
    fclose(handle);
    handle = nullptr;
    try {
        if (backup) {
            path backup_name(fname);
            backup_name.base_append("~");
            fname.rename(backup_name.get_base(), true);
        } else
            fname.rm();
        copy_p.rename(fname.get_base(), true);
    } catch (const c4s_exception&) {
        throw rterror("file::backup_rename - temp file rename error.");
    }
    handle = fopen(fname.get_ccpath(), "r+");
    if (!handle)
        throw rterror("file::backup_rename - unable to reopen file.");
}

// -------------------------------------------------------------------------------------------------
/** Relaces the text between start and end tags with the given replacement string. Only first
  instance is replaced. Replace tags are left in place, only text in between is replaced. Throws
  exception if file cannot be opened or written and other read/write errors.

   \param start_tag Start tag.
   \param end_tag End tag.
   \param rpl_txt Replacemnt text.
   \param backup If true then original file is backed up.
   \retval bool True if replacement was done. False if start or end tag was not found.
*/
bool file::replace_block(const ntbs& start_tag,
                         const ntbs& end_tag,
                         const ntbs& rpl_txt,
                         bool backup)
{
    char buffer[0x4000];
    size_t br, soffset, eoffset;

    // Search strings cannot exceed the buffer size.
    if (start_tag.size() >= (SSIZE_T)sizeof(buffer) || end_tag.size() >= (SSIZE_T)sizeof(buffer))
        throw rterror(
            "file::replace_block - Tag size too big. Exceeds internal buffer size.");

    // Search the start and end tags.
    rewind(handle);
    if (!search(start_tag)) {
        return false;
    }
    soffset = ftell(handle);
    soffset += start_tag.size();
    fseek(handle, start_tag.size(), SEEK_CUR);
    if (!search(end_tag)) {
        return false;
    }
    eoffset = ftell(handle);

    // Do the replacing
    rewind(handle);
    path copy_p(fname);
    copy_p.base_append("~tmp");
    FILE *copy_h = fopen(copy_p.get_ccpath(), "wb");
    if (!copy_h) {
        throw rterror("file::replace_block - Unable to open temporary file.");
    }
    // Copy full blocks until replacement start
    for (SIZE_T ndx = 0; ndx < soffset / sizeof(buffer); ndx++) {
        br = fread(buffer, 1, sizeof(buffer), handle);
        if (br != sizeof(buffer))
            goto EXIT_REPLACE;
        fwrite(buffer, 1, sizeof(buffer), copy_h);
    }
    // Copy the rest of start block
    br = fread(buffer, 1, soffset % sizeof(buffer), handle);
    if (br != soffset % sizeof(buffer))
        goto EXIT_REPLACE;
    fwrite(buffer, 1, soffset % sizeof(buffer), copy_h);

    // Copy rpl_txtment and write the rest.
    fwrite(rpl_txt.get(), 1, rpl_txt.size(), copy_h);
    fseek(handle, eoffset, SEEK_SET);
    while (!feof(handle)) {
        br = fread(buffer, 1, sizeof(buffer), handle);
        fwrite(buffer, 1, br, copy_h);
    }
    fclose(copy_h);
    backup_rename(copy_p, backup);
    return true;

EXIT_REPLACE:
    fclose(copy_h);
    copy_p.rm();
    throw rterror("file::replace_block - Read size mismatch. Aborting replace.");
}

}; // namespace c4s
