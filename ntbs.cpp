#include <cstdarg>
#include <cstdio>
#include <stdexcept>
#include <iostream>

#include "ntbs.hpp"
#include "exception.hpp"
#include "util.hpp"

#ifdef NTBS_DEBUG
#include <stdio.h>
extern FILE* utlog;
#endif

namespace c4s {

void ntbs::initialize(TYPE tt)
{
    type = tt;
    max = 0;
    len = 0;
    data.store = 0;
}

ntbs::ntbs(size_t _max)
    : max(_max)
    , len(0)
{
    if (max == 0)
        initialize(CONST);
    else {
        max++;
        data.store = new char[max]();
        type = ALLOC;
    }
}

ntbs::ntbs(char* source, size_t _max)
    : max(_max)
    , type(STACK)
{
    if (!source) {
        initialize(STACK);
        return;
    }
    if (max) {
        len = 0;
        *source = 0;
    } else {
        len = strlen(source);
        max = len + 1;
    }
    data.store = source;
}

ntbs::ntbs(const char* buffer, size_t _max, TYPE _type)
    : type(_type)
{
    if (!buffer) {
        initialize(CONST);
        return;
    }
    if (!_max) {
        len = strlen(buffer);
        max = len + 1;
    } else {
        len = _max;
        max = len + 1;
        if (type == CONST)
            type = ALLOC;
    }
    if (type == CONST) {
        data.store = (char*)buffer;
    } else if (type == STACK) {
        data.store = (char*)buffer;
        if (_max)
            data.store[len] = 0;
    } else { // ALLOC
        data.store = new char[max];
        strncpy(data.store, buffer, len);
        data.store[len] = 0;
    }
}

ntbs::ntbs(const ntbs& orig)
{
    max = orig.max;
    len = orig.len;
    if (max) {
        if (orig.type == CONST) {
            type = CONST;
            data.store = orig.data.store;
        } else {
            type = ALLOC;
            data.store = new char[max];
            strcpy(data.store, orig.data.store);
        }
        return;
    }
    initialize(CONST);
}

void ntbs::reset()
{
    if (max) {
        len = strlen(data.store);
        if (len > max - 1) {
            throw rterror("ntbs::reset - buffer overrun detected: len %ld / max %ld",
                     len,
                     max);
        }
    } else {
        len = 0;
        data.store = 0;
    }
}

ntbs::ntbs(const std::string& orig)
{
    len = orig.size();
    max = len + 1;
    type = ALLOC;
    data.store = new char[max];
    strcpy(data.store, orig.data());
}

void ntbs::operator=(const std::string& orig)
{
    len = orig.size();
    realloc(len, false);
    std::strcpy(data.store, orig.c_str());
}

void ntbs::realloc(size_t req_bytes, bool copy)
{
    // Note! Const types are always reallocated.
    if (type != CONST && req_bytes < max)
        return;
#ifdef NTBS_DEBUG
    fprintf(utlog, "..ntbs::realloc %ld => %ld\n", max, req_bytes + 1);
#endif
    max = req_bytes + 1;
    char* news = new char[max]();
    if (data.store) {
        if (copy || type == CONST)
            strcpy(news, data.store);
        if (type == ALLOC)
            delete[] data.store;
    }
    data.store = news;
    type = ALLOC;
}

void ntbs::clear()
{
    len = 0;
    if (type == CONST) {
        data.store = 0;
        max = 0;
    } else if (data.store)
        *data.store = 0;
}

void ntbs::operator=(const char* source)
{
    if (!source)
        return;
    len = strlen(source);
    realloc(len, false);
    strcpy(data.store, source);
}

void ntbs::operator=(const ntbs& orig)
{
    if (orig.len == 0) {
        clear();
        return;
    }
    len = orig.len;
    if (type == CONST) {
        if (orig.type == CONST) {
            data.store = orig.data.store;
            max = orig.max;
            return;
        }
        max = 0;
    }
    realloc(len, false);
    strcpy(data.store, orig.data.store);
}

void ntbs::operator+=(const char* source)
{
    if (!source)
        return;
    if (type == CONST)
        max = 0;
    size_t sl = strlen(source);
    if (len + sl >= max)
        realloc(len + sl);
    std::strcpy(data.store + len, source);
    len += sl;
}

void ntbs::operator+=(char ch)
{
    if (type == CONST)
        max = 0;
    if (len + 1 >= max)
        realloc(len + 5);
    data.store[len] = ch;
    data.store[len + 1] = 0;
    len++;
}

ntbs ntbs::operator+(const ntbs& right) const
{
    ntbs cc(max + right.max + 1);
    cc = *this;
    cc += right;
    return cc;
}

char& ntbs::operator[](size_t ndx)
{
    if ((size_t)ndx < len)
        return data.store[ndx];
    throw c4s_exception("ntbs[] index out of range");
}


const char& ntbs::operator[](size_t ndx) const
{
    if ((size_t)ndx < len)
        return data.store[ndx];
    throw c4s_exception("ntbs[] index out of range");
}

int ntbs::compare(size_t beg, size_t count, const ntbs& needle) const
{
    if (beg >= len)
        return -1;
    if (beg + count > len)
        count = len - beg;
    return strncmp(data.store + beg, needle.data.store, count);
}
// =================================================================================================
size_t ntbs::find(int ch, size_t offset) const
{
    if (!max || offset >= len)
        return SIZE_MAX;
    const char* ndx = std::strchr(data.store + offset, ch);
    return ndx ? ndx - data.store : SIZE_MAX;
}

size_t ntbs::find(const char* target, size_t offset) const
{
    if (!max || target == data.store || offset >= len)
        return SIZE_MAX;
    const char* ndx = std::strstr(data.store + offset, target);
    return ndx ? ndx - data.store : SIZE_MAX;
}

size_t ntbs::rfind(int ch, size_t offset) const
{
    if (!max)
        return SIZE_MAX;
    if (offset == SIZE_MAX)
        offset = len - 1;
    else if (offset > len)
        return SIZE_MAX;
    for (char* ptr = data.store + offset; ptr >= data.store; ptr--) {
        if ((int)*ptr == ch)
            return ptr - data.store;
    }
    return SIZE_MAX;
}
// =================================================================================================
int ntbs::sprint(const char* fmt, ...)
{
    va_list vl;
    bool first = true;
    if (type == CONST)
        max = 0; // Force allocation
retry_sp:
    va_start(vl, fmt);
    int bw = std::vsnprintf(data.store, max, fmt, vl);
    va_end(vl);
    if (bw < 0)
        throw rterror("ntbs::sprintf format error.");
    if ((size_t)bw >= max) {
        if (first) {
            first = false;
            realloc(bw, false);
            goto retry_sp;
        } else
            throw rterror("ntbs::sprintf unable to alloc more memory.");
    }
    len = (size_t)bw;
    return len;
}

int ntbs::addprint(const char* fmt, ...)
{
    va_list vl;
    if (!max || type == CONST)
        throw c4s_exception("ntbs::addprint called on empty or constant string.");
    bool first = true;
    char* ptr = data.store + len;
    size_t limit = max - len;

retry_ap:
    va_start(vl, fmt);
    int bw = std::vsnprintf(ptr, limit, fmt, vl);
    va_end(vl);
    if (bw < 0)
        throw rterror("ntbs::addprint format error.");
    if ((size_t)bw >= limit) {
        if (first) {
            first = false;
            realloc(len + bw, true);
            limit = max - len;
            ptr = data.store + len;
            goto retry_ap;
        } else
            throw rterror("ntbs::addprint unable to alloc more memory.");
    }
    len = (size_t)bw + len;
    return len;
}

ntbs ntbs::substr(size_t beg, size_t count) const
{
    if (count == SIZE_MAX || beg + count > len)
        count = len - beg;
    if (count >= len || !max)
        return ntbs(*this);
    ntbs copy(count);
    memcpy(copy.data.store, data.store + beg, count);
    copy.data.store[count] = 0;
    copy.len = count;
    return copy;
}

void ntbs::substr(size_t beg, size_t count, ntbs& target) const
{
    if (count == SIZE_MAX || beg + count > len)
        count = len - beg;
    if (count >= len || !max) {
        target = *this;
        return;
    }
    target.realloc(count);
    memcpy(target.data.store, data.store + beg, count);
    target.data.store[count] = 0;
    target.len = count;
}

void ntbs::assign(const ntbs& src, size_t beg, size_t count)
{
    if (beg > src.length())
        return;
    assign(src.data.store + beg, count);
}

void ntbs::assign(const char* src, size_t count)
{
    if (!src || !count)
        return;
    realloc(count);
    memcpy(data.store, src, count);
    len = count;
    data.store[len] = 0;
}

void ntbs::set_const(const char* src)
{
    if (type == ALLOC)
        delete[] data.store;
    initialize(CONST);
    if (!src)
        return;
    len = strlen(src);
    max = len + 1;
    data.store = (char*)src;
}

void ntbs::append(const char* src, size_t count)
{
    if (!count || !src)
        return;
    realloc(len + count, true);
    memcpy(data.store + len, src, count);
    len += count;
    data.store[len] = 0;
}

void ntbs::prepend(const char* src, size_t count)
{
    if (count == SIZE_MAX)
        count = strlen(src);
    realloc(len + count, true);
    memmove(data.store + count, data.store, len + 1);
    memcpy(data.store, src, count);
}

void ntbs::replace(size_t beg, size_t count, const ntbs& source)
{
    if (beg >= len)
        throw c4s_exception("ntbs::replace - begin offset overflow");
    if (len - beg <= count) {
        erase(beg);
        *this += source;
        return;
    }
    realloc(len - count + source.len, true);
    char* start = data.store + beg;
    memmove(start + source.len, start + count, len - beg - count + 1);
    memcpy(start, source.get(), source.len);
    len = strlen(data.store);
}

void ntbs::insert(size_t beg, const ntbs& source)
{
    if (beg >= len)
        throw c4s_exception("ntbs::replace - begin offset overflow");
    realloc(len + source.length());
    memmove(data.store + beg + source.length(), data.store + beg, len - beg + 1);
    memcpy(data.store + beg, source.get(), source.length());
    len += source.length();
}

void ntbs::erase(size_t from, size_t count)
{
    if (!max || from >= len)
        return;
    if (count > len - from) {
        data.store[from] = 0;
        len = from;
        return;
    }
    if (type == CONST) {
        max = 0;
        realloc(len, true);
    }
    memmove(data.store + from, data.store + from + count, len - from - count);
    len -= count;
    data.store[len] = 0;
}

void ntbs::copy_reference(const ntbs& source)
{
    if (!source.max)
        return;
    if (type == ALLOC)
        delete[] data.store;
    data.store = source.data.store;
    type = CONST;
    len = source.len;
    max = source.max;
}

// NOTE: Trim function will not work properly if gcc optimization is -O2
void ntbs::trim(TRIM tt)
{
    using namespace std;
    const char *wi, *ws = " \t\n\r\0";
    if (!data.store)
        return;
    if (type == CONST) {
        max = 0;
        realloc(len);
    }
    if (tt == BOTH || tt == RIGHT) {
        char* end = data.store + len - 1;
        do {
            for (wi = ws; *wi; wi++) {
                if (*end == *wi)
                    break;
            }
            if (*wi) {
                *end = 0;
                end--;
                len--;
            }
        } while (*wi && end >= data.store);
        if (!data.store)
            return;
    }
    if (tt == BOTH || tt == LEFT) {
        char* beg = data.store;
        do {
            for (wi = ws; *wi; wi++) {
                if (*beg == *wi)
                    break;
            }
            if (*wi) {
                beg++;
                len--;
            }
        } while (*wi && *beg);
        if (*beg)
            memmove(data.store, beg, len + 1);
        else
            *data.store = 0;
    }
}

void ntbs::trim_end(size_t count)
{
    if (!max)
        return;
    if (len < count) {
        *data.store = 0;
        len = 0;
        return;
    }
    data.store[len - count] = 0;
    len -= count;
}

uint64_t ntbs::hash_fnv(uint64_t salt) const
{
    if (!max || len == 0)
        return 0;
    if (len < 9) {
        // For short less than 64bit strings the string itself is the hash.
        union {
            char ch[8];
            uint64_t key;
        } pack;
        pack.key = 0;
        memcpy(pack.ch, data.store, len);
        if (salt)
            pack.key |= salt;
        return pack.key;
    }
    return fnv_str_hash64(data.store, len, salt);
}

// --------------------------------------------------------
void ntbs::dump(FILE* out) const
{
    char buffer[64];

    if (!out)
        return;
    const char *tstr = type == ALLOC ? "Alloc"
                         : (type == CONST ? "Const"
                         : (type == STACK ? "None" : "Unknown"));
    fprintf(out, "ntbs /l:%ld /m:%ld /type:%s ", len, max, tstr);
    if (max && data.store) {
        if (sizeof(buffer) < len) {
            strncpy(buffer, data.store, sizeof(buffer) - 1);
            buffer[sizeof(buffer) - 1] = 0;
            fprintf(out, "/store:%s...", buffer);
        } else
            fprintf(out, "/store:%s", data.store);
    } else
        fputs("(empty)", out);
    fputc('\n', out);
}

} // namespace c4s

// --------------------------------------------------------
std::ostream& std::operator<<(std::ostream& os, const c4s::ntbs& ns)
{
    os << ns.get();
    return os;
}

std::istream& std::operator>>(std::istream& is, c4s::ntbs& ns)
{
    streamoff posnow = is.tellg();
    is.seekg(0, std::ios_base::end);
    streamoff posend = is.tellg();
    is.seekg(posnow, std::ios_base::beg);

    streamoff max = posend - posnow;
    if (max > C4S_NTBS_MAX_ISTREAM_READ)
        max = C4S_NTBS_MAX_ISTREAM_READ;
    ns.realloc(max, false);
    is.read(ns.get(), max);
    ns.reset();
    return is;
}
