/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#ifndef C4S_USER_HPP
#define C4S_USER_HPP

#if defined(__linux) || defined(__APPLE__)

namespace c4s {

typedef int (*fork_fn)(void*);

// -----------------------------------------------------------------------------------------------------------
//! Class that encapsulates a user in the target system.
/*! User stores the system's user and group id as well as the user and group names.
  User and group ids corresponding to given names are read at the constructor. Please note that user
  does not necessarily belong to the named group. In this case the valid() function
  returns false. User can be added with create() -funtion.
*/
class user
{
public:
    //! Default constructor initializes empty user object.
    user();
    //! Creates a new user object from user name. Group name will be empty.
    user(const char* name);
    //! Creates a new user object with name and group only.
    user(const char* name, const char* group, bool system = false);
    //! Creates a new user object with all given information
    user(const char* name,
         const char* group,
         bool system,
         const char* home,
         const char* shell = 0,
         const char* GROUPS = 0);
    //! Copy constructor
    user(const user& orig);

    //! Sets user information by reading it from the system.
    void set(int id_u, int id_g);
    //! Copies user to another.
    void operator=(const user& org)
    {
        name = org.name;
        group = org.group;
        home = org.home;
        uid = org.uid;
        gid = org.gid;
        system = org.system;
    }

    //! Clears this user object.
    void clear()
    {
        name.clear();
        group.clear();
        uid = -1;
        gid = -1;
    }
    //! Makes sure the user exists in the system as currently presented in memory.
    void create(bool append_groups = false);
    //! Returns zero if user exists in system as it is now in memory.
    int status() const;
    //! Returns true if the user and group names have not been specified.
    bool empty() { return name.empty() && group.empty() ? true : false; }
    //! Returns true if given ids match this user.
    bool match(int uid, int gid) const;
    bool match(const user&) const;

    //! Returns user's name.
    ntbs get_name() { return name; }
    //! Returns user's name as cc
    const char* get_ccname() { return name.get(); }
    //! Returns user's primary group.
    ntbs get_group() { return group; }
    //! Returns user's primary group as cc.
    const char* get_ccgroup() { return group.get(); }
    //! Return user's home directory. Note: directory is without ending slash.
    ntbs get_home() { return home.get(); }
    //! Returns user's ID. Value is -1 if user did not exist in the system.
    int get_uid() const { return uid; }
    //! Returns user's group ID. Value is -1 if group did not exist in the system.
    int get_gid() const { return gid; }

    //! Sets user's home directory.
    void set_home(const ntbs& src)
    {
        static_cast<ntbs>(home) = src;
        if (home.get_last() == C4S_DSEP)
            home.trim_end();
    }
    //! Returns true if this user is designated as root/admin
    bool is_admin() const { return (status() == 0 && uid == 0) ? true : false; }
    //! Returns true if user's name and group can be found from the system.
    bool is_ok() const { return (uid >= 0 || gid >= 0) ? true : false; }

    //! Forks to given callback function as a user.
    int run_fork(fork_fn, void*);

    static user get_current();
    //! Writes user's attributes to given stream. Use for debugging.
    void dump(std::ostream&) const;

    ntbs_xs GROUPS; //!< Comma separated list that the user should also belong to.
    ntbs_sm shell;  //!< User's login shell.

protected:
    //! Reads the user's user and group ids from the system.
    bool read();

    ntbs_xs name;  //!< User's account name.
    ntbs_xs group; //!< User's primary group.
    ntbs_sm home;  //!< User's home directory
    bool system;
    int uid, gid;
};

} // namespace c4s

#endif // Linux | Apple

#endif // C4S_USER_HPP
