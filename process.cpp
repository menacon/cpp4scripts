/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#include <iostream>
#include <cstring>
#include <fcntl.h>
#include <grp.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

#include "libconfig.hpp"
#include "ntbs.hpp"
#include "exception.hpp"
#include "user.hpp"
#include "path.hpp"
#include "path_list.hpp"
#include "compiled_file.hpp"
#include "program_arguments.hpp"
#include "ring_buffer.hpp"
#include "process.hpp"
#include "util.hpp"
#include "logger.hpp"

#ifdef C4S_UNITTEST
#include <stdio.h>
extern FILE* utlog;
#endif

using namespace std;

namespace c4s {

// -------------------------------------------------------------------------------------------------
/*!
  Initializes process pipes by creating three internal pipes.
*/
proc_pipes::proc_pipes()
{
    int fd_tmp[2];
    memset(fd_out, 0, sizeof(fd_out));
    memset(fd_err, 0, sizeof(fd_err));
    memset(fd_in, 0, sizeof(fd_in));

    if (pipe(fd_out))
        throw c4s_exception(
            "proc_pipes::proc_pipes - Unable to create pipe for the process std output.");
    if (fd_out[0] < 3 || fd_out[1] < 3) {
        if (pipe(fd_tmp))
            throw c4s_exception(
                "proc_pipes::proc_pipes - Unable to create pipe for the process std output (2).");
        fd_out[0] = fd_tmp[0];
        fd_out[1] = fd_tmp[1];
    }
    if (pipe(fd_err))
        throw c4s_exception(
            "proc_pipes::proc_pipes - Unable to create pipe for the process std error.");
    if (pipe(fd_in))
        throw c4s_exception(
            "proc_pipes::proc_pipes - Unable to create pipe for the process std input.");

    // Make out and err read pipes nonblocking
    int fflag = fcntl(fd_out[0], F_GETFL, 0);
    fcntl(fd_out[0], F_SETFL, fflag | O_NONBLOCK);
    fflag = fcntl(fd_err[0], F_GETFL, 0);
    fcntl(fd_err[0], F_SETFL, fflag | O_NONBLOCK);
    br_in = 0;
    send_ctrlZ = false;
}
// -------------------------------------------------------------------------------------------------
void proc_pipes::close_child_input()
{
    if (fd_in[0]) {
        close(fd_in[0]);
        fd_in[0] = 0;
    }
    if (fd_in[1]) {
        close(fd_in[1]);
        fd_in[1] = 0;
    }
}
// -------------------------------------------------------------------------------------------------
/*!
  Closes the pipes created by the constructor.
*/
proc_pipes::~proc_pipes()
{
    if (fd_out[0])
        close(fd_out[0]);
    if (fd_out[1])
        close(fd_out[1]);
    if (fd_err[0])
        close(fd_err[0]);
    if (fd_err[1])
        close(fd_err[1]);
    if (fd_in[0])
        close(fd_in[0]);
    if (fd_in[1])
        close(fd_in[1]);
}
// -------------------------------------------------------------------------------------------------
/*!
  Initilizes child side pipes. Call this from the child right after the fork and before exec.
  After this function you should destroy this object.
  \retval bool True on succes, false on error.
*/
void proc_pipes::init_child()
{
    close(fd_in[1]);
    close(fd_out[0]);
    close(fd_err[0]);
    dup2(fd_in[0], STDIN_FILENO);   // = 0
    dup2(fd_out[1], STDOUT_FILENO); // = 1
    dup2(fd_err[1], STDERR_FILENO); // = 2
    close(fd_in[0]);
    close(fd_out[1]);
    close(fd_err[1]);
    memset(fd_out, 0, sizeof(fd_out));
    memset(fd_err, 0, sizeof(fd_err));
    memset(fd_in, 0, sizeof(fd_in));
}
// -------------------------------------------------------------------------------------------------
/*!
  Initializes the parent side pipes. Call this from parent right after the fork.
  NOTE! This does nothing in Windows since the functionality is not needed
*/
void proc_pipes::init_parent()
{
    // Close the input read side
    close(fd_in[0]);
    fd_in[0] = 0;
    // Close the output write sides
    close(fd_out[1]);
    close(fd_err[1]);
    fd_out[1] = 0;
    fd_err[1] = 0;
}

// -------------------------------------------------------------------------------------------------
bool proc_pipes::write_to_file(FILE* log)
{
    char lb[8196];
    ssize_t br1 = 0;
    ssize_t br2 = 0;

    br1 = ::read(fd_out[0], lb, sizeof(lb));
    if (br1 > 0)
        fwrite(lb, br1, 1, log);
    br2 = ::read(fd_err[0], lb, sizeof(lb));
    if (br2 > 0)
        fwrite(lb, br2, 1, log);
    return (br1 > 0 || br2 > 0) ? true : false;
}

// -------------------------------------------------------------------------------------------------
/*!
  Read stdout from the child process and print it out on the given stream.
  \param pout Stream for the output
*/
bool proc_pipes::read_child_stdout(ring_buffer* rbuf)
{
    size_t rsize = rbuf->write_from(fd_out[0]);
#ifdef C4S_UNITTEST
    if (rsize > 0)
        fprintf(utlog, "proc_pipes::read_child_stdout - %ld bytes\n", rsize);
#endif
    return rsize > 0 ? true : false;
}
// -------------------------------------------------------------------------------------------------
/*!
  Read stderr from the child process and print it out on the given stream.
  \param pout Stream for the output
*/
bool proc_pipes::read_child_stderr(ring_buffer* rbuf)
{
    size_t rsize = rbuf->write_from(fd_err[0]);
#ifdef C4S_UNITTEST
    if (rsize > 0)
        fprintf(utlog, "proc_pipes::read_child_stdout - %ld bytes\n", rsize);
#endif
    return rsize > 0 ? true : false;
}
// -------------------------------------------------------------------------------------------------
/*!
  Writes the ring_buffer content into the child input.
  \param input String to write.
*/
size_t proc_pipes::write_child_input(ring_buffer* data)
{
    size_t cnt = 0, ss;
    if (!data || !fd_in[1])
        return 0;
    do {
        ss = data->read_into(fd_in[1], data->max_size());
        cnt += ss;
    } while (ss > 0);
#ifdef C4S_UNITTEST
    fprintf(utlog, "proc_pipes::write_child_input - %ld bytes to child stdin\n", cnt);
#endif
    return cnt;
}
// -------------------------------------------------------------------------------------------------
size_t proc_pipes::write_child_input(ntbs* data)
{
    if (!data || !fd_in[1])
        return 0;
    ssize_t bw = 0;
    size_t cnt = 0;
    size_t max = data->length();
    char* ptr = data->get();
    int retry_count = 0;
    while (retry_count < 10 && bw < (ssize_t)max) {
        bw = ::write(fd_in[1], ptr, max);
        if (bw == -1) {
            if (errno == EAGAIN)
                retry_count++;
            else
                return 0;
        } else {
            max -= (size_t)bw;
            ptr += bw;
            cnt += bw;
        }
    }
#ifdef C4S_UNITTEST
    fprintf(utlog, "proc_pipes::write_child_input (ntbs) - Wrote %ld bytes to child stdin\n", cnt);
#endif
    return cnt;
}

// -------------------------------------------------------------------------------------------------
// ###############################  PROCESS ########################################################
// -------------------------------------------------------------------------------------------------
bool process::no_run = false;
bool process::nzrv_exception = false;
bool process::nzrv_save_stderr = true;
unsigned int process::general_timeout = 0;
FILE* process::global_log = 0;

// -------------------------------------------------------------------------------------------------
/*! Common initialize code for all constructors. Constructors call this function first.
 */
void process::init_member_vars()
{
    pid = 0;
    last_ret_val = 0;
    stream_in = 0;
    pipes = 0;
    echo = false;
    owner = 0;
    daemon = false;
    timeout = general_timeout;
    command_ok = false;
    environment = 0;
}

// -------------------------------------------------------------------------------------------------
/*! \param cmd Command to execute.
  \param args Arguments to pass to the executable.
  \param _owner = User account that should be used to run the process.
*/
process::process(const char* cmd, const char* args, PIPE pipe, user* _owner)
    : rb_out(pipe == PIPE::NONE ? 0 : (pipe == PIPE::SM ? RB_SIZE_SM : RB_SIZE_LG))
    , rb_err(pipe == PIPE::NONE ? 0 : (pipe == PIPE::SM ? RB_SIZE_SM : RB_SIZE_LG))
    , command(cmd)
{
    if (command.empty())
        throw c4s_exception("process::process - empty command is not allowed.");
    init_member_vars();
    if (args)
        set_args(args);
    if (_owner) {
        if (!_owner->is_ok() || _owner->status() > 0)
            throw c4s_exception("process::process - Invalid process owner.");
        owner = _owner;
    }
}

// -------------------------------------------------------------------------------------------------
/*! \param cmd Command to execute.
  \param args Arguments to pass to the executable.
  \param _owner = User account that should be used to run the process.
*/
process::process(const ntbs& cmd, const ntbs& args, PIPE pipe, user* _owner)
    : rb_out(pipe == PIPE::NONE ? 0 : (pipe == PIPE::SM ? RB_SIZE_SM : RB_SIZE_LG))
    , rb_err(pipe == PIPE::NONE ? 0 : (pipe == PIPE::SM ? RB_SIZE_SM : RB_SIZE_LG))
    , command(cmd)
    , arguments(args)
{
    if (command.empty())
        throw c4s_exception("process::process - empty command is not allowed.");
    init_member_vars();
    if (_owner) {
        if (!_owner->is_ok() || _owner->status() > 0)
            throw c4s_exception("process::process - Invalid process owner.");
        owner = _owner;
    }
}

// -------------------------------------------------------------------------------------------------
/*! \param cmd Name of the command to execute.
 *  \param args Arguments to pass to the executable.
 *  \param child_out Process output is sent to this stream.
 */
process::process(const path& cmd, const char* args)
    : rb_out(0)
    , rb_err(0)
    , command(cmd)
    , arguments(args)
{
    if (command.empty())
        throw c4s_exception("process::process - empty command is not allowed.");
    init_member_vars();
}

// -------------------------------------------------------------------------------------------------
/*! If the the daemon mode has NOT been set then the destructor kills the process if it is still
 * running.
 */
process::~process()
{
#ifdef C4S_UNITTEST
    fprintf(utlog, "process::~process - name=%s\n", command.get_ccbase());
#endif
    if (pid && !daemon)
        stop();

    if (pipes) {
        delete pipes;
        pipes = 0;
    }
}
// -------------------------------------------------------------------------------------------------
/*!  Finds the command from current directory or path.
*/
bool process::find_command()
{
    if (command.empty())
        return false;
    struct stat sbuf;
    memset(&sbuf, 0, sizeof(sbuf));
    // Check the user provided path first. This includes current directory.
    if (stat(command.get_ccpath(), &sbuf) == -1 ||
        !has_anybits(sbuf.st_mode, S_IXUSR | S_IXGRP | S_IXOTH))
    {
        if (!command.exists_in_env_path("PATH", true))
            return false;
    }
    command_ok = true;
    return true;
}
// -------------------------------------------------------------------------------------------------
/*!  Copies the command name, arguments and stream_out. Other
  attributes are simply cleared. Function should not be called if this process is running.
  \param source Source process to copy.
*/
void process::operator=(const process& source)
{
    pid = 0;
    command = source.command;
    command_ok = source.command_ok;
    arguments = source.arguments;
    timeout = source.timeout;
    if (source.owner)
        owner = source.owner;
    if (source.rb_out.max_size())
        rb_out.resize(source.rb_out.max_size());
    if (source.rb_err.max_size())
        rb_err.resize(source.rb_err.max_size());
    daemon = source.daemon;
}
// ### \TODO continue to improve documentation from here on down.
// -------------------------------------------------------------------------------------------------
void process::dump(FILE* out)
{
    if (!out) {
        if (global_log)
            out = global_log;
        else
            return;
    }
    const char* pe = echo ? "true" : "false";
    const char* env = environment ? "set" : "none";
    fprintf(out, "%s (%s);\n", command.get_ccpath(), arguments.get());
    fprintf(out, "  PID=%d; echo=%s; retval=%d; stdout=%ld; environment=%s\n",
            pid, pe, last_ret_val, rb_out.max_size(), env);
}

/*  For calling exec-functions arguments need to be in *argv[]. At the time of the call we are in
    child process so we copy the original string in 'arg_str' into given 'buffer' changing spaces
    to zeros and at the same time making pointers to 'args_out'.
    arg_lst[0] = name of the executable
    arg_lst[1] = pointer to first argument => we start filling pointers from index 2
*/
int process::convert_arguments(const char** arg_lst, const char* arg_str, char* buffer)
{
    char prev = ' ', quote = 0;
    arg_lst[1] = buffer;
    int ptr_count = 2;
    while (*arg_str) {
        if (quote) {
            if (quote == *arg_str) {
                if (prev != '\\') {
                    quote = 0;
                } else
                    *(buffer - 1) = *arg_str;
            } else
                *buffer++ = *arg_str;
        } else if (*arg_str == '\'' || *arg_str == '\"') {
            if (prev == '\\') {
                *(buffer - 1) = *arg_str;
            } else {
                quote = *arg_str;
            }
        } else if (*arg_str == ' ') {
            if (prev != ' ') {
                *buffer++ = 0;
                arg_lst[ptr_count++] = buffer;
                if (ptr_count >= MAX_PROCESS_ARGS - 1) {
                    printf("c4s::process::start - Too many arguments. Use response file.\n");
                    return -1;
                }
            }
        } else
            *buffer++ = *arg_str;
        prev = *arg_str;
        arg_str++;
    }
    *buffer = 0;

    if (quote) {
        printf("c4s::process::start - Unmatched quote marks in arguments.\n");
        return -2;
    }

    arg_lst[ptr_count] = 0;
    if (arg_lst[ptr_count - 1][0] == 0)
        arg_lst[ptr_count - 1] = 0;

    return ptr_count;
}

// Static
FILE* process::global_log_open(const char* filename)
{
    if (global_log)
        throw rterror("process::global_log_open - Already open.");
    global_log = fopen(filename, "ab");
    if (!global_log)
        throw rterror("process::global_log_open - Unable to open %s", filename);
    return global_log;
}

// -------------------------------------------------------------------------------------------------
void process::start(const ntbs& args)
{
    if (!command_ok && !find_command()) {
        throw rterror("process::start - Command '%s' not found or not executable.", command.get_ccpath());
    }
    if (pid) {
#ifdef C4S_UNITTEST
        fputs("process::start - earlier instance running. Stopping it...\n", utlog);
#endif
        stop();
    }
    if (args.length())
        arguments = args;
    last_ret_val = 0;
    if (no_run)
        return;

    if (pipes)
        delete pipes;
    pipes = new proc_pipes();
    if (rb_err.max_size())
        rb_err.clear();
    if (rb_out.max_size())
        rb_out.clear();

    // Create the child process i.e. fork
    proc_started = clock();
    proc_ended = proc_started;
    pid = fork();
    if (!pid) {
        // Convert arguments from string into a list of pointers to parameters
        ntbs_lg arg_str;
        const char* arg_list[MAX_PROCESS_ARGS];
        memset(arg_list, 0, sizeof(arg_list));
        arg_list[0] = command.get_ccpath();
        if (arguments.length()) {
            arguments.trim();
            arg_str.realloc(arguments.length(), false);
            int rv = convert_arguments(arg_list, arguments.get(), arg_str.get());
            if (rv < 0)
                _exit(rv);
        }
        // Initialize pipes
        pipes->init_child();
        delete pipes;
        if (owner) {
            if (initgroups(owner->get_ccname(), owner->get_gid()) != 0 ||
                setuid(owner->get_uid()) != 0) {
                fprintf(stderr, "process::start - child-process: Unable to change process persona. "
                        "User:%s\nError:%s\n", owner->get_ccname(), strerror(errno));
                _exit(EXIT_FAILURE);
            }
        }
        // Start the process
        if (environment) {
            if (execve(command.get_ccpath(), (char**)arg_list, (char* const*)environment) == -1) {
                fprintf(stderr, "process::start - child-process: Unable to start execve: %s; Error: %s\n",
                        command.get_ccbase(), strerror(errno));
            }
        } else {
            if (execv(command.get_ccpath(), (char**)arg_list) == -1) {
                fprintf(stderr, "process::start - child-process: Unable to start execv: %s; Error: %s\n",
                        command.get_ccbase(), strerror(errno));
            }
        }
        _exit(EXIT_FAILURE);
    }
    pipes->init_parent();
#ifdef C4S_UNITTEST
    fprintf(utlog, "process::start - created child: %s; pid:%d\n", command.get_ccbase(), pid);
#endif
}
// -------------------------------------------------------------------------------------------------
void process::set_user(user* _owner)
{
    if (!_owner)
        return;
    if (_owner->status() > 0)
        throw c4s_exception("process::set_user - User's information does not match system data."
                            " Unable to set user");
    owner = _owner;
}
// -------------------------------------------------------------------------------------------------
/*! Attaching allows developer to stop running processes by first attaching object to a process and
    then calling stop-function. If process alredy is running this function does nothing. Daemon
    mode is set on.
    \param _pid Process id to attach to.
 */
bool process::attach(int _pid)
{
    if (pid) {
        CS_PRINT_ERRO("process::attach - pid already attached.");
        return false;
    }
    pid = _pid;
    last_ret_val = 0;
    daemon = true;
    if (pipes) {
        delete pipes;
        pipes = 0;
    }
    if (kill(pid, 0) != 0) {
        CS_PRINT_ERRO("process::attach - process already stopped or invalid pid.");
        return false;
    }
    return true;
}
// -------------------------------------------------------------------------------------------------
bool process::attach(const path& pid_file)
{
    long attach_pid = 0;
    if (!pid_file.exists()) {
        CS_PRINT_ERRO("process::attach - pid file not found.");
        return false;
    }
    ifstream pf(pid_file.get_ccpath());
    if (!pf) {
        CS_PRINT_ERRO("process::attach - Unable to open pid file.");
        return false;
    }
    pf >> attach_pid;
    if (attach_pid)
        return attach((int)attach_pid);
    CS_PRINT_ERRO("process::attach - Unable to read pid from file.");
    return false;
}
// -------------------------------------------------------------------------------------------------
int process::query(ntbs* answer, ntbs* input, int _timeout)
{
    timeout = _timeout;
    if (!answer)
        throw c4s_exception("process::query - Missing pointer to answer");
    if (rb_out.max_size() == 0)
        rb_out.resize(answer->size());
    start();
    if (input) {
        write_stdin(input);
        close_stdin();
    }
    wait_for_exit();
    rb_out.read(*answer, true);
    return last_ret_val;
}
// STATIC ------------------------------------------------------------------------------------------
/*! \param cmd Command to run.
  \param args Command arguments.
  \param output Buffer where the output will be stored into.
*/
int process::query(const char* cmd, const char* args, ntbs* answer, ntbs* input, int _timeout)
{
    if (!cmd)
        return -1;
    process source(cmd, args, PIPE::SM);
    return source.query(answer, input, _timeout);
}
// -------------------------------------------------------------------------------------------------
void process::run_daemon()
{
    daemon = true;
    start();
}
// -------------------------------------------------------------------------------------------------
/*!
   \retval bool True if it is, false if not.
*/
bool process::is_running()
{
    if (!pid)
        return false;
    // Take a little nap
    struct timespec ts_delay, ts_remain;
    ts_delay.tv_sec = 0;
    ts_delay.tv_nsec = 100000000L;
    nanosleep(&ts_delay, &ts_remain);
    // Check for timeout
    if (proc_started && timeout) {
        proc_ended = clock();
        if (timeout < duration()) {
#ifdef C4S_UNITTEST
            fprintf(utlog, "Process %s timeout\n", command.get_ccbase());
#endif
            stop();
            throw rterror("process::is_running - %s; timeout %u", command.get_ccbase(), timeout);
        }
    }
    // Read the pipes
    if (pipes) {
        bool serr_out = false;
        bool sout_out = false;
        if (global_log)
            pipes->write_to_file(global_log);
        else {
            if (rb_err.max_size())
                serr_out = pipes->read_child_stderr(&rb_err);
            if (rb_out.max_size())
                sout_out = pipes->read_child_stdout(&rb_out);
            if (serr_out || sout_out) {
    #ifdef C4S_UNITTEST
                fprintf(utlog, "process::is_running - data read for: %s\n", command.get_ccbase());
    #endif
                return true;
            }
        }
    }
    // Are we still running
    int status;
    pid_t wait_val = waitpid(pid, &status, WNOHANG);
    if (!wait_val) {
        return true;
    }
    if (wait_val && wait_val != pid) {
        throw rterror("process::is_running - name = %s; pid = %d; wait error = %s",
                 command.get_ccbase(), pid, strerror(errno));
    }
    // We are done, close up.
    last_ret_val = interpret_process_status(status);
#ifdef C4S_UNITTEST
    fprintf(utlog, "process::is_running - running stopped. Returns: %d\n", last_ret_val);
#endif
    pid = 0;
    stop();
    return false;
}
// -------------------------------------------------------------------------------------------------
/*! \retval int Return value from the process.
 */
int process::wait_for_exit()
{
    if (!pid)
        return last_ret_val;
    if (no_run) {
        last_ret_val = 0;
        return 0;
    }

#ifdef C4S_UNITTEST
    fprintf(utlog, "process::wait_for_exit - name=%s; pid=%d; timeout=%d\n", command.get_ccbase(), pid, timeout);
    while (is_running()) {
        sleep(1);
    }
#else
    while (is_running()) {
        // Intentionally empty
    }
#endif
    return final_check();
}
// -------------------------------------------------------------------------------------------------
/*!
 * Saves the process output into a given file. If no output is available function does nothing.
 * \param fname file name with path to to file where the contents is stored.
 * \param include_stderr Saves stderr as well if available (default true)
 * \retval True on success, False if file cannot be written into.
  */
bool process::save_output(const c4s::path& fname, bool include_stderr)
{
    FILE* handle = fopen(fname.get_ccpath(), "w");
    if (!handle)
        return false;
    if (rb_out.length())
        rb_out.read_into(handle);
    if (include_stderr && rb_err.length())
        rb_out.read_into(handle);
    return true;
}

// -------------------------------------------------------------------------------------------------
/*!
  Executes process with additional argument. Original arguments restored after execution.
  Returns when the process is completed or timeout exeeded.
  \param args Additional argument to append to current set.
  \retval int Return value from the command.
*/
int process::execa(const char* plus)
{
    ntbs backup(arguments);
    arguments += plus;
    start();
    int rv = wait_for_exit();
    arguments = backup;
    return rv;
}

// -------------------------------------------------------------------------------------------------
/*! Executes procerss with given arguments. Logs output if it is defined. 
  \param args Optional arguments for the command.
  \param out Optional log for recording the output.
  \retval int Return value from the command.
*/
int process::operator()(const char* args, FILE* out)
{
    FILE* handle = global_log ? global_log : out;
    if (handle && rb_out.max_size() == 0)
        rb_out.resize(RB_SIZE_LG);
    for (start(args); is_running();) {
        rb_out.read_into(handle);
    }
    return final_check(handle);
}
int process::operator()(const ntbs& args, std::ostream& os)
{
    if (args.length()) {
        arguments += ' ';
        arguments += args;
    }
    return operator()(os);
}
// -------------------------------------------------------------------------------------------------
int process::operator()(FILE* out)
{
    FILE* handle = global_log ? global_log : out;
    if (handle && rb_out.max_size() == 0)
        rb_out.resize(RB_SIZE_LG);
    for (start(); is_running();) {
        rb_out.read_into(handle);
    }
    return final_check(handle);
}
int process::operator()(std::ostream& os)
{
    if (global_log)
        rb_out.resize(RB_SIZE_LG);
    else if (rb_out.max_size() == 0)
        rb_out.resize(RB_SIZE_LG);
    for (start(); is_running();) {
        if (global_log)
            rb_out.read_into(global_log);
        else
            rb_out.read_into(os);
    }
    return final_check(global_log);
}

// -------------------------------------------------------------------------------------------------
int process::final_check(FILE* out)
{
    FILE* handle = global_log ? global_log : out;
    if (last_ret_val && nzrv_save_stderr && handle)
        rb_err.read_into(handle);
    if (nzrv_exception && last_ret_val != 0) {
        throw rterror("Process: '%s %s' retured: %d", command.get_ccbase(), arguments.get(), last_ret_val);
    }
    return last_ret_val;
}

// -------------------------------------------------------------------------------------------------
void process::stop_daemon()
{
#ifdef C4S_UNITTEST
    fprintf(utlog, "process::stop_daemon - name=%s\n", command.get_ccbase());
#endif
    if (!pid)
        return;
    // Send termination signal.
    if (kill(pid, SIGTERM)) {
        char estr[256];
        snprintf(estr, sizeof(estr), "process::stop: kill(pid,SIGTERM) error:%s", strerror(errno));
#ifdef C4S_UNITTEST
        fputs(estr, utlog);
#endif
        throw c4s_exception(estr);
    }
    // Lets wait for a while:
    struct timespec ts_delay, ts_remain;
    ts_delay.tv_sec = 0;
    ts_delay.tv_nsec = 400000000L;
    int rv, count = 20;
    do {
        nanosleep(&ts_delay, &ts_remain);
        rv = kill(pid, 0);
        count--;
    } while (count > 0 && rv == 0);
    if (proc_started)
        proc_ended = clock();
#ifdef C4S_UNITTEST
    fprintf(utlog, "process::stop_daemon - term result:%d; count=%d; errno=%d; runtime=%f\n",
            rv, count, errno, duration());
#endif
    if (count == 0) {
        count = 10;
        kill(pid, SIGKILL);
        do {
            nanosleep(&ts_delay, &ts_remain);
            rv = kill(pid, 0);
            count--;
        } while (count > 0 && rv == 0);
#ifdef C4S_UNITTEST
        fprintf(utlog, "process::stop_daemon - kill result:%d; count=%d; errno=%d\n", rv, count, errno);
#endif
        if (count == 0)
            throw c4s_exception("process::stop_daemon - Failed, daemon sill running.");
    }
    if (errno != ESRCH) {
#ifdef C4S_UNITTEST
        fputs("process::stop_daemon - error stopping daemon\n", utlog);
#endif
        throw c4s_exception("process::stop_daemon - error stopping daemon");
    }
    pid = 0;
    last_ret_val = 0;
}

// -------------------------------------------------------------------------------------------------
// Private - Linux/Apple only
int process::interpret_process_status(int status)
{
    if (WIFEXITED(status))
        return WEXITSTATUS(status);
    else if (WTERMSIG(status) || WIFSTOPPED(status))
        return -1;
    return 0;
}
// ---------------------------------------------------------------------------------------------------------
/*!
  It is not recommended that this function is used to stop the process since the processes usually
  stop on their own. Use the exec- or wait_for_exit-functions instead. If the wait_for_exit gives
  timeout exception then this function can be called to stop the run-loose process.
*/
void process::stop()
{
    int status;
    char estr[256];
    if (pipes) {
        if (global_log)
            pipes->write_to_file(global_log);
        else {
            if (rb_out.max_size())
                pipes->read_child_stdout(&rb_out);
            if (rb_err.max_size())
                pipes->read_child_stderr(&rb_err);
        }
        //         else if(nzrv_save_stderr && last_ret_val && rb_out.size()) {
        // #ifdef C4S_UNITTEST
        //             fputs("process::stop - error in process. Try to read stderr.\n", utlog);
        // #endif
        //             pipes->read_child_stderr(&rb_out);
        //        }
    }
    if (pid) {
        if (daemon) {
            stop_daemon();
            return;
        }
    AGAIN:
        pid_t cid = waitpid(pid, &status, WNOHANG | WUNTRACED);
        if (cid == 0) {
            if (kill(pid, SIGTERM)) {
                snprintf(estr,
                         sizeof(estr),
                         "Unable to send termination signal to "
                         "running process:%d; errno = %d",
                         pid,
                         errno);
                throw c4s_exception(estr);
            }
            cid = waitpid(pid, &status, WNOHANG | WUNTRACED);
            if (cid == 0) {
                if (kill(pid, SIGKILL)) {
                    snprintf(
                        estr, sizeof(estr), "Unable to kill process %d; errno = %d", pid, errno);
                    throw c4s_exception(estr);
                }
            }
#ifdef C4S_UNITTEST
            fprintf(utlog, "Process::stop - used TERM/KILL to stop %d\n", pid);
#endif
        }
        if (cid == -1) {
#ifdef C4S_UNITTEST
            fprintf(utlog, "process::stop - name=%s, waitpid failed. Errno=%d\n",
                    command.get_ccbase(), errno);
#endif
            if (errno == EINTR)
                goto AGAIN;
            else if (errno == ECHILD) {
                // Process was probably attached and hence this is not a parent for the process.
                snprintf(estr, sizeof(estr), "process::stop: waitpid error: %s", strerror(errno));
                throw c4s_exception(estr);
            } else {
                throw c4s_exception("Process::stop: Invalid argument to wait-function.");
            }
        } else {
            last_ret_val = interpret_process_status(status);
        }
        pid = 0;
    } // if(pid)

    proc_ended = clock();
#ifdef C4S_UNITTEST
    fprintf(utlog, "process::stop - name=%s; runtime=%f\n", command.get_ccbase(), duration());
#endif
    if (pipes) {
        delete pipes;
        pipes = 0;
    }
}

} // namespace c4s
