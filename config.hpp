/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#ifndef C4S_SETTINGS_HPP
#define C4S_SETTINGS_HPP

#include <unordered_map>
#include <vector>
#include <cstdint>

namespace c4s {

namespace config {
class section;
class item;
}; // namespace config

// ...............................................
class config_store
{
public:
    enum class FORMAT { NONE, TOML, JSON };

    config_store();
    config_store(const c4s::path& fname, FORMAT fmt = FORMAT::NONE );
    ~config_store();

    bool read_toml(const c4s::path& fname);
    bool write_toml(const c4s::path& fname, const char* title = nullptr);
    bool read_json(const c4s::path& fname);

    config::section* get_section(const ntbs& name);

    bool has(const ntbs& section, const ntbs& name)
    {
        return find(section, name) == nullptr ? false : true;
    }
    const char* get_string(const ntbs& section, const ntbs& name);
    bool get_value(const ntbs& section, const ntbs& name, ntbs& val);
    bool get_value(const ntbs& section, const ntbs& name, uint64_t& val);
    bool get_value(const ntbs& section, const ntbs& name, float& val);
    bool get_value(const ntbs& section, const ntbs& name, bool& val);
    bool is(const ntbs& section, const ntbs& name);

    std::list<config::section*> sections;

protected:
    config::item* find(const ntbs& section, const ntbs& name);
    int conf_line;
};

namespace config {

enum class TYPE { STR, LONG, FLOAT, BOOL };

class item
{
    friend class section;

public:
    item(TYPE _t)
        : type(_t)
    {
    }
    virtual ~item() {}
    TYPE get_type() { return type; }
    virtual void get_text(ntbs& str) const = 0;

private:
    TYPE type;
};

typedef std::unordered_map<ntbs, config::item*> setting_map;

// ...............................................
class str_item : public item
{
public:
    str_item(const ntbs& val)
        : item(TYPE::STR)
        , value(val)
    {
    }
    str_item(const str_item& original)
        : item(TYPE::STR)
    {
        value = original.value;
    }
    void get_text(ntbs& str) const { str = value; }
    ntbs_md value;
};
// ...............................................
class integer_item : public item
{
public:
    integer_item(uint64_t val)
        : item(TYPE::LONG)
        , value(val)
    {
    }
    integer_item(const integer_item& original)
        : item(TYPE::LONG)
    {
        value = original.value;
    }
    void get_text(ntbs& str) const { str.sprint("%ld", value); }
    uint64_t value;
};
// ...............................................
class float_item : public item
{
public:
    float_item(float val)
        : item(TYPE::FLOAT)
        , value(val)
    {
    }
    float_item(const float_item& original)
        : item(TYPE::FLOAT)
    {
        value = original.value;
    }
    void get_text(ntbs& str) const { str.sprint("%f", value); }
    float value;
};
// ...............................................
class bool_item : public item
{
public:
    bool_item(bool val)
        : item(TYPE::BOOL)
        , value(val)
    {
    }
    bool_item(const bool_item& original)
        : item(TYPE::BOOL)
    {
        value = original.value;
    }
    void get_text(ntbs& str) const { str = (value ? "true" : "false"); }
    bool value;
};

// ...............................................
//! Parse time data structure
struct parse_data
{
    parse_data() {
        pos = 0;
        read_level = 0;
        array_item = -1;
        line = 0;
    }
    void level_up() {
        key += '.';
        read_level++;
    }
    void level_down() {
        key.pop_back();
        read_level--;
    }
    bool is_array() {
        return array_item >= 0 ? true : false;
    }
    ntbs get_key() {
        if (array_item >= 0) {
            ntbs_sm kai;
            kai.sprint("%s[%d]", key.get(), array_item);
            return kai;
        } else
            return key;
    }
    size_t pos;
    size_t line;
    ntbs key;
    int read_level;
    int array_item;
};
// ...............................................
class section
{
    friend class c4s::config_store;

public:
    section();
    section(const ntbs& name);
    ~section();

    void get_subkeys(const ntbs& name, std::vector<ntbs>& keys);
    void get_values(const ntbs& name, std::vector<ntbs>& values);

    ntbs name;
    setting_map items;

protected:
    bool read_toml(FILE *handle, parse_data& pd);
    bool read_json(FILE* handle, parse_data& pd);
    bool parse_value(ntbs& key, ntbs& value);
};

}; // namespace config

}; // namespace c4s

inline std::ostream& operator<<(std::ostream& os, const c4s::config::item* im)
{
    c4s::ntbs_md value;
    im->get_text(value);
    os << value;
    return os;
}

#endif
