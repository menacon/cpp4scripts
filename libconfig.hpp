/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#ifndef CPP4SCRIPTS_CONFIG
#define CPP4SCRIPTS_CONFIG

/* If log macros are used then log statements can be removed from executable at the compile time.
   The higher the level number, less messages are displayed. 1 = trace ... 7 critical. Set
   C4S_LOG_LEVEL define to 0 to include all logs to build and to 8 to remove all from binary.*/
#ifndef C4S_LOG_LEVEL
#ifdef NDEBUG
#define C4S_LOG_LEVEL 2
#else
#define C4S_LOG_LEVEL 1
#endif
#endif

/* Specifies the internal buffer size for logger::vaprt function.*/
#ifndef C4S_LOG_VABUFFER_SIZE
#define C4S_LOG_VABUFFER_SIZE 0x800
#endif

/* Size of static full path buffer for c4s::path class. */
#ifndef C4S_FULLPATH
#define C4S_FULLPATH 256
#endif

#ifndef C4S_BUILD_RECURSIVE_CHECK_MAX
#define C4S_BUILD_RECURSIVE_CHECK_MAX 2
#endif

/* Size of the static buffer reserved for c4s exception messages. */
#ifndef C4S_EXCEPTION_BUFFER_SIZE
#define C4S_EXCEPTION_BUFFER_SIZE 1024
#endif

/* Max number of bytes to read in std::operator>> override */
#ifndef C4S_NTBS_MAX_ISTREAM_READ
#define C4S_NTBS_MAX_ISTREAM_READ 512
#endif

/* For convenience really: */
typedef const char* ccp;

#include <stdint.h>

#include <errno.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>
#define HANDLE pid_t
#define C4S_DSEP '/'
#define C4S_PSEP ':'
#define C4S_QUOT '\''
#define SSIZE_T ssize_t
#define SIZE_T size_t

namespace c4s {

// Library constants
const SSIZE_T SSIZE_T_MAX = ~0;
const size_t RB_SIZE_SM = 0x1000;
const size_t RB_SIZE_LG = 0x4000;
const int MAX_NESTING = 50;
const int BUILDER_TIMEOUT = 45;
const uint64_t FNV_1_PRIME = 0x84222325cbf29ce4UL;
const int MAX_PROCESS_ARGS = 100;

} // namespace c4s

#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <sstream>
#include <stdexcept>
#include <unordered_map>

#endif // CPP4SCRIPTS_CONFIG
