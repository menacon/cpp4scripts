/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#ifndef C4S_TM_DATETIME_HPP
#define C4S_TM_DATETIME_HPP

#include <cstring>

namespace c4s {

class tm_datetime
{
public:
    tm_datetime() { clear(); }
    tm_datetime(const tm_datetime& orig) { std::memcpy(&dtv, &orig.dtv, sizeof(struct tm)); }
    tm_datetime(time_t* tt) { localtime_r(tt, &dtv); }
    tm_datetime(struct tm* stm) { std::memcpy(&dtv, stm, sizeof(struct tm)); }
    tm_datetime(const char* str, const char* lang = nullptr) { parse(str, lang); }
    tm_datetime(int year, int month);
    void operator=(const tm_datetime& orig) { std::memcpy(&dtv, &orig.dtv, sizeof(struct tm)); }
    void operator=(time_t* tt) { localtime_r(tt, &dtv); }
    void operator=(struct tm* source) { std::memcpy(&dtv, source, sizeof(struct tm)); }
    void operator=(const char* str) { parse(str); }

    void cp(struct tm* target) const { std::memcpy(target, &dtv, sizeof(struct tm)); }
    void cp(tm_datetime& target) const { std::memcpy(&target.dtv, &dtv, sizeof(struct tm)); }
    bool parse(const char*, const char* lang = nullptr);
    bool parseISO(const char*);
    bool parseSql(const char*);
    int compare(tm_datetime& tg);
    int compare(time_t tg);

    void now();
    void thisMonth(); // current month with day=1 and time 00:00
    void nextMonth(int moff = 1);
    void setDay1();
    void setLastDay();

    int getYear() { return dtv.tm_year + 1900; }
    int getMonth() { return dtv.tm_mon + 1; }
    const char* print(const char* lang = nullptr);
    const char* printShort(const char* lang = nullptr);
    const char* printTime(const char* lang = nullptr);
    const char* printTimeShort(const char* lang = nullptr);
    const char* printYear();
    const char* printHourMinutes();
    const char* printISODate();
    const char* printISOTimestamp();
    const char* printDBDate();
    const char* printDBTimestamp();

    bool empty() const { return dtv.tm_year == 0 ? true : false; }
    bool isValid() { return dtv.tm_mday > 0 ? true : false; }
    void clear()
    {
        memset(&dtv, 0, sizeof(struct tm));
        memset(tstr, 0, sizeof(tstr));
    }

    struct tm dtv;

protected:
    char tstr[24];
};

} // namespace c4s

#endif