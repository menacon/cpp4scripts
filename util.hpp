/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#ifndef C4S_UTIL_HPP
#define C4S_UTIL_HPP

#include <string>
#include <unordered_map>
#include <stdint.h>

#include "ntbs.hpp"

namespace c4s {

class path;

// inline void cp(const char *from, const char *to) { path source(from); source.cp(to); }
// inline void cp_to_dir(const char *from, path &to, path::cpf flags=path::NONE) { path
// source(from); source.cp(to,flags); }
enum DATETYPE
{
    DATE_ONLY,
    WITH_TIME
};
bool exists_in_path(const char* file);
#ifdef _WIN32
//! Provides error string output from Windows system with the same name as the linux 'strerror'
//! fuction.
const char* strerror(int);
#endif
//! Returns a string designating the build type. Eg '64bit-Release'
const char* get_build_type();

//! Returns current library version as text
const char* get_c4s_version();

//! Returns todays timestamp in ISO format
const char* get_ISO_date(DATETYPE);

//! Makes sure that the path separators int the given target string are native type.
ntbs force_native_dsep(const ntbs&);

//! Gets an environment value.
bool get_env_var(const char*, ntbs&);

//! Merges given environment strings with current environment. Caller must delete allocated array.
const char** merge_environment(const char** new_items);
void delete_environment(const char** env);

//! Matches the wildcard to the string and returns true if it succeeds.
bool match_wildcard(const char* pWild, const char* pString);

//! Returns users login or real name as configured to the system.
ntbs get_user_name(bool loginname = true);

//! Returns current hostname
const char* get_host_name();

//! Efficient string search function.
bool search_bmh(const unsigned char* haystack,
                size_t hlen,
                const unsigned char* needle,
                size_t nlen,
                size_t* offset_out);

//! Efficient hash algorithm: http://www.isthe.com/chongo/src/fnv/hash_64.c
uint64_t fnv_str_hash64(const char* str, size_t len, uint64_t salt=0);

//! Waits for input on stdin
bool wait_stdin(int timeout);

//! Appends a slash tot he end of the ntbs if it does not have one
void append_slash(ntbs&);

//! Creates a 'next' available filename into the base part based on given wild card.
bool generate_next_base(path& target, const char* wild);

#if defined(__linux) || defined(__APPLE__)
//! Maps the mode from numeric hex to linux symbolic
mode_t hex2mode(int);

//! Maps the mode from linux symbolic into numeric hex
int mode2hex(mode_t);

//! Reads the current file mode from named file.
int get_path_mode(const char* pname);

//! Sets owner and mode recursively to entire subtree. Use with care. (Linux&Apple only)
void set_owner_mode(const char* dirname, int userid, int groupid, int dirmode, int filemode);
#endif

bool has_anybits(uint32_t target, uint32_t bits);
bool has_allbits(uint32_t target, uint32_t bits);

//! Parse given string into map
bool parse_key_values(const char* str, std::unordered_map<ntbs, ntbs>& kv, char separator = ',');

inline size_t min_size(size_t val_a, size_t val_b)
{
    return val_a < val_b ? val_a : val_b;
}
inline size_t max_size(size_t val_a, size_t val_b)
{
    return val_a > val_b ? val_a : val_b;
}


typedef unsigned int flag32;

class flags32_base
{
public:
    flags32_base(flag32 _value)
        : value(_value)
    {
    }
    flags32_base()
        : value(0)
    {
    }

    flag32 get() { return value; }
    bool has_any(flag32 bits) { return (value & bits) > 0 ? true : false; }
    bool has_all(flag32 bits) { return (value & bits) == bits ? true : false; }
    void set(flag32 bits) { value = bits; }
    void clear(flag32 bits) { value &= ~bits; }

    void add(flag32 bits) { value |= bits; }
    flag32& operator|=(flag32 bits)
    {
        value |= bits;
        return value;
    }
    bool operator==(flag32 bits) { return value == bits; }
    bool operator!=(flag32 bits) { return value != bits; }

protected:
    flag32 value;
};

} // namespace c4s
#endif
