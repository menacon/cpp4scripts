/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#ifndef C4S_FILE_HPP
#define C4S_FILE_HPP

#include <stdio.h>

namespace c4s {

class ntbs;
class path;

class file
{
public:
    file(const c4s::path& name);
    ~file() { if (handle) fclose(handle); }

    FILE* get_handle() { return handle; }

    size_t read_into(c4s::ntbs buffer, size_t count) const;
    size_t write_from(const c4s::ntbs buffer, size_t count = 0) const;
    size_t append_from(const c4s::ntbs buffer, size_t count = 0) const;
    uint64_t fnv_hash64(uint64_t salt) const;
    bool search(const ntbs& needle) const;
    //! Performs a search-replace for a file pointed by this path
    int search_replace(const ntbs& search, const ntbs& replace, bool backup = false);
    //! Performs a single block replacement in a file pointed by this path.
    bool replace_block(const ntbs&, const ntbs&, const ntbs&, bool backup = false);

protected:
    void backup_rename(path&, bool backup);
    path fname;
    FILE* handle;
};

}; // namespace c4s
#endif
