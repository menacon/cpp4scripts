/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#ifndef C4S_BUILDER_HPP
#define C4S_BUILDER_HPP

#include "util.hpp"

namespace c4s {

class path_list;
class compiled_file;
class process;

enum class BUILD_STATUS : int {OK, TIMEOUT, ERROR, ABORTED, NOTHING_TO_DO};
enum class BUILD_TYPE : int {BIN, SO, LIB};

class BUILD_OPT : public flags32_base
{
public:
    static const flag32 NONE       = 0x000; //!< No flags.
    static const flag32 EXPORT     = 0x001; //!< Build prepares options for export and skips build step.
    static const flag32 VERBOSE    = 0x002; //!< Verbose build, shows more output
    static const flag32 RESPFILE   = 0x004; //!< Puts arguments into response file before compiling or linking.
    static const flag32 NOLINK     = 0x010; //!< Only compilation is done at build-command.
    static const flag32 NOINCLUDES = 0x020; //!< Don't check includes for oudated status
    static const flag32 FORCELINK  = 0x040; //!< Do link step even if no outdated files found.

    BUILD_OPT()
        : flags32_base(NONE)
    {
    }
    explicit BUILD_OPT(flag32 fx)
        : flags32_base(fx)
    {
    }
};

/** An abstract builder class that defines an interface to CPP4Scripts builder feature. This is a
    common base class for all specific compilers. It collects options, constructs the binary and
    build directory name and has the build-function interface.
*/
class builder : public BUILD_OPT
{
public:
    // Destructor, releases source list if it was reserved during build process.
    virtual ~builder() {}

    //! Add list of source to existing file list
    void add_sources(const path_list& src) { sources.add(src); }
    //! Add files reported by 'git ls-files' command
    void add_git_files();
    //! Adds more compiler options to existing ones.
    void add_comp(const char* arg);
    void add_comp(const ntbs& arg);

    //! Adds more linker/librarian options to existing ones.
    void add_link(const char* arg);
    void add_link(const ntbs& arg);
    //! Adds a compiled file for linking
    void add_link(const compiled_file& cf);
    //! Adds more object files to be linked.
    void add_link(const path_list& src, const char* obj)
    {
        extra_obj.add(src, build_dir.get(), obj);
    }

    //! Prints current options into given file
    void print(FILE* out, bool list_sources = false);
    //! Returns the padded name.
    ntbs& get_name() { return name; }
    //! Returns the padded name, i.e. with system specific extension and possibly prepended 'lib'
    ntbs& get_target_name() { return target; }
    //! Returns relative target path, i.e. padded name with prepended build_dir
    path get_target_path() { return path(build_dir.get(), target.get()); }
    //! Returns generated build directory name. Note: name does not have dir-separator at the end.
    ntbs& get_build_dir() { return build_dir; }
    //! Sets the build directory, required before compilation.
    void set_build_dir(const ntbs& src);

    //! Generates compiler_commands.json.
    void export_compiler_commands(c4s::path& exp, const c4s::path& src);
    //! Generates cmake CMakeLists.txt.
    void export_cmake(c4s::path& dir);
    //! Generic export creating 'export' directory with export files.
    //! Calls specific exports based on type parameter [cmake|ccdb]
    void export_prj(const ntbs& type, c4s::path& exp, const c4s::path& ccd);

    //! Template for the build command
    virtual BUILD_STATUS build() = 0;
    //! Sets default debug parameters or overrides them with users choises
    virtual void set_debug_params(const char* params = nullptr) = 0;

    //! Increments the build number in the given file
    static int update_build_no(const char* filename);
    //! Shorthand logic to determine outcome of build/compile
    static bool is_fail_status(BUILD_STATUS bs)
    {
        if (bs == BUILD_STATUS::TIMEOUT || bs == BUILD_STATUS::ERROR)
            return true;
        return false;
    }

protected:
    //! Initializes the builder
    builder(const ntbs& name, BUILD_TYPE, const char *_compiler, const char *_linker, FILE* log);
    //! Executes compile step
    BUILD_STATUS compile(const char* out_ext, const char* out_arg);
    //! Executes link/library step.
    BUILD_STATUS link(const char* out_ext, const char* out_arg);
    //! Check if includes have been changed for named source (or include)
    bool check_includes(c4s::path& obj, c4s::path& source, ntbs& offender, int rlevel = 0);
    void generate_target_name();

    BUILD_TYPE   type;         //!< Resulting executable type.
    c4s::process compiler;     //!< Compiler process for this builder.
    c4s::process linker;       //!< Linker process for this builder.
    std::ostringstream c_opts; //!< List of options for the compiler.
    std::ostringstream l_opts; //!< List of options for the linker.
    FILE* log;         //!< If not null, will receive compiler and linker output (stderr)
    c4s::path_list sources;    //!< List of source files. Relative paths are possible.
    c4s::path_list extra_obj;  //!< Optional additional object files to be included at link step.
    ntbs name;                 //!< Simple target name.
    ntbs target;               //!< Decorated and final target name.
    ntbs build_dir;            //!< Generated build directory name. No dir-separater at the end.
    ntbs ccdb_root;            //!< Root directory for compiler_commands.json generation.
};

} // namespace c4s

#endif