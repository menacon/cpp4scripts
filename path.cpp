/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#include <stdio.h>
#include <string.h>
#include <iostream>

#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "libconfig.hpp"
#include "exception.hpp"
#include "ntbs.hpp"
#include "path.hpp"
#include "path_list.hpp"
#include "user.hpp"
#include "util.hpp"
#include "logger.hpp"

#ifdef C4S_UNITTEST
extern FILE* utlog;
#endif

namespace c4s {

// -------------------------------------------------------------------------------------------------
void path::init_common()
{
    change_time = 0;
    flag = false;
    owner = 0;
    mode = -1;
}
// -------------------------------------------------------------------------------------------------
path::path()
{
    init_common();
}
// -------------------------------------------------------------------------------------------------
path::path(const path& _dir, const char* _base, user* _owner, int _mode)
{
    init_common();
    dir = _dir.dir;
    if (_base)
        base = _base;
    if (_owner)
        owner = _owner;
    else
        owner = _dir.owner;
    if (_mode)
        mode = _mode;
    else
        mode = _dir.mode;
}
// -------------------------------------------------------------------------------------------------
/** If string has dir separator as last character then the base is empty. If no directory
  separators are detected then the dir is empty.
  \param init Path name to initialize the object with. String can be a file name, path or
  combination of both i.e. full path.  */
path::path(const ntbs& p)
{
    init_common();
    set(p);
}
// -------------------------------------------------------------------------------------------------
path::path(const ntbs& d, const ntbs& b, const ntbs& e)
{
    init_common();
    set(d, b, e);
}
// -------------------------------------------------------------------------------------------------
path::path(const ntbs& d, const ntbs& b)
{
    init_common();
    set(d, b);
}
// -------------------------------------------------------------------------------------------------
path::path(const ntbs& d, const ntbs& b, user* o, int m)
{
    init_common();
    set(d, b);
    owner = o;
    mode = m;
}
path::path(const ntbs& p, user* o, int m)
{
    init_common();
    set(p);
    owner = o;
    mode = m;
}
// -------------------------------------------------------------------------------------------------
void path::operator=(const path& p)
{
    dir = p.dir;
    base = p.base;
    change_time = p.change_time;
    flag = p.flag;
    owner = p.owner;
    mode = p.mode;
}
// -------------------------------------------------------------------------------------------------
/**
  \param vs Reference to an array of strings where the directory parts are filled into.
 */
void path::get_dir_parts(std::vector<ntbs>& vs) const
{
    ntbs_sm part;
    size_t pt = 1, prev = 1;
    if (!dir.length())
        return;

    char* ptr = dir.get() + 1;
    while (*ptr) {
        if (*ptr == C4S_DSEP) {
            dir.substr(prev, pt - prev, part);
            vs.push_back(part);
            prev = pt + 1;
        }
        ptr++;
        pt++;
    }
}
// -------------------------------------------------------------------------------------------------
void path::get_path(ntbs& full)
{
    full = dir;
    full += base;
}
// -------------------------------------------------------------------------------------------------
ntbs path::get_path_quot() const
{
    ntbs copy(dir.size() + base.size() + 2);
    bool quotes = false;
    if ((!dir.empty() && dir.find(' ') != SIZE_MAX) ||
        (!base.empty() && base.find(' ') != SIZE_MAX))
        quotes = true;
    if (quotes)
        copy[0] = C4S_QUOT;
    copy += dir.get();
    copy += base.get();
    if (quotes)
        copy += C4S_QUOT;
    return copy;
}
// -------------------------------------------------------------------------------------------------
const char* path::get_ccpath() const
{
    static ntbs ps(C4S_FULLPATH);
    ps = dir;
    ps += base;
    return ps.get();
}
// -------------------------------------------------------------------------------------------------
/** If extension is given it is exchanged with the base's current extension or then appended to
  base name if it currently does not have extension.

  \param ext Extension for the name. Include the '.' separator to the beginning of extension.
  \retval string Resulting base name.
*/
ntbs path::get_base(const ntbs& ext) const
{
    if (ext.empty())
        return base;
    size_t loc = base.rfind('.');
    if (loc == SIZE_MAX)
        return base + ext;
    return base.substr(0, loc) + ext;
}
// -------------------------------------------------------------------------------------------------
/**
  \retval String Base without extension
*/
ntbs path::get_base_plain() const
{
    size_t extOffset = base.rfind('.');
    if (extOffset == SIZE_MAX)
        return base;
    return base.substr(0, extOffset);
}
// -------------------------------------------------------------------------------------------------
ntbs path::get_base_or_dir()
{
    if (!base.empty())
        return base;
    size_t loc = dir.rfind(C4S_DSEP);
    if (loc == SIZE_MAX)
        return dir;
    return dir.substr(loc + 1);
}
// -------------------------------------------------------------------------------------------------
/**
  \retval string Extension of the file name part. Empty string is returned if extension does not
  exist.
*/
ntbs path::get_ext() const
{
    size_t extOffset = base.rfind('.');
    if (extOffset != SIZE_MAX)
        return base.substr(extOffset);
    return ntbs();
}
// -------------------------------------------------------------------------------------------------
/** If the string ends with directory separator string is copied into dir and base is left
  empty. If the string does not have any directory separators string is copied to base and dir is
  left epty. Otherwice the string after the last directory separator is taken as base and
  beginning is taken as directory.

  \param init String to initialize with.
*/
void path::set(const ntbs& source)
{
    change_time = 0;
    size_t last = source.rfind(C4S_DSEP);
    if (last == SIZE_MAX) {
        dir.clear();
        base = source;
        return;
    }
    size_t len = source.length();
    if (last == len) {
        base.clear();
        set_dir(source);
        return;
    }
    last++;
    dir.assign(source, 0, last);
    base.assign(source, last, len - last);
}
// -------------------------------------------------------------------------------------------------
/**
  \param dir_in Directory name.
  \param base_in Base name (=file name)
*/
void path::set(const ntbs& dir_in, const ntbs& base_in)
{
    change_time = 0;
    set_dir(dir_in);
    base = base_in;
}
// -------------------------------------------------------------------------------------------------
/**
   \param dir_in Directory name.
   \param base_in Base name (=file name) with option exception.
   \param ext Extension. If base has extension then it is changed to this extension. Parameter is
   optional. Defaults to null.
*/
void path::set(const ntbs& dir_in, const ntbs& base_in, const ntbs& ext)
{
    change_time = 0;
    set_dir(dir_in);
    base = base_in;
    if (base.find('.') == SIZE_MAX)
        base += ext;
    else
        base = get_base(ext);
}
// -------------------------------------------------------------------------------------------------
/** Replaces the current directory part of this path. The directory separator is added to the end
  of given string if it is missing.
  \param new_dir New directory for path.
*/
void path::set_dir(const ntbs& new_dir)
{
    change_time = 0;
    if (new_dir.empty())
        return;
    dir = new_dir;
    append_slash(dir);
}
// -------------------------------------------------------------------------------------------------
void path::set_dir2home()
{
    ntbs_md home;
    if (!get_env_var("HOME", home)) {
        throw rterror("path::set_dir2home error: Unable to find HOME environment variable");
    }
    dir = home;
    dir += C4S_DSEP;
    change_time = 0;
}
// -------------------------------------------------------------------------------------------------
/**
  \param newb New base. If empty the base is cleared.
*/
void path::set_base(const ntbs& newb)
{
    change_time = 0;
    if (newb.empty()) {
        base.clear();
        return;
    }
    base = newb;
}
// -------------------------------------------------------------------------------------------------
/**
  If given ext is empty then the extension is cleared.
  \param ext New extension string.
*/
void path::set_ext(const ntbs& ext)
{
    if (base.empty())
        return;
    size_t extOffset = base.rfind('.');
    if (extOffset != SIZE_MAX)
        base.erase(extOffset);
    if (ext.empty())
        return;
    base += ext;
    change_time = 0;
}
// -------------------------------------------------------------------------------------------------
void path::base_append(const char* str)
{
    if (base.empty() || !str)
        return;
    if (base[0] == '.') {
        base += str;
        return;
    }
    size_t extOffset = base.rfind('.');
    if (extOffset == SIZE_MAX)
        base += str;
    else
        base.insert(extOffset, ntbs(str));
    change_time = 0;
}
// -------------------------------------------------------------------------------------------------
void path::base_prepend(const char* str)
{
    if (base.empty() || !str)
        return;
    base.prepend(str);
    change_time = 0;
}
// -------------------------------------------------------------------------------------------------
/**
  \param to Directory to change to.
  \retval True on success, false on failure.
*/
void path::cd(const char* to)
{
    if (!to || to[0] == 0)
        return;
    if (chdir(to)) {
        throw rterror("path::cd failed");
    }
}
// -------------------------------------------------------------------------------------------------
void path::read_cwd()
{
    char chCwd[512];
    if (!getcwd(chCwd, sizeof(chCwd)))
        throw rterror("Unable to get current dir");
    dir = chCwd;
    dir += C4S_DSEP;
}
// -------------------------------------------------------------------------------------------------
/** Checks the status of the path's owner.
 *  \retval OWNER
 */
OWNER_STATUS
path::owner_status()
{
    struct stat dsbuf;
    if (!owner)
        return OWNER_STATUS::EMPTY;
    if (!owner->is_ok())
        return OWNER_STATUS::MISSING;
    if (stat(get_dir_plain().get(), &dsbuf))
        return OWNER_STATUS::NOPATH;
#ifndef C4S_UNITTEST
    if (owner->match(dsbuf.st_uid, dsbuf.st_gid)) {
        if (mode >= 0) {
            ntbs fp = base.empty() ? get_dir_plain() : get_path();
            if (get_path_mode(fp.get()) == mode)
                return OWNER_STATUS::OK;
            else
                return OWNER_STATUS::NOMATCH_MODE;
        }
        return OWNER_STATUS::OK;
    }
#endif
    return OWNER_STATUS::NOMATCH_UG;
}
// -------------------------------------------------------------------------------------------------
/** Reads the current owner of the path on disk and loads this path with that particular user.
  If path does not exist an exception is thrown.
*/
void path::owner_read()
{
    struct stat dsbuf;
    if (!exists())
        throw rterror("Cannot read owner for non-existing path.");
    if (!owner)
        throw rterror("Cannot read owner into null.");
    ntbs fp = base.empty() ? get_dir_plain() : get_path();
    if (stat(fp.get(), &dsbuf))
        throw rterror("Unable to get ownership for file:%s. Error:%s", get_ccpath(), strerror(errno));
#ifndef C4S_UNITTEST
    owner->set(dsbuf.st_uid, dsbuf.st_gid);
#endif
}
// -------------------------------------------------------------------------------------------------
/** If current user does not have a permission to do so, an exception is thrown. File mode
  needs to be set separately.
*/
void path::owner_write()
{
    if (!owner)
        throw rterror("Cannot write non-existing owner.");
    if (!owner->is_ok())
        throw rterror("Both user and group must be defined to write file ownership to:%s- user:%s - group:%s",
            get_ccpath(),
            owner->get_ccname(),
            owner->get_ccgroup());
    if (!exists())
        throw rterror("Cannot write owner for non-existing path");
    ntbs fp = base.empty() ? get_dir_plain() : get_path();
    if (chown(fp.get(), owner->get_uid(), owner->get_gid()))
        throw rterror("Unable to set path owner for %s%s  - system error: %s",
                    dir.get(), base.get(), strerror(errno));
}
// -------------------------------------------------------------------------------------------------
/** Reads current path mode from file system.
 */
void path::read_mode()
{
    ntbs fp = base.empty() ? get_dir_plain() : get_path();
    int pm = get_path_mode(fp.get());
    if (pm >= 0)
        mode = pm;
}
// -------------------------------------------------------------------------------------------------
bool path::is_absolute() const
{
    if (dir.length() && dir.get(0) == '/')
        return true;
    return false;
}
// -------------------------------------------------------------------------------------------------
void path::make_absolute()
{
    if (is_absolute())
        return;

    ntbs_xl cwd;
    if (!getcwd(cwd.get(), cwd.max_size()))
        throw rterror("Unable to get current dir - %s", strerror(errno));
    cwd.reset();
    cwd += '/';
    if (dir.empty()) {
        dir = cwd;
        return;
    }
    if (dir[0] == '.' && dir[1] == '.') {
        size_t dirIndex = 1;
        size_t slash = cwd.length() - 2;
        do {
            slash = cwd.rfind(C4S_DSEP, slash - 1);
            dirIndex += 3;
        } while (dirIndex < dir.length() && dir[dirIndex] == '.' && slash != SIZE_MAX);
        if (slash == SIZE_MAX)
            throw rterror("Incorrect reletive path %s detected for %s",
                     dir.get(),
                     cwd.get());
        dir = cwd.substr(0, slash + 1) + dir.substr(dirIndex - 1);
    } else if (dir[0] == '.') {
        dir.replace(0, 2, cwd);
    } else {
        cwd += dir;
        dir = cwd;
    }
}
// -------------------------------------------------------------------------------------------------
/**  Root is expected to be absolute directory and this path relative. If this path is absolute
  the current dir is simply replaced with root.  Otherwise this relative dir is added into the
  root.
  \param root Source full / absolute directory.
*/
void path::make_absolute(const ntbs& root)
{
    // If absolute - replace dir and return
    if (is_absolute()) {
        set_dir(root);
        return;
    }
    // Roll down possible parent directory markers.
    size_t offset = root.length() - 1;
    int count = 0;
    while (dir.find("..", count) == 0) {
        offset = root.rfind(C4S_DSEP, offset - 1);
        count += 3;
    }
    // Append the remaining dir to rolled down root
    ntbs tmp(root);
    tmp.replace(offset + 1, root.length() - offset + 1, dir);
    dir = tmp;
}
// -------------------------------------------------------------------------------------------------
void path::make_relative()
{
    path parent;
    parent.read_cwd();
    make_relative(parent);
}
// -------------------------------------------------------------------------------------------------
/**  If parent is longer than this directory or if the parent is not found from this directory
  the function does nothing. The './' is NOT added to the front.

  \param parent Parent part of the directory name is removed from this path.
*/
void path::make_relative(const path& parent)
{
    size_t pos = dir.find(parent.dir);
    if (pos > 0 || pos == SIZE_MAX)
        return;
    dir.erase(0, parent.dir.size());
}
// -------------------------------------------------------------------------------------------------
/** If count is larger than directories in path then the dir part is left empty.

  \param count Number of directories to drop from the end of the dir tree.
*/
void path::rewind(int count)
{
    size_t offset = dir.length();
    if (offset == 0)
        return;
    change_time = 0;
    offset--;
    for (int i = 0; i < count; i++) {
        offset--;
        offset = dir.rfind('/', offset);
        if (offset == SIZE_MAX) {
            dir.clear();
            return;
        }
    }
    dir.erase(offset + 1);
}
// -------------------------------------------------------------------------------------------------
/**  First base is directly copied from append-path. Then the directories are merged. If append
  directory is absolute it is simply copied over this dir.  If append directory has .. - this
  dir is rolled down untill no .. exist and then remaining append dir is appended. Otherwice
  the the append directory is appended to this dir.

  \param append Path to append to this one.
*/
void path::merge(const path& append)
{
    change_time = 0;
    if (!append.base.empty())
        base = append.base;
    if (append.is_absolute()) {
        dir = append.dir;
        return;
    }
    if (append.dir.empty())
        return;
    if (append.dir[0] != '.') {
        dir += append.dir;
        return;
    }
    if (append.dir[1] == C4S_DSEP) {
        dir += append.dir.substr(2);
        return;
    }
    // Roll down this path directories
    const char* ad_ptr = append.dir.get();
    while (size_t(ad_ptr - append.dir.get()) < append.dir.length() && !strncmp(ad_ptr, "../", 3)) {
        dir.trim_end(dir.length() - (dir.rfind(C4S_DSEP, 1) + 1));
        ad_ptr += 3;
    }
    // Append the remaining dir to rolled down dir
    dir += ad_ptr;
}
// -------------------------------------------------------------------------------------------------
bool path::dirname_exists() const
{
    struct stat file_stat;
    if (!stat(get_dir_plain().get(), &file_stat)) {
        if (S_ISDIR(file_stat.st_mode))
            return true;
    }
    return false;
}
// -------------------------------------------------------------------------------------------------
/**
  \param target Path to compare to
  \param option Compare option: CMP_DIR compares directory parts only, CMP_BASE compares bases only.
                combine options to compare entire path.
 */
int path::compare(const path& target, unsigned char option) const
{
    if ((option & (CMP_DIR | CMP_BASE)) == 0)
        return 0;
    if ((option & CMP_DIR) > 0) {
        if ((option & CMP_BASE) > 0)
            return get_path().compare(target.get_path());
        return dir.compare(target.dir);
    }
    return base.compare(target.base);
}
// -------------------------------------------------------------------------------------------------
/**  If the dir is relative the dir is first translated to absolute path using current
  directory as reference point. Function is able to create entire tree specified by this
  path.
  \param l_mode     if specified will be used as mode when directory is created.
  \param l_owner    if specified will be used as directory owner when it is created.
*/
void path::mkdir(user* l_owner, int l_mode) const
{
    ntbs fullpath;
    path mkpath;
    if (is_absolute())
        fullpath = get_dir();
    else {
        path tmp(*(const path*)this);
        tmp.make_absolute();
        fullpath = tmp.get_dir();
    }
    size_t offset = 0;
    do {
        offset = fullpath.find(C4S_DSEP, offset + 1);
        mkpath.dir = (offset == SIZE_MAX) ? fullpath : fullpath.substr(0, offset + 1);
        if (!mkpath.dirname_exists()) {
            if (::mkdir(mkpath.get_dir().get(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1) {
                throw rterror("path::mkdir - Unable to create directory: %s",
                         mkpath.get_dir().get());
            }
            if (l_owner) {
                if (!l_owner->is_ok())
                    throw rterror("path::mkdir - owner specified but it is not valid in the system");
                if (chown(mkpath.get_dir_plain().get(), l_owner->get_uid(), l_owner->get_gid())) {
                    throw rterror("path::mkdir - Unable to set path owner for: %s - system error: %s",
                             mkpath.get_dir_plain().get(),
                             strerror(errno));
                }
            }
            if (l_mode != -1) {
                int new_mode = l_mode;
                if ((l_mode & 0x400) > 0)
                    new_mode |= 0x100;
                if ((l_mode & 0x40) > 0)
                    new_mode |= 0x10;
                if ((l_mode & 0x4) > 0)
                    new_mode |= 0x1;
                mkpath.chmod(new_mode);
            }
        }
    } while (offset != SIZE_MAX);
}
// -------------------------------------------------------------------------------------------------
/**  Base name is ignored. If recursive is not set then the exception is thrown if the
  directory is not empty. If directory is not found this function does nothing.
  \param recursive If true then the directory is deleted recursively. USE WITH CARE!
*/
void path::rmdir(bool recursive) const
{
    if (!::rmdir(dir.get()))
        return;
    if (errno == ENOENT)
        return;
    if (errno != ENOTEMPTY)
        throw rterror("path::rmdir - failed on directory: %s\n%s",
                 dir.get(),
                 strerror(errno));
    if (!recursive)
        throw rterror("path::rmdir - Directory to be removed is not empty: %s",
                 dir.get());
    // Open the directory
    DIR* target_dir = opendir(dir.get());
    if (!target_dir)
        throw rterror("path::rmdir - Unable to access directory: %s\n%s",
                 dir.get(),
                 strerror(errno));

    char filename[512];
    struct dirent* de = readdir(target_dir);
    while (de) {
        strcpy(filename, dir.get());
        strcat(filename, de->d_name);
        if (strcmp(de->d_name, ".") && strcmp(de->d_name, "..")) {
            if (de->d_type == DT_DIR) {
                strcat(filename, "/");
                path child(filename);
                child.rmdir(true);
            } else {
                if (unlink(filename) == -1) {
                    throw rterror("path::rmdir - Unable to delete file from rmdir target: %s\n%s",
                                  filename, strerror(errno));
                }
            }
        }
        de = readdir(target_dir);
    }
    closedir(target_dir);
    if (::rmdir(dir.get())) {
        throw rterror("path::rmdir - Unable to remove directory: %s\n%s",
                 dir.get(), strerror(errno));
    }
}
// -------------------------------------------------------------------------------------------------
/** If the base is empty function calls dirname_exists().  In Linux existence of symbolic link
  also returns true.
  \retval bool True if dir and base exists, false if not.
*/
bool path::exists() const
{
    if (base.empty())
        return dirname_exists();
    // Simply stat the file
    struct stat stat_data;
    if (!stat(get_ccpath(), &stat_data)) {
        if (S_ISREG(stat_data.st_mode) || S_ISLNK(stat_data.st_mode)) {
            return true;
        }
    }
    return false;
}
// -------------------------------------------------------------------------------------------------
/**
  \param envar Pointer to environment variable.
  \param set_dir If true then the directory is replaced with the found directory path.
  \retval bool True if base exists, false if not.
*/
bool path::exists_in_env_path(const char* envar, bool set_dir)
{
    ntbs_md envpath;
    if (!get_env_var(envar, envpath))
        throw rterror("path::exists_in_env_path - Unable to find variable: %s", envar);

    // Save the original dir
    ntbs backupDir(dir);
    size_t end, start = 0;
    do {
        end = envpath.find(C4S_PSEP, start);
        if (end == SIZE_MAX)
            dir = envpath.substr(start);
        else
            dir = envpath.substr(start, end - start);
        start = end + 1;
        dir += C4S_DSEP;
        if (exists()) {
            if (!set_dir)
                dir = backupDir;
            return true;
        }
    } while (end != SIZE_MAX);
    dir = backupDir;
    return false;
}
// -------------------------------------------------------------------------------------------------
bool path::get_stat()
{
    if (base.empty())
        return false;
    if (!change_time) {
        ntbs_lg full;
        get_path(full);
        struct stat stat_data;
        if (stat(full.get(), &stat_data) == -1) {
            change_time = 0;
            if (errno != ENOENT)
                CS_VAPRT_ERRO("path::get_stat - Unable to access: %s; (%s)\n",full.get(), strerror(errno));
            return false;
        }
#ifdef __APPLE__
        change_time = (time_t) stat_data.st_mtimespec.tv_sec;
#else
        change_time = (time_t) stat_data.st_mtim.tv_sec;
#endif
#ifdef C4S_UNITTEST
        fprintf(utlog, "path::get_stat - %s (%ld)\n", base.get(), change_time);
#endif
    }
    return true;
}
// -------------------------------------------------------------------------------------------------
/**
 * \param target Path to target file
 * \retval int 0 if the file modification times are equal, -1 if this file is older than target and
 *               1 if this file is newer than target.
 */
int path::compare_times(path& target)
{
    int rv = 0;
    if (!get_stat() || !target.get_stat())
        throw rterror("path::compare_times - unable to stat '%s' and target '%s'. Cannot compare.",
                      base.get(), target.base.get());
    if (change_time < target.change_time)
        rv = -1;
    else if (change_time > target.change_time)
        rv = 1;
#ifdef C4S_UNITTEST
    int diff = change_time - target.change_time;
    fprintf(utlog, "path::compare_times - %s -> %s = %d (%d)\n",
            base.get(), target.base.get(), rv, diff);
#endif
    return rv;
}
// -------------------------------------------------------------------------------------------------
void path::unix2dos()
{
    size_t offset = dir.find('/', 0);
    while (offset != SIZE_MAX) {
        dir[offset] = '\\';
        dir.find('/', offset + 1);
    }
}
// -------------------------------------------------------------------------------------------------
void path::dos2unix()
{
    size_t offset = dir.find('\\', 0);
    while (offset != SIZE_MAX) {
        dir[offset] = '/';
        dir.find('\\', offset + 1);
    }
}
// -------------------------------------------------------------------------------------------------
inline bool isflag(int f, const int t)
{
    return (f & t) == t ? true : false;
}
#define IS(x) isflag(flags, x)
// -------------------------------------------------------------------------------------------------
/** Copies this file into the target. In Linux, after the file is copied the owner and mode
  is changed if they have been defined for the target.
  \param to Path to target file
  \param flags See PCF constants
  \retval int Number of files copied. 1 or more if PCF_RECURSIVE is defined.
*/
int path::cp(const path& to, int flags) const
{
    char rb[0x4000];
    const char* out_mode;
    FILE *f_from, *f_to;
    path tmp_to(to);
    size_t br;

    // Check for recursive copy
    if (base.empty()) {
        if (!to.base.empty())
            throw rterror("path::cp - cannot copy directory into a file.");
        if (IS(PCF_RECURSIVE)) {
            return copy_recursive(to, flags);
        }
        throw rterror("path::cp - source is a directory and path::RECURSIVE is not defined.");
    }
    // Create the target path
    if (tmp_to.base.empty() || IS(PCF_ONAME))
        tmp_to.base = base;
    // If the target exists and force is not on: bail out.
    if (tmp_to.exists()) {
        if (IS(PCF_BACKUP)) {
            path backup(tmp_to);
            ntbs bb(tmp_to.get_base());
            bb += "~";
            backup.rename(bb);
        } else if (!IS(PCF_FORCE))
            throw rterror("path::cp - target file exists: %s", tmp_to.get_ccpath());
#ifdef C4S_UNITTEST
        fprintf(utlog, "path::cp - DEBUG: forcing file overwrite:%s\n", tmp_to.get_ccpath());
#endif
    }

    // Append if told so
    if (IS(PCF_APPEND))
        out_mode = "ab";
    else
        out_mode = "wb";

    // Open source file
    f_from = fopen(get_path().get(), "rb");
    if (!f_from) {
        throw rterror("path::cp - Unable to open source file: %s (%d)",
                 get_ccpath(), errno);
    }
    // Open target
    f_to = fopen(tmp_to.get_path().get(), out_mode);
    if (!f_to) {
        // If the directory did not exist: create it.
        if (!tmp_to.dirname_exists() && IS(PCF_FORCE)) {
            tmp_to.mkdir();
            f_to = fopen(tmp_to.get_path().get(), out_mode);
        }
        if (!f_to) {
            fclose(f_from);
            throw rterror("path::cp - unable to open target: %s (%d)",
                     tmp_to.get_ccpath(), errno);
        }
#ifdef C4S_UNITTEST
        std::cout << "path::cp - DEBUG: Created new directory for target file\n";
#endif
    }
    // copy one chunk at the time.
    do {
        br = fread(rb, 1, sizeof(rb), f_from);
        if (fwrite(rb, 1, br, f_to) != br) {
            fclose(f_from);
            fclose(f_to);
            throw rterror("path::cp - output error to: %s (%d)",
                     tmp_to.get_ccpath(), errno);
         }
    } while (!feof(f_from));

    // Close the files and copy permissions.
    fclose(f_from);
    fclose(f_to);
    if (!IS(PCF_DEFPERM)) {
#ifdef C4S_UNITTEST
        std::cout << "path::cp - DEBUG: Setting permissions\n";
#endif
        if (mode != -1)
            tmp_to.chmod(mode);
        else
            copy_mode(tmp_to);
        if (owner) {
            tmp_to.owner = owner;
            tmp_to.owner_write();
        }
    }

    // If this was a move operation, remove the source file.
    if (IS(PCF_MOVE))
        rm();
    return 1;
}
// -------------------------------------------------------------------------------------------------
void path::cat(const path& tail)
/** Concatenates given file into file pointed by this path
  \param tail Ref to path that is added to the end of this file.
*/
{
    using ::std::ios;
    char rb[1024];

    // Check that base is not empty;
    if (base.empty())
        throw rterror("path::cat - cannot cat to directory");
    // Open this file
    std::fstream target(get_path().get(), ios::in | ios::out | ios::ate | ios::binary);
    if (!target)
        throw rterror("path::cat - Unable to open target file: %s", get_ccpath());

    // Open tail file
    std::ifstream tfil(tail.get_path().get(), ios::in | ios::binary);
    if (!tfil) {
        target.close();
        throw rterror("path::cat - unable to open file to concatenate: %s", tail.get_ccpath());
    }
    // copy one chunk at the time.
    do {
        tfil.read(rb, sizeof(rb));
        target.write(rb, tfil.gcount());
        if (target.fail()) {
            target.close();
            tfil.close();
            throw rterror("path::cat - unable to write to the cat target: %s", get_ccpath());
        }
    } while (!tfil.eof());
    // Close the files
    target.close();
    tfil.close();
    change_time = 0;
}
// -------------------------------------------------------------------------------------------------
/**  In Windos this only copies file time-attributes only.

  \param target Path to target where the attributes are copied into.
*/
void path::copy_mode(const path& target) const
{
    int src = open(get_path().get(), O_RDONLY);
    if (src == -1)
        throw rterror("path::cp - unable to open source: %s", get_ccpath());
    int tgt = open(target.get_path().get(), O_WRONLY);
    if (tgt == -1)
        throw rterror("path::cp - unable to open target: %s", target.get_ccpath());
    struct stat sbuf;
    fstat(src, &sbuf);
    fchmod(tgt, sbuf.st_mode);
    close(src);
    close(tgt);
}
// -------------------------------------------------------------------------------------------------
int path::copy_recursive(const path& target, int flags) const
/** Copies everything from this directory to target. If this object has base defined it will be
  ignored. If the target does not exist it will be created (recursively). If files exist in target
  they will be copied over. File times are preserved. Only regular files are copied. \param target
  Target directory for the copied files.
*/
{
    int copy_count = 0;
    // Open the directory
    DIR* source_dir = opendir(dir.get());
    if (!source_dir) {
        throw rterror("path::cpr - Unable to access directory: %s\n%s",
                 dir.get(), strerror(errno));
    }

    // Make sure the target directory exists
    if (!target.dirname_exists())
        target.mkdir();

    // Read the source directory
    path cp_source;
    ntbs file_name;
    struct stat file_stat;
    struct dirent* de = readdir(source_dir);
    while (de) {
        cp_source.dir = dir;

        // Get the file's lstat. The dirent type is not reliable
        file_name = dir;
        file_name += de->d_name;
        if (!lstat(file_name.get(), &file_stat)) {
            // If entry is a regular file: copy it
            if (S_ISREG(file_stat.st_mode)) {
                cp_source.base = de->d_name;
                copy_count += cp_source.cp(target, flags);
            }
            // Else if directory:
            else if (S_ISDIR(file_stat.st_mode) && de->d_name[0] != '.') {
                cp_source.dir += de->d_name;
                cp_source.dir += C4S_DSEP;
                path sub_target(target);
                sub_target.dir += de->d_name;
                sub_target.dir += C4S_DSEP;
                copy_count += cp_source.copy_recursive(sub_target, flags);
            }
        }
        de = readdir(source_dir);
    }
    closedir(source_dir);
    return copy_count;
}
// -------------------------------------------------------------------------------------------------
/**
  Renames the file pointed by this path. Only the base name is affected. Use copy function to
  move the file to another directory.
  \param new_base New base name for the path
  \param force If new file alerady exist, it is deleted if force is true. Otherwice exception is
  thrown.
*/
void path::rename(const ntbs& new_base, bool force)
{
    ntbs old_base = base;
    ntbs old = get_path();
    set_base(new_base);
    ntbs nw = get_path();
    if (exists()) {
        if (force) {
            if (!rm()) {
                set_base(old_base);
                throw rterror("path::rename - unable to remove existing file.");
            }
        } else {
            set_base(old_base);
            throw rterror("path::rename - target already exist.");
        }
    }
    if (::rename(old.get(), nw.get()) == -1) {
        set_base(old_base);
        throw rterror("path::rename(base) from %s to %s - error %s", old.get(), nw.get(), strerror(errno));
    }
}
// -------------------------------------------------------------------------------------------------
/**
  Renames = moves the file within the same filesystem.
  \param new_base New path for the file
  \param force If new file alerady exist, it is deleted if force is true. Otherwice exception is
  thrown.
*/
void path::rename(const path& target, bool force)
{
    if (target.exists()) {
        if (force) {
            if (target.is_dir())
                target.rmdir(true);
            else if (!target.rm())
                throw rterror("path::rename - unable to remove existing file.");
        } else
            throw rterror("path::rename - target already exist.");
    }
    ntbs src = get_path();
    if (src.get_last() == '/')
        src.trim_end();
    ntbs tgt = target.get_path();
    if (tgt.get_last() == '/')
        tgt.trim_end();
    if (::rename(src.get(), tgt.get()) == -1)
        throw rterror("path::rename(path) from %s to %s - error %s", get_path().get(), target.get_path().get(), strerror(errno));
    dir = target.dir;
    base = target.base;
}
// -------------------------------------------------------------------------------------------------
/**
  Removes the file from the disk. Removes empty directories as well. If file does not exist or
  directory has files this function returns false. Other errors cause exception. USE WITH CARE!!
  \retval bool True if deletion is successful or if file does not exist. False otherwise.
*/
bool path::rm() const
{
    ntbs name = base.empty() ? get_dir_plain() : get_path();
    if (unlink(name.get()) < 0) {
        if (errno == ENOENT)
            return true;
        if (errno == EPERM) {
            if (::rmdir(name.get()) < 0)
                return false;
            return true;
        }
        throw rterror("path::rm - unable to delete %s; error:%s", get_ccpath(), strerror(errno));
    }
    return true;
}
// -------------------------------------------------------------------------------------------------
/**
  Creates a named symbolic link to current path.
  \param link Name of the link
*/
void path::symlink(const path& link) const
{
    ntbs_md source;
    ntbs_md linkname;
    if (base.empty()) {
        if (!dirname_exists())
            throw rterror("path::symlink - Symbolic link target dir:%s does no exist", dir.get());
        source = get_dir_plain();
    } else {
        if (!exists())
            throw rterror("path::symlink - Symbolic link target file:%s%s does no exist",
                     dir.get(), base.get());
        source = get_path();
    }
    linkname = link.base.empty() ? link.get_dir_plain() : link.get_path();
    if (::symlink(source.get(), linkname.get())) {
        throw rterror("path::symlink - Unable to create link '%s' to '%s': %s",
                 linkname.get(), source.get(), strerror(errno));
    }
}
// -------------------------------------------------------------------------------------------------
/**  In Windows environment file is changed to read-only if user write flag is not set.

  \param mode_in New mode to set. Use hex values for representing permissions. If default value -1
  is used then the mode specified in constructor will be used. If nothing was specified at
  constructor function does nothing.
*/
void path::chmod(int mode_in)
{
    if (mode_in == -1) {
        if (mode >= 0)
            mode_in = mode;
        else
            return;
    } else if (mode < 0)
        mode = mode_in;
    mode_t final = hex2mode(mode_in);
    if (::chmod(get_path().get(), final) == -1)
        throw rterror("path::chmod failed - %s - Error:%s", get_ccpath(), strerror(errno));
}
// -------------------------------------------------------------------------------------------------
/**
  Sample: if this path's directory is '/original/short/' and src is '/path/to/append/' this path
  will be '/original/short/append/' after this function is called.
 */
void path::append_last(const path& src)
{
    if (src.dir.empty())
        return;
    size_t dirIndex = src.dir.rfind(C4S_DSEP, src.dir.size() - 2);
    if (dirIndex == SIZE_MAX)
        return;
    dir += src.dir.substr(dirIndex + 1);
    change_time = 0;
}
// -------------------------------------------------------------------------------------------------
void path::append_dir(const char* srcdir)
{
    dir += srcdir;
    normalize();
    change_time = 0;
}
// -------------------------------------------------------------------------------------------------
void path::normalize()
{
    if (dir.get_last() != C4S_DSEP)
        dir += C4S_DSEP;
}
// -------------------------------------------------------------------------------------------------
void path::dump(std::ostream& log)
{
    log << "path /dir:" << dir << " /base:" << base << " /flag:";
    if (flag)
        log << "true; ";
    else
        log << "false; ";
    log << " /time:" << change_time;
    log << " /mode:" << std::hex << mode << std::dec << "; ";
#ifndef C4S_UNITTEST
    if (owner)
        owner->dump(log);
    else
        log << "owner: NULL";
#endif
    log << '\n';
}

} // namespace c4s
