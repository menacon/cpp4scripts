// csv_reader.hpp
#ifndef C4S_CSVREADER_HPP
#define C4S_CSVREADER_HPP

#include <stdio.h>
#include <vector>
#include "path.hpp"
#include "ntbs.hpp"
#include "util.hpp"

namespace c4s {

class CSVOPT : public flags32_base
{
public:
    static const flag32 NONE = 0x0000;
    static const flag32 STOP_ON_ERR = 0x0001;
    static const flag32 TRIM_CELLS = 0x0002;
    static const flag32 ALLOW_EMPTY_LINES = 0x0004;

    CSVOPT() : flags32_base(NONE) {}
    explicit CSVOPT(flag32 fx) : flags32_base(fx) {}
};

class csv_reader : public CSVOPT
{
public:
    csv_reader(const c4s::path& fname, int _separator);
    ~csv_reader();

    bool parse_next();
    size_t get_lineno() { return line_no; }

    std::vector<c4s::ntbs> header;
    std::vector<c4s::ntbs> line;

protected:
    void add_cell(int ndx, const c4s::ntbs& value) {
        if (ndx < line.size())
            line[ndx] = value;
        else
            line.push_back(value);
    }
    FILE* fhandle;
    size_t line_no;
    int separator;
};

}; // namespace c4s

#endif