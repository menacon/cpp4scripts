/*******************************************************************************
config.cxx
This is a sample for Cpp for Scripting library.
Use of ntbs is tested.

../debug/makec4s --dev config.cxx

................................................................................
License: LGPLv3
Copyright (c) Menacon Oy, Finland
*******************************************************************************/

#include <stdio.h>
#include "../c4slib.hpp"

using namespace c4s;

#include "run_utest.cxx"

const char* jsontxt =
"{"
"    /* C-style comments are allowed */"
"    \"section1\": {"
"        \"item1.1\": \"1.1 value\","
"        \"item1.2\": 1.2,"
"        \"item1.3\": true"
"    },"
"    \"section2\": {"
"        \"item2.1\": \"Value with comment\", /* comment for 2.1 */"
"        \"item2.2\": false,"
"        \"item2.3\": \"val 2.3\""
"    }"
"}";

const char* tomltxt =
"# toml\n"
"item = \"general text\"\n"
"[Section1]\n"
"Text1 = \"Hello world\"\n"
"\"Text2\" = \"Goodbye earth\"\n"
"  Boolean = true\n"
"Integer = 42\n"
"Float = 3.14\n"
"\n"
"[Section2]\n"
"# Allow following exceptionally\n"
"Text2.1 = Hello world\n"
"Integer = 0x2A # Magic number\n"
"Bool = t";

bool test1()
{
    bool rv = true;
    using namespace config;

    // Write our config first
    FILE* handle = fopen("config-read-tmp.json", "w");
    if (!handle) {
        fputs("Unable to write config-read-tmp.json\n", utlog);
        return false;
    }
    fputs(jsontxt, handle);
    fclose(handle);

    try {
        ntbs value;
        config_store cs("config-read-tmp.json");
        // Dump the contents into a log
        for (auto sn : cs.sections) {
            puts(sn->name.get());
            for (auto item : sn->items) {
                item.second->get_text(value);
                printf("  %s\n", value.get());
            }
        }

        float item1_2;
        bool item2_2;

        if (!cs.get_value("section1", "item1.2", item1_2)) {
            fputs("Unable to read section1/item1.2\n", utlog);
            rv = false;
        } else if (12 != int(item1_2*10)) {
            fputs("Incorrect value for section1/item1.2\n", utlog);
            rv = false;
        }
        if (!cs.get_value("section2", "item2.2", item2_2)) {
            fputs("Unable to read section2/item2.2\n", utlog);
            rv = false;
        } else if (item2_2) {
            fputs("Incorrect value for section2/item2.2\n", utlog);
            rv = false;
        }
    }
    catch (const c4s_exception& ce) {
        fprintf(utlog, "test1 failed: %s\n", ce.what());
        return false;
    }
    return rv;
}
bool test2()
{
    using namespace config;
    config_store cs;
    bool rv = true;

    config::section* ss = new section("general");
    cs.sections.push_back(ss);
    ss->items[ntbs("g_item1")] = new integer_item(42);
    ss->items[ntbs("g_item2")] = new bool_item(true);
    ss->items["g_item3"] = new str_item("Hello world");

    cs.write_toml(path("config-write-tmp.toml"), "Generated by config unit test.");
    return rv;
}
bool test3()
{
    //bool rv = true;
    using namespace config;
    // Write our config first
    FILE* handle = fopen("config-read-tmp.toml", "w");
    if (!handle) {
        fputs("Unable to write config-read-tmp.toml\n", utlog);
        return false;
    }
    fputs(tomltxt, handle);
    fclose(handle);

    try {
        ntbs vs;
        bool vb;
        uint64_t vi;
        float vf;
        config_store cs("config-read-tmp.toml");
        fputs("\n---\n", utlog);

        // Dump the contents to screen
        for (auto sn : cs.sections) {
            puts(sn->name.get());
            for (auto item : sn->items) {
                item.second->get_text(vs);
                printf("  %s = %s\n", item.first.get(), vs.get());
            }
        }
        vs.clear();
        if (!cs.get_value("general", "item", vs) ||
            vs != "general text" ) {
            fprintf(utlog, "general.item not found or incorrect:%s", vs.get());
            return false;
        }
        if (!cs.get_value("Section1", "Boolean", vb) || vb == false) {
            fprintf(utlog, "Section1.Boolean not found or incorrect\n");
            return false;
        }
        if (!cs.get_value("Section1", "Float", vf)) {
            fprintf(utlog, "Section1.Float not found\n");
            return false;
        }
        if (vf < 3.139  || vf > 3.141 ) {
            fprintf(utlog, "Section1.Float incorrect\n");
            return false;
        }
        if (!cs.get_value("Section2", "Bool", vb) || vb == false) {
            fprintf(utlog, "Section2.Bool not found or incorrect\n");
            return false;
        }
        if (!cs.get_value("Section2", "Integer", vi) || vi != 42) {
            fprintf(utlog, "Section2.Integer not found or incorrect\n");
            return false;
        }
    }
    catch (const c4s_exception& ce) {
        fprintf(utlog, "test1 failed: %s\n", ce.what());
        return false;
    }
    return true;
}

int main(int argc, char **argv)
{
    TestItem tests[] = {
        { &test1,  "Parsing JSON"},
        { &test2,  "Creating TOML"},
        { &test3,  "Parsing TOML"},
        { 0, 0}
    };
    puts("#-- Running config test");
    return run_tests(argc, argv, tests);
}