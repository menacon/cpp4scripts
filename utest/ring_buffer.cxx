/*******************************************************************************
This is a unit test file for Cpp4Scripting library.

Compile:
../debug/makec4s --dev -s ring_buffer.cxx

................................................................................
License: LGPLv3
Copyright (c) Menacon Oy
*******************************************************************************/

#include <string>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <time.h>
// --
#include "../ring_buffer.hpp"
#include "../ring_buffer.cpp"
#include "../ntbs.cpp"

using namespace c4s;
using namespace std;

#include "run_utest.cxx"

const char* lorem_full = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse\n"
                      "et nunc tristique, egestas urna ut, ultricies massa. Suspendisse\n"
                      "potenti. Quisque tincidunt felis ex, ac rhoncus tortor interdum sed.\n"
                      "Phasellus eu justo porta, iaculis sem eu, facilisis libero. Nam\n"
                      "commodo mollis velit sit amet rhoncus. Suspendisse iaculis dolor sed\n"
                      "orci ornare, nec facilisis velit rutrum. Cras maximus a dui et\n"
                      "rutrum. Nunc varius risus nunc, eget aliquet odio lacinia ut.\n"
                      "Pellentesque sit amet lacinia sem, quis varius magna. Nam varius\n"
                      "sodales ultrices. Mauris semper odio ex. Curabitur sollicitudin et\n"
                      "est ut vestibulum. Suspendisse aliquam mauris eu dolor lacinia, sit\n"
                      "amet ullamcorper metus elementum. Proin sodales finibus nibh. Nullam\n"
                      "rhoncus metus posuere magna dapibus aliquam sit amet ut dui.";

const char* lorem_lines[] = {
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse\n",
    "et nunc tristique, egestas urna ut, ultricies massa. Suspendisse\n",
    "potenti. Quisque tincidunt felis ex, ac rhoncus tortor interdum sed.\n",
    "Phasellus eu justo porta, iaculis sem eu, facilisis libero. Nam\n",
    "commodo mollis velit sit amet rhoncus. Suspendisse iaculis dolor sed\n",
    "orci ornare, nec facilisis velit rutrum. Cras maximus a dui et\n",       // 5
    "rutrum. Nunc varius risus nunc, eget aliquet odio lacinia ut.\n",
    "Pellentesque sit amet lacinia sem, quis varius magna. Nam varius\n",
    "sodales ultrices. Mauris semper odio ex. Curabitur sollicitudin et\n",
    "est ut vestibulum. Suspendisse aliquam mauris eu dolor lacinia, sit\n",
    "amet ullamcorper metus elementum. Proin sodales finibus nibh. Nullam\n", // 10
    "rhoncus metus posuere magna dapibus aliquam sit amet ut dui.",
    0
};

bool test1()
{
    ntbs copy(1024);
    ring_buffer rb(1024);

    fputs("test1 --\n", utlog);
    rb.write(lorem_full, strlen(lorem_full));
    size_t len = rb.read(copy, copy.max_size());

    return len == strlen(lorem_full) && copy == lorem_full;
}

bool test2()
{
    ring_buffer rb(1024);
    ntbs_md line;
    int ndx;

    fputs("test2 --\n", utlog);
    rb.write(lorem_full, strlen(lorem_full));

    for (ndx=0; lorem_lines[ndx] && rb.read_line(line, true, false); ndx++) {
        if (line != lorem_lines[ndx]) {
            fprintf(utlog, "  line %d does not match:\n", ndx+1, );
            fprintf(utlog, "    %s\n", line.get());
            return false;
        }
    }
    if (lorem_lines[ndx]) {
        fputs("  incomplete read.\n", utlog);
        return false;
    }
    return true;
}

bool test3()
{
    ring_buffer rb(1024);
    ntbs_md line;
    size_t br;

    fputs("test3 --\n", utlog);
    if (!rb.empty()) {
        fputs("Initial buffer not empty.\n", utlog);
        return false;
    }
    rb.write(lorem_lines[11]);
    br = rb.read_line(line, false, false);
    if (br != 0) {
        fputs("  Partial line disabled but read anyway.\n", utlog);
        return false;
    }
    if (rb.length() == 0) {
        fputs("  Partial read not cancelled.\n", utlog);
        return false;
    }
    br = rb.read_line(line, true, false);
    if (br == 0) {
        fputs("  Partial line enabled but not read.\n", utlog);
        return false;
    }
    if (line != lorem_lines[11]) {
        fputs("  Partial read mismatch.\n", utlog);
        return false;
    }
    // --
    rb.clear();
    br = rb.read_line(line, true, false);
    if (br > 0 || rb.gcount() > 0) {
        fputs("  read_line should not proceed on an empy buffer.\n", utlog);
        return false;
    }

    return true;
}

// -------------------------------------------------------------------------------------------------
int main(int argc, char** argv)
{
    TestItem tests[] = { { &test1, "Write buffer full content, read buffer full content" },
                         { &test2, "Write buffer full content, read by lines" },
                         { &test3, "Read lines: special cases" },
                         { 0, 0 } };

    return run_tests(argc, argv, tests);
}