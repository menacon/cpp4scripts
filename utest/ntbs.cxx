/*******************************************************************************
ntbs.cxx
This is a sample for Cpp for Scripting library.
Use of ntbs is tested.

../debug/makec4s --dev ntbs.cxx

................................................................................
License: LGPLv3
Copyright (c) Menacon Oy, Finland
*******************************************************************************/

#include <sstream>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include "../c4slib.hpp"

using namespace std;
using namespace c4s;

#include "run_utest.cxx"

bool test1()
{
    ntbs_sm origin("Aliquam tempus nisl ut mi aliquet, ut vulputate lacus venenatis. Nunc fringilla"
                   " tempor purus. Mauris vel metus tristique, rutrum ligula eget.");

    origin.prepend("Nullam in dolor ultricies. ");
    if (origin != "Nullam in dolor ultricies. Aliquam tempus nisl ut mi aliquet, ut vulputate lacus venenatis."
                  " Nunc fringilla tempor purus. Mauris vel metus tristique, rutrum ligula eget.") {
        fprintf(utlog, "test1 - Prepend not matching. length=%ld\n", origin.length());
        return false;
    }
    return true;
}

bool test2()
{
    bool caught = false;
    ntbs_sm abc("abcdefg");

    try {
        char& x = abc[10];
        x = 'X';
    } catch (const c4s_exception& ce) {
        caught = true;
    }
    if (!caught) {
        fputs("Operator[] out of bounce allowed.\n", utlog);
        return false;
    }

    char& b = abc[1];
    b = 'B';
    return 'b' == abc[1] ? false : true;
}

bool sub3(const ntbs& n)
{
    n.dump(stdout);
    cout << n << endl;
    return true;
}

bool test3()
{
    return sub3(ntbs("hello"));
}

int main(int argc, char **argv)
{
    TestItem tests[] = {
        { &test1,  "Prepend function"},
        { &test2,  "operator[] overload"},
        { &test3,  "Const constructor"},
        { 0, 0}
    };

    return run_tests(argc, argv, tests);
}