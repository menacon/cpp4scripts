/**
 * makec4s client.cpp
 */

#include <stdio.h>
#include <cpp4scripts/c4slib.hpp>

extern char **environ;

using namespace c4s;

int main(int argc, const char **argv)
{
    program_arguments args;

    args += argument("--help",  false, "Outputs this help / parameter list.");
    args += argument("-env", false, "Print environment variables");
    args += argument("-params", false, "Echo given parameters");
    args += argument("-out", false, "Print dummy lines to stdout and stderr");
    args += argument("--fe", true, "Find named environment value. Returns 0 if found.");

    try {
        args.initialize(argc,argv);
    }catch(const c4s_exception &ce){
        puts("Incorrect parameters:");
        puts(ce.what());
        return 1;
    }
    if (args.is_set("--help") ) {
        args.usage();
        return 0;
    }
    if (args.is_set("-env")) {
        puts("# Environment:");
        for (char** cc = environ; *cc; cc++) {
            printf("%s\n", *cc);
        }
    }
    if (args.is_set("-params")) {
        puts("# Parameters:");
        for (int ndx=0; ndx < argc; ndx++) {
            printf("%d: %s\n", ndx, argv[ndx]);
        }
    }
    if (args.is_set("-out")) {
        for (int ndx=0; ndx<10; ndx++) {
            fprintf(stdout, "Stdout line %d\n", ndx+1);
            fprintf(stderr, "Stderr line %d\n", ndx+1);
        }
    }
    if (args.is_set("--fe")) {
        ntbs_sm value(args.get_value("--fe"));
        for (char** cc = environ; *cc; cc++) {
            if (value == *cc) {
                puts("OK");
                return 0;
            }
        }
        puts("Not found");
        return 1;
    }
    return 0;
}
