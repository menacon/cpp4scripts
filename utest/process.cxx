/*******************************************************************************
This is a sample / unit test file for Cpp4Scripts library.

../debug/makec4s --dev -deb process.cxx

................................................................................
License: LGPLv3
Copyright (c) Menacon Oy, Finland
*******************************************************************************/

#include <sstream>
#include <fstream>
#include "../c4slib.hpp"

using namespace std;
using namespace c4s;

#include "run_utest.cxx"

// -------------------------------------------------------------------------------------------------
bool test1()
{
    const char *final[20];
    char tokenized[64];
    const int MAX = 3;
    const char *raw[MAX] = {
        "hello world hello universe",
        "Two \"variables quoted\"",
        "Nested 'single \\'quoted varables\\''"
    };
    // due to exename there is always one extra argument
    int args[MAX] = {5,3,3};
    int items;
    final[0] = "(exename)";
    for (int ndx = 0; ndx < MAX; ndx++) {
        items = process::convert_arguments(final, raw[ndx], tokenized);
        if (items != args[ndx]) {
            fprintf(utlog, "Error: %d/%d in %d\nArgs:", items, args[ndx], raw[ndx]);
            for (ndx = 0; ndx < items; ndx++) {
                fprintf(utlog, "  %s\n", final[ndx]);
            }
            return false;
        }
    }
    return true;
}

bool test2()
{
    process p1("process");
    if (!p1.is_valid()) {
        fputs("command not found in current dir.\n", utlog);
        return false;
    }
    process p2("zsh");
    if (!p1.is_valid()) {
        fputs("command not found in path\n", utlog);
        return false;
    }

    return true;
}

bool test3()
{
    process::global_log_open("global.log");
    process("ls")();
    process("client", "-out")();
    process::global_log_close();

    return true;
}

bool test4()
{
    const char* t_env[] = {
        "HOME=/test/path",
        0
    };
    process::nzrv_exception = true;
    const char** merged = merge_environment(t_env);
    process client("client");
    client.set_environment(merged);
    try {
        client("--fe HOME=/test/pathx");
        fprintf(utlog, "Calling client: --fe HOME=/test/pathx\n");
        fprintf(utlog, "  return value: %d\n", client.last_return_value());
    } catch (const rterror& re) {
        fprintf(utlog, "Setting client environment failed: %s\n", re.what());
        delete_environment(merged);
        return false;
    }
    delete_environment(merged);
    return true;
}

// ==========================================================================================
int main(int argc, char **argv)
{
    TestItem tests[] = {
        { &test1,  "Test argument conversion"},
        { &test2,  "Finding commands"},
        { &test3,  "Global log"},
        { &test4,  "Set process environment"},
        { 0, 0}
    };

    return run_tests(argc, argv, tests);
}