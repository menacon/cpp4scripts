/*
../debug/makec4s --dev -deb -s csvutil.cpp
cp csvutil ~/bin

*/

#include <iostream>
#include <cpp4scripts/c4slib.hpp>

using namespace std;
using namespace c4s;

program_arguments args;

int jiraToMerlin()
{
    size_t pos;
    ntbs_md subject;
    int days;

    path in(args.get_value("-jira"));
    path out(in);
    if (args.is_set("-out"))
        out.set_base(args.get_value("-out"));
    else
        out.set_base("merlin_import.csv");

    try {
        csv_reader cr(in, ',');
        FILE* csv = fopen(out.get_ccpath(), "wb");
        fprintf(csv, "Title;Days\n");
        while (cr.parse_next()) {
            pos = cr.line[4].find('-');
            if (pos != SIZE_MAX) subject = cr.line[4].substr(pos+2);
            else subject = cr.line[4];
            if (subject.length() > 80) {
                subject = subject.substr(0, 80);
                subject += "...";
            }
            fprintf(csv, "%s %s;",  cr.line[1].get(), subject.get());
            days = cr.line[5].stoi();
            if (!days)
                days = 5;
            fprintf(csv, "%d\n",days);
        }
        fclose(csv);
        cout << "CSV converted.";
    } catch (const c4s_exception ce) {
        cout << "CSV conversion failed: " << ce.what() << '\n';
        return 1;
    }
    return 0;
}

int main(int argc, char const *argv[])
{
    args += argument("-jira", true, "Convert Jira csv to Merlin project");
    args += argument("-out", true, "Name of the output");
    args += argument("--help", false, "Outputs this help / parameter list.");

    try {
        args.initialize(argc, argv);
        if (args.is_set("--help")) {
            args.usage();
            return 0;
        }
    } catch (const c4s_exception& ce) {
        cout << "Incorrect parameters.\n" << ce.what() << '\n';
        return 1;
    }

    if (args.is_set("-jira"))
        return jiraToMerlin();
    else
        cout << "Nothing to do...\n";
    return 0;
}