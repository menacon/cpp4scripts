/** util.cxx
 * ../debug/makec4s --dev -deb util.cxx
*/

#include <stdio.h>
#include "../c4slib.hpp"
using namespace c4s;

extern char **environ;

#include "run_utest.cxx"

bool test1()
{
    const char* my_env[] = {
        "PATH=/my/test/path",
        "UTILCXX_A='this is a custom value'",
        "UTILCXX_B=42",
        0
    };
    const char** mp;
    bool rv = true;

    // count current number of env values
    int count1 = 0;
    for (char** e_item = environ; *e_item; e_item++)
        count1++;
    // Replace existing path and add 2 new
    char** new_env = merge_environment(my_env);
    int count2 = 0;
    for (char** np = new_env; *np; np++)
        count2++;
    if (count1 + 2 != count2) {
        c4slog << "Incorrect number of merged environment lines.\n";
        rv = false;
        goto CLEANUP1;
    }
    // Find the items:
    for (mp = my_env; *mp; mp++) {
        for (char** np = new_env; *np; np++) {
            if (!strcmp(*np, *mp))
                break;
        }
        if (!*mp) {
            c4slog << "Environment key-value: " << *mp << " not found.\n";
            return false;
        }
    }
    // Delete allocated environment
CLEANUP1:
    delete_environment(new_env);
    return rv;
}

// ==========================================================================================
int main(int argc, char **argv)
{
    TestItem tests[] = {
        { &test1,  "Merge environment values"},
        { 0, 0}
    };

    return run_tests(argc, argv, tests);
}