#include <stdio.h>
#include <fstream>

/*
#ifdef C4S_UNITTEST
#include <stdio.h>
extern FILE* utlog;
#endif
*/

FILE* utlog = 0;

typedef bool (*tfptr)();

struct TestItem
{
    TestItem(tfptr fn, const char* s)
    {
        test = fn;
        subject = s;
    }
    bool OK() { return test ? true : false; }

    tfptr test;
    const char* subject;
};

bool is_set(int argc, char** argv, const char* name)
{
    for (int n = 1; n < argc; n++) {
        if (!strncmp(argv[n], name, strlen(name)))
            return true;
    }
    return false;
}

const char* get_value(int argc, char** argv, const char* name)
{
    for (int n = 1; n < argc; n++) {
        if (!strncmp(argv[n], name, strlen(name))) {
            if (n + 1 < argc)
                return argv[n + 1];
        }
    }
    return "";
}

int run_tests(int argc, char** argv, TestItem* tests)
{
    using namespace std;
    int ndx;

    utlog = fopen("unit_test.log", "wb");
    if (!utlog) {
        fputs("Unable to open unit_test.log\n", stderr);
        return 99;
    }
    try {
        if (is_set(argc, argv, "-a")) {
            int count_OK = 0;
            int count_FAIL = 0;
            for (ndx = 0; tests[ndx].OK(); ndx++) {
                printf("%d %s", ndx + 1, tests[ndx].subject);
                fprintf(utlog, "## %d %s\n", ndx + 1, tests[ndx].subject);
                if (tests[ndx].test()) {
                    puts("\tOK");
                    count_OK++;
                } else {
                    puts("\tFail");
                    count_FAIL++;
                }
            }
            puts("------------");
            printf("Pass/fail = %d / %d = %d%% Success\n",
                    count_OK, count_FAIL, 100 * count_OK / (count_OK + count_FAIL));
        }
        else if (is_set(argc, argv, "-l")) {
            for (ndx = 0; tests[ndx].OK(); ndx++) {
                printf("(%d) %s\n", ndx + 1, tests[ndx].subject);
            }
            putc('\n', stdout);
            return 0;
        }
        else if (is_set(argc, argv, "-t")) {
            int t_index = stol(get_value(argc, argv, "-t"));
            for (ndx = 0; tests[ndx].OK(); ndx++) {
                if (ndx + 1 == t_index) {
                    puts(tests[ndx].subject);
                    if (tests[ndx].test())
                        puts("Test OK");
                    else
                        puts("Test Failed");
                    break;
                }
            }
            if (!tests[ndx].OK()) {
                fputs("Unknown test index\n", stderr);
                return -1;
            }
        }
        else {
            puts("Nothing to do. Commands: \n"
                    "-a     = run all tests.\n"
                    "-l     = list all available tests.\n"
                    "-t (x) = run test number 'x'");
        }
    } catch (const rterror& re) {
        puts("Unit test failed. See log for details.");
        fprintf(utlog,"Unit test failure: %s\n", re.what());
        fclose(utlog);
        return 1;
    }
    fclose(utlog);
    return 0;
}
