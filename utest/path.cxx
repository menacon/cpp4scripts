/*******************************************************************************
path.cxx
This is a sample for Cpp for Scripting library.
Use of path and path_list is tested.

../debug/makec4s --dev path.cxx

................................................................................
License: LGPLv3
Copyright (c) Menacon Oy, Finland
*******************************************************************************/

#include <stdio.h>
#include "../c4slib.hpp"

using namespace c4s;

#include "run_utest.cxx"

// -------------------------------------------------------------------------------------------------
bool test1()
{
    try{
        path target("c4stest/");
        if(!target.exists())
            target.mkdir();
        fputs("Copying *.cpp  to ./c4stest/\n", utlog);
        path_list pl(path("./"), ntbs("\\.cpp$"), ntbs());
        pl.copy_to(target);
    }catch(const c4s_exception &ce){
        fprintf(utlog, "\nTest1 failed: %s\n", ce.what());
        return false;
    }
    return true;
}
// -------------------------------------------------------------------------------------------------
bool test2()
{
    try {
        path src("./c4stest/");
        path tgt("./c4stmp/");
        src.rename(tgt);
        if (!tgt.dirname_exists())
            return false;
    } catch(const c4s_exception &ce) {
        fprintf(utlog, "\nTest1 failed: %s\n", ce.what());
        return false;
    }
    return true;
}
// -------------------------------------------------------------------------------------------------
bool test3()
{
    try {
        path tgt("./c4stmp/");
        tgt.rmdir(true);
        if (tgt.dirname_exists())
            return false;
    }catch(const c4s_exception &ce){
        fprintf(utlog, "\nTest3 failed: %s\n", ce.what());
        return false;
    }
    return true;
}

// -------------------------------------------------------------------------------------------------
bool test4()
{
    user staff("root","staff");
    user www("www-data","www-datax",false,"/var/www/");

    if (!www.is_ok()) {
        fputs("www user not found\n", utlog);
        return false;
    }
    const char* fname = "/opt/www/test/file.txt";
    fprintf(utlog, "Target file: %s\n", fname);
    path lp(fname, &www, 0x770);
    try {
        lp.mkdir();
/*
        ofstream of(lp.get_ccpath());
        of << "Test file created\n";
        of.close();
        lp.ch_owner_mode();

        user rpowner;
        path rp(fname, &rpowner);
        rp.read_owner_mode();

        if (lp.get_path().compare(rp.get_path()) != 0)
            c4slog << "Name mismatch.\n";
        else if (lp.get_mode() != rp.get_mode())
            c4slog << "Mode mismatch\n";
        else if (!rpowner.match(ufile)) {
            c4slog << "User mismatch\n";
            rpowner.dump(c4slog);
        }
        else
            c4slog << "Test OK.\n";
*/
    }catch(const c4s_exception &ce) {
        fprintf(utlog, "\nTest4 failed: %s\n", ce.what());
        return false;
    }
    return true;
}
// -------------------------------------------------------------------------------------------------
bool test5()
{
    fputs("# Test 5: compare times\n", utlog);
    bool exception_event = false;

    path src("../version.cpp");
    path tmp("path.tmp");
    tmp.rm();
    try {
        // if target does not exist => outdated
        if (src.compare_times(tmp) >= 0) {
            fputs("False positive for non-existent file\n", utlog);
            return false;
        }
    } catch (const rterror&) {
        exception_event = true;
    }
    if (!exception_event) {
        fputs("Non-existent file should have caused exception.\n", utlog);
        return false;
    }

    FILE *tmp_h = fopen(tmp.get_ccpath(), "w");
    fputs("Remove me!\n", tmp_h);
    fclose(tmp_h);

    try {
        if (src.compare_times(tmp) >= 0) {
            fputs("False positive on compare_times\n", utlog);
            return false;
        }
        fprintf(utlog, "src time: %ld; tmp time: %ld\n",
                src.get_changetime(), tmp.get_changetime());
        tmp.rm();
    } catch (const c4s_exception& ce) {
        fprintf(utlog, "test5 failed: %s\n", ce.what());
        return false;
    }

    path util_cpp("../util.cpp");
    util_cpp.get_stat();
    path util_hpp(util_cpp);
    util_hpp += "util.hpp";
    if (util_cpp.compare_times(util_hpp) == 0) {
        fputs("util.cpp and util.hpp should not be in same time.", utlog);
        return false;
    }

    return true;
}
// -------------------------------------------------------------------------------------------------
bool test6()
{
    bool rv = true;
    path p("/tmp/filename.ext");
    p.base_append("~");
    if (p.get_base() != "filename~.ext") {
        fprintf(utlog, "Base append failed. Result: %s\n", p.get_ccbase());
        rv = false;
    }
    p.base_prepend("pre_");
    if (p.get_base() != "pre_filename~.ext") {
        fprintf(utlog, "Base prepend failed. Result: %s\n", p.get_ccbase());
        rv = false;
    }
    return rv;
}
// -------------------------------------------------------------------------------------------------
bool test7()
{
    int rv = true;

    path p("/tmp/long/path/to/target.txt");
    p.rewind();
    if (p.get_dir().compare("/tmp/long/path/")) {
        fprintf(utlog, "Rewound path: %s\n", p.get_ccpath());
        rv = false;
    }
    return rv;
}

// ==========================================================================================
int main(int argc, char **argv)
{
    TestItem tests[] = {
        { &test1,  "path_list, mkdir: copies *.cpp files from current dir to c4stest dir."},
        { &test2,  "Move directory c4stest => c4stmp"},
        { &test3,  "Remove c4stmp directory"},
        { &test4,  "mkdir recursive: Creates long path and sets user rights on the way"},
        { &test5,  "Compare times (../version.cpp => path.tmp)"},
        { &test6,  "Base append and prepend."},
        { &test7,  "Path rewind"},
        { 0, 0}
    };

    return run_tests(argc, argv, tests);
}