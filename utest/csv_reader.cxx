/*******************************************************************************
csv_reader.cxx
This is a sample for Cpp for Scripting library.
Use of ntbs is tested.

../debug/makec4s --dev -s csv_reader.cxx

................................................................................
License: LGPLv3
Copyright (c) Menacon Oy, Finland
*******************************************************************************/

#include <vector>
#include <iostream>

#include "../c4slib.hpp"

#include "run_utest.cxx"

using namespace std;
using namespace c4s;

bool test1()
{
    vector<ntbs> cells;

    try {
        csv_reader cr("utest.csv", ';');
        // cr |= CSVOPT::TRIM_CELLS;
        if (cr.header.size() != 5) {
            fputs("Wrong number of columns\n", utlog);
            return false;
        }
        for (auto hc : cr.header)
            cout << hc << ';';
        cout << '\n';
        while (cr.parse_next()) {
            if (!cr.line.size())
                continue;
            for (auto cell : cr.line) {
                cout << cell << ";";
            }
            cout << '\n';
        }
        if (cr.get_lineno() != 5) {
            fprintf(utlog, "Wrong number of lines:%d\n", cr.get_lineno());
            return false;
        }
    } catch (const c4s_exception& ce) {
        fprintf(utlog, "Parsing failed: %s\n", ce.what());
        return false;
    }

    return true;
}

int main(int argc, char **argv)
{
    TestItem tests[] = {
        { &test1,  "Basic parsing with valid file."},
        { 0, 0}
    };

    return run_tests(argc, argv, tests);
}