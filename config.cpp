/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#include "libconfig.hpp"
#include "exception.hpp"
#include "ntbs.hpp"
#include "logger.hpp"
#include "config.hpp"
#include "path.hpp"

#ifdef C4S_UNITTEST
#include <stdio.h>
extern FILE* utlog;
#endif

namespace c4s {

namespace config {

section::section() {}

section::section(const ntbs& _name)
    : name(_name)
{
}

section::~section()
{
    for (auto item : items) {
        delete item.second;
    }
}

void section::get_subkeys(const ntbs& name, std::vector<ntbs>& keys)
{
    std::vector<ntbs>::iterator kit;
    ntbs needle;
    size_t pos = name.find('*');
    if (pos == SIZE_MAX)
        needle = name;
    else
        needle = name.substr(0, pos);

    size_t len = needle.size();
    for (auto item : items) {
        if (!item.first.compare(0, len, needle)) {
            pos = item.first.find('.', len);
            ntbs subkey =
                pos == SIZE_MAX ? item.first.substr(len) : item.first.substr(len, pos - len);
            for (kit = keys.begin(); kit != keys.end(); kit++) {
                if (!kit->compare(subkey))
                    break;
            }
            if (kit == keys.end())
                keys.push_back(subkey);
        }
    }
}

void section::get_values(const ntbs& name, std::vector<ntbs>& values)
{
    setting_map::iterator si;
    size_t pos = name.find('*');
    if (pos == SIZE_MAX) {
        return;
    }

    size_t ndx = 0;
    ntbs_md key;
    do {
        key.sprint("%s%d%s",name.substr(0, pos).get(), ndx, name.substr(pos + 1).get());
        for (si = items.begin(); si != items.end(); si++) {
            if (si->first == key && si->second->get_type() == TYPE::STR) {
                values.push_back(static_cast<str_item*>(si->second)->value);
                break;
            }
        }
        ndx++;
    } while (si != items.end());
}

bool section::read_json(FILE* handle, parse_data& pd)
{
    enum { START, KEY, VALUE, VSTR, VNUM, KEYWORD, END, COMMENT } state;
    enum { LONG, FLOAT } numtype;
    ntbs original_key(pd.key);
    ntbs value;

    state = pd.is_array() ? VALUE : START;
    char ch = (char) fgetc(handle);
    while (!feof(handle)) {
        switch (state) {
        case START:
            if (ch == '"')
                state = KEY;
            else if (ch == '}') {
                CS_VAPRT_ERRO("section::read_json - expected key at %ld", pd.pos);
                return false;
            }
            else if (ch == '/') {
                char c2 =  fgetc(handle);
                ungetc(c2, handle);
                if (c2 == '/') {
                    CS_VAPRT_ERRO("section::read_json - single line comments are not allowed. Position: %ld", pd.pos);
                    return false;
                }
                else if (c2 == '*')
                    state = COMMENT;
            }
            break;
        case KEY:
            if (ch == '"') {
                state = VALUE;
                value.clear();
            } else
                pd.key += ch;
            break;
        case VALUE:
            if (ch == '"')
                state = VSTR;
            else if ((ch >= '0' && ch <= '9') || ch == '-') {
                numtype = LONG;
                state = VNUM;
                fseek(handle, -1, SEEK_CUR);
            } else if (ch == 'n' || ch == 't' || ch == 'f') {
                if (pd.is_array()) {
                    CS_PRINT_ERRO("section::read_json - arrays of true/false, null not supported.");
                    return false;
                }
                state = KEYWORD;
                fseek(handle, -1, SEEK_CUR);
            } else if (ch == '[') {
                if (pd.is_array()) {
                    CS_PRINT_ERRO("section::read_json - nested arrays not supported.");
                    return false;
                }
                pd.array_item = 0;
                pd.level_up();
                if (!read_json(handle, pd))
                    return false;
                state = END;
            } else if (ch == '{') {
                if (pd.is_array()) {
                    CS_PRINT_ERRO("section::read_json - objects within array not supported.");
                    return false;
                }
                pd.level_up();
                if (!read_json(handle, pd))
                    return false;
                state = END;
            } else if (ch == '}') {
                CS_VAPRT_ERRO("section::read_json - expected value for key %s", pd.key.get());
                return false;
            }
            break;
        case VSTR:
            if (ch == '"') {
                items[pd.get_key()] = new str_item(value);
                state = END;
            } else
                value += ch;
            break;
        case VNUM:
            if (ch == ',' || ch == '}' || ch == '\t' || ch == ' ' || ch == '\n' || ch == ']') {
                try {
                    if (numtype == LONG) {
                        items[pd.get_key()] = new integer_item(value.stol());
                    } else
                        items[pd.get_key()] = new float_item(value.stod());
                    if (ch == ']') {
                        pd.level_down();
                        pd.array_item = -1;
                        return true;
                    }
                    if (ch == ',' || ch == '}')
                        fseek(handle, -1, SEEK_CUR);
                    state = END;
                } catch (const rterror& re) {
                    CS_VAPRT_ERRO("section::read_json - number conversion failed for: %s", value.get());
                    return false;
                }
            } else if (ch == '.') {
                numtype = FLOAT;
                value += ch;
            } else
                value += ch;
            break;
        case KEYWORD:
            if (ch == ',' || ch == '}') {
                if (!value.compare("true"))
                    items[pd.key] = new bool_item(true);
                else if (!value.compare("false"))
                    items[pd.key] = new bool_item(false);
                else if (value.compare("null")) {
                    CS_VAPRT_ERRO("section::read - unknown json keyword: %s", value.get());
                    return false;
                }
                fseek(handle, -1, SEEK_CUR);
                state = END;
            } else if (ch != '\t' && ch != ' ' && ch != '\n')
                value += ch;
            break;
        case END:
            if (ch == ',') {
                if (pd.is_array()) {
                    state = VALUE;
                    value.clear();
                } else {
                    state = START;
                    pd.key = original_key;
                }
            } else if (ch == '}') {
                pd.level_down();
                pd.key = original_key;
                return true;
            } else if (ch == ']') {
                pd.level_down();
                pd.array_item = -1;
                return true;
            }
            // else if (ch == '/' && input.peek() == '/') {
            //     state = COMMENT;
            // }
            break;
        case COMMENT: {
            char c2 =  fgetc(handle);
            ungetc(c2, handle);
            if (ch == '*' && c2 == '/')
                state = START;
            break;
            }
        }
        pd.pos++;
        ch = (char) fgetc(handle);
    }
    return true;
}

bool section::read_toml(FILE *handle, parse_data& pd)
{
    enum { KEY, VALUE, COMMENT } state;
    enum { LONG, FLOAT } numtype;
    ntbs value;

#ifdef C4S_UNITTEST
    fputs("\n@:", utlog);
#endif
    pd.key.clear();
    state = KEY;
    char ch = (char) fgetc(handle);
    while (!feof(handle)) {
#ifdef C4S_UNITTEST
        fputc(ch, utlog);
#endif
        switch(state) {
        case KEY:
            if (ch == '#')
                state = COMMENT;
            else if (ch == '=')
                state = VALUE;
            else if (ch == '\n') {
                CS_VAPRT_WARN("section::read_toml - possibly incomplete key-value on line: %d", pd.line);
                pd.line++;
                return true;
            }
            else if (ch != ' ' && ch != '\t' && ch != '\"')
                pd.key += ch;
            break;
        case VALUE:
            if (ch == '\n' || ch == '#') {
                return parse_value(pd.key, value);
            }
            if (ch != '\t')
                value += ch;
            break;
        case COMMENT:
            if (ch == '\n') {
                pd.line++;
                return true;
            }
            break;
        }
        ch = (char) fgetc(handle);
    }
    if (state == VALUE)
        return parse_value(pd.key, value);
    return true;
}

bool section::parse_value(ntbs& key, ntbs& value)
{
    value.trim();
    char c0 = value[0];
    if (c0 == '"' && value.get_last() == '"') {
        items[key] = new str_item(value.substr(1, value.length()-2));
        return true;
    }
    if (c0 == 't' || c0 == 'f') {
        if (value.length() == 1 || value == "true") {
            items[key] = new bool_item(true);
            return true;
        }
        if (value.length() == 1 || value == "false") {
            items[key] = new bool_item(false);
            return true;
        }
    }
    if (c0 >= '0' && c0 <= '9') {
        if (value.find('.') == SIZE_MAX)
            items[key] = new integer_item(value.stol());
        else
            items[key] = new float_item(value.stof());
        return true;
    }
    items[key] = new str_item(value);
    return true;
}

}; // namespace config
// ------------------------------------------------------------------------------------------------
using namespace config;

config_store::config_store()
{
    // Intentionally empty
}
config_store::config_store(const path& fname, config_store::FORMAT format)
{
    conf_line = 0;
    if (!fname.exists())
        throw rterror("config_store::config_store - Unable to find: %s", fname.get_ccpath());
    if ( format == FORMAT::JSON ||
         (format == FORMAT::NONE && fname.get_ext() == ".json"))
    {
        if (!read_json(fname))
            throw rterror("config_store json read or syntax error. Line: %d", conf_line);
    }
    else if ( format == FORMAT::TOML ||
             (format == FORMAT::NONE && fname.get_ext() == ".toml"))
    {
        if (!read_toml(fname))
            throw rterror("config_store toml read or syntax error. Line: %d", conf_line);
    }
    else {
        throw rterror("config_store::config_store - unknown format");
    }
}
config_store::~config_store()
{
    for (auto sn : sections){
        delete sn;
    }
}
bool config_store::read_toml(const path& fname)
{
    parse_data pd;
    ntbs sname;
    enum { START, COMMENT, HEADER, KV } state = START;

    FILE *handle = fopen(fname.get_ccpath(), "rb");
    if (!handle) {
        CS_VAPRT_ERRO("config_store::read_toml - Unable to open %s. Errno %d", fname.get_ccpath(), errno);
        return false;
    }

#ifdef C4S_UNITTEST
    fputs("config_store::read_toml - echoing chars:\n", utlog);
#endif
    config::section* ss = new config::section("general");
    sections.push_back(ss);

    char ch = (char) fgetc(handle);
    while (!feof(handle)) {
#ifdef C4S_UNITTEST
        fputc(ch, utlog);
#endif
        switch(state) {
        case START:
            if (ch == '#')
                state = COMMENT;
            else if (ch == '[')
                state = HEADER;
            else if (ch != ' ' && ch != '\t') {
                ungetc(ch, handle);
                if (!ss->read_toml(handle, pd))
                    return false;
                state = KV;
            }
            break;
        case HEADER:
            if (ch == '\n') {
                CS_VAPRT_ERRO("config_store::read_toml - New line in header. Line %d", pd.line);
                return false;
            }
            else if (ch == ']') {
                state = KV;
                ss = new config::section(sname);
                sections.push_back(ss);
                sname.clear();
            }
            else if (ch != ' ' && ch != '\t')
                sname += ch;
            break;
        case KV:
            if (ch == '[')
                state = HEADER;
            else if (ch != '\n') {
                ungetc(ch, handle);
                if (!ss->read_toml(handle, pd))
                    return false;
#ifdef C4S_UNITTEST
                fputc('\n', utlog);
#endif
            }
            break;
        case COMMENT:
            if (ch == '\n')
                state = START;
            break;
        }
        if (ch == '\n') {
            pd.line ++;
#ifdef C4S_UNITTEST
            fputc('\n', utlog);
#endif
        }
        ch = (char) fgetc(handle);
    }
    return true;
}
bool config_store::write_toml(const c4s::path& fname, const char* title)
{
    FILE* handle = fopen(fname.get_ccpath(), "w");
    if (!handle) {
        CS_VAPRT_ERRO("config_store::write_toml - Unable to write: %s\n  %s",
                      fname.get_ccpath(), strerror(errno));
        return false;
    }
    if (title)
        fprintf(handle, "# %s\n", title);
    ntbs_md value_str;
    for (auto sn : sections) {
        if (sections.size() > 1)
            fprintf(handle, "[%s]\n", sn->name.get());
        for (auto item : sn->items) {
            item.second->get_text(value_str);
            if (item.second->get_type() == TYPE::STR)
                fprintf(handle, "%s = \"%s\"\n", item.first.get(), value_str.get());
            else
                fprintf(handle, "%s = %s\n", item.first.get(), value_str.get());
        }
    }
    fclose(handle);
    return true;
}

bool config_store::read_json(const path& fname)
{
    config::section* ss;
    enum { START, L0, SNAME, L1 } state = START;
    parse_data pd;
    char ch;
    ntbs sname;

    FILE *handle = fopen(fname.get_ccpath(), "rb");
    if (!handle) {
        CS_VAPRT_ERRO("config_store::read_json - Unable to open %s. Errno %d", fname.get_ccpath(), errno);
        return false;
    }
    ch = fgetc(handle);
    while (!feof(handle)) {
        switch (state) {
        case START:
            if (ch == '{')
                state = L0;
            break;
        case L0:
            if (ch == '"') {
                sname.clear();
                state = SNAME;
            }
            break;
        case SNAME:
            if (ch == '"')
                state = L1;
            else
                sname += ch;
            break;
        case L1:
            if (ch == '{') {
                ss = new config::section(sname);
                if (!ss->read_json(handle, pd)) {
                    delete ss;
                    return false;
                }
                sections.push_back(ss);
                state = L0;
            }
            break;
        }
        ch = fgetc(handle);
    }
    return true;
}
// =================================================================================================
config::section* config_store::get_section(const ntbs& name)
{
    for (auto sn : sections) {
        if (sn->name == name)
            return sn;
    }
    return 0;
}

config::item* config_store::find(const ntbs& sn_name, const ntbs& name)
{
    // find sections
    section *sn = get_section(sn_name);
    if (!sn)
        return 0;
    // Find item
    auto si = sn->items.find(name);
    if (si == sn->items.end())
        return 0;
    return si->second;
}

const char* config_store::get_string(const ntbs& section, const ntbs& name)
{
    config::item* si = find(section, name);
    if (!si || si->get_type() != config::TYPE::STR)
        return "";
    return static_cast<config::str_item*>(si)->value.get();
}
bool config_store::get_value(const ntbs& section, const ntbs& name, ntbs& val)
{
    config::item* si = find(section, name);
    if (!si || si->get_type() != config::TYPE::STR)
        return false;
    val = static_cast<config::str_item*>(si)->value;
    return true;
}
bool config_store::get_value(const ntbs& section, const ntbs& name, uint64_t& val)
{
    config::item* si = find(section, name);
    if (!si || si->get_type() != config::TYPE::LONG)
        return false;
    val = static_cast<config::integer_item*>(si)->value;
    return true;
}
bool config_store::get_value(const ntbs& section, const ntbs& name, float& val)
{
    config::item* si = find(section, name);
    if (!si || si->get_type() != config::TYPE::FLOAT)
        return false;
    val = static_cast<config::float_item*>(si)->value;
    return true;
}
bool config_store::get_value(const ntbs& section, const ntbs& name, bool& val)
{
    config::item* si = find(section, name);
    if (!si || si->get_type() != config::TYPE::BOOL)
        return false;
    val = static_cast<config::bool_item*>(si)->value;
    return true;
}
bool config_store::is(const ntbs& section, const ntbs& name)
{
    config::item* si = find(section, name);
    if (!si || si->get_type() != config::TYPE::BOOL)
        return false;
    return static_cast<config::bool_item*>(si)->value;
}

}; // namespace c4s
