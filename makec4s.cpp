/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#include <stdio.h>
#include "c4slib.hpp"

#ifdef C4S_UNITTEST
#include <stdio.h>
FILE* utlog;
#endif

using namespace std;
using namespace c4s;

program_arguments args;

int make_template(const path& src)
{
    if (src.exists()) {
        cout << "Template already exists. Please use another name.\n";
        return 2;
    }
    ofstream ts(src.get_ccpath());
    if (!ts) {
        cout << "Unable to open template " << args.get_value("-new") << " for writing.\n";
        return 2;
    }
    ts << "#include <stdio.h>\n";
    ts << "#include <cpp4scripts/c4slib.hpp>\n\n"
          "using namespace c4s;\n\n";
    ts << "int main(int argc, const char **argv)\n{\n"
          "    program_arguments args;\n\n";

    ts << "    args += argument(\"--help\",  false, \"Outputs this help / parameter list.\");\n"
          "    try {\n"
          "        args.initialize(argc,argv);\n"
          "    }catch(const c4s_exception &ce){\n"
          "        puts(\"Incorrect parameters:\");\n"
          "        puts(ce.what());\n"
          "        return 1;\n"
          "    }\n";
    ts << "    if( args.is_set(\"--help\") ) {\n"
          "        args.usage();\n"
          "        return 0;\n"
          "    }\n";
    ts << "    return 0;\n";
    ts << "}\n";

    ts.close();
    cout << "Template created.\n";
    return 0;
}

int main(int argc, const char** argv)
{
    bool debug = false;
    bool verbose = false;
    path_list sources;
    path src;

#ifdef C4S_UNITTEST
    utlog = stdout;
#endif
    // Set arguments and initialize them.
    args += argument("-deb", false, "Create debug version.");
    args += argument("-rel", false, "Create release version (default).");
    args += argument("-new", true, "Make a new c4s-template file with VALUE as the name.");
    args += argument(
        "-c4s", true, "Path where Cpp4Scripts is installed. '/usr/local/cpp4scripts' is default");
    args += argument("-inc", true, "External include file to add to the build.");
    args += argument("-lib", true, "External library to add to the link command");
    args += argument("-hash", true, "Calculate FNV hash for named file.");
    args += argument("-v", false, "Prints the version number.");
    args += argument("-V", false, "Verbose mode. Prints more messages, including build command.");
    args += argument(
        "--dev", false, "Run  builder for unit test files in CPP4Scripts samples directory");
    args += argument("--help", false, "Outputs this help / parameter list.");

    try {
        // Last argument is the source
        if (argv[argc-1][0] != '-') {
            src = argv[argc-1];
            if (!strcmp("-new", argv[argc-2])) {
                return make_template(src);
            }
            sources += src;
            argc--;
        }
        args.initialize(argc, argv);
    } catch (const c4s_exception& ce) {
        cout << "Incorrect parameters.\n" << ce.what() << '\n';
        cout << "Use --help if needed.\n";
        return 1;
    }
    // ............................................................
    // Handle simple arguments.
    if (args.is_set("--help")) {
        cout << "Cpp4Scripts make program. " << get_c4s_version();
#ifndef NDEBUG
        cout << " (Debug)";
#endif
        cout << "\n\nUsage:\n"
                "   makec4s [options] (source file)\n\n";
        args.usage();
        return 0;
    }
    cout << "Cpp4Scripts make program. " << get_c4s_version() << ' ' << get_build_type() << '\n';
    if (args.is_set("-v")) {
        return 0;
    }
    if (args.is_set("-hash")) {
        file target(path(args.get_value("-hash")));
        cout << "FNV hash: " << hex << target.fnv_hash64(0) << "\n";
        return 0;
    }
    // ............................................................
    if (!src.exists()) {
        cout << "Source file '" << src.get_path() << "' does not exist.\n";
        return 1;
    }
    debug = args.is_set("-deb") || args.is_set("--dev") ? true : false;

    ntbs_sm target;
    target = src.get_base_plain();
    builder* make = 0;
    try {
#ifdef __clang__
        cout << "Building with clang\n";
        make = new builder_clang(target, BUILD_TYPE::BIN, stdout);
        make->add_comp("-x c++ -stdlib=libc++ -std=c++17 -pthread -fno-rtti -fexceptions "
                       "-fno-diagnostics-show-option -fno-show-column -fno-diagnostics-fixit-info "
                       "-Wall -Wno-unused-result");
        if (debug)
            make->add_comp("-O0 -glldb");
#else
        make = new builder_gcc(target, BUILD_TYPE::BIN, stdout);
        make->add_comp("-x c++ -std=c++17 -pthread "
                       "-fno-rtti -fcompare-debug-second -fexceptions -fuse-cxa-atexit "
                       "-Wall -Wno-unused-result");
        if (debug)
            make->add_comp("-O0 -ggdb");
#endif
        if (args.is_set("-V")) {
            verbose = true;
            make->set(BUILD_OPT::VERBOSE);
            cout << "Using " << src.get_path() << " as a source file.\n";
        }
        make->add_sources(sources);
        if (args.is_set("-c4s")) {
            ntbs inc("-I");
            inc += args.get_value("-c4s");
            make->add_comp(inc);
        } else {
            if (args.is_set("--dev"))
                make->add_comp("-I..");
            else
                make->add_comp("-I/usr/local/include");
        }
        make->add_link("-lc4s");
        if (debug) {
            if (args.is_set("--dev"))
                make->add_link("-L../debug");
            else
                make->add_link("-L/usr/local/lib-d");
        } else {
            make->add_comp("-DNDEBUG -O2");
            make->add_link("-L/usr/local/lib");
        }
        if (args.is_set("-inc"))
            make->add_comp(args.get_value("-inc"));
        if (args.is_set("-lib"))
            make->add_link(args.get_value("-lib"));
#ifdef C4S_UNITTEST
        utlog = fopen("build_trace.log", "wb");
#endif
        BUILD_STATUS bs = make->build();
        if (builder::is_fail_status(bs)) {
            cout << "Build failed: " << (int)bs << "\n";
            delete make;
            return 2;
        } else
            cout << "Target: " << src.get_base() << " ready.\n";
        delete make;
#ifdef C4S_UNITTEST
        fclose(utlog);
#endif

    } catch (const c4s_exception& ce) {
        cout << "Error: " << ce.what() << '\n';
        if (make)
            delete make;
        return 1;
    }
    return 0;
}
