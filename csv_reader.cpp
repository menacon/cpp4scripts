// csv_reader.cpp

#include <stdio.h>

#include "libconfig.hpp"
#include "exception.hpp"
#include "ntbs.hpp"
#include "logger.hpp"
#include "config.hpp"
#include "csv_reader.hpp"

namespace c4s
{

csv_reader::csv_reader(const c4s::path& fname, int _separator)
    : separator(_separator)
{
    fhandle = fopen(fname.get_ccpath(), "rb");
    if (!fhandle) {
        ntbs_lg err_str;
        err_str.sprint("csv_reader - unable to open: %s", fname.get_ccpath());
        throw c4s_exception(err_str.get());
    }
    line_no = 1;
    parse_next();
    header = line;
}

csv_reader::~csv_reader()
{
    if (fhandle)
        fclose(fhandle);
}

bool csv_reader::parse_next()
{
    int ndx = 0;
    ntbs_md cell;
    int ch = 0;

    if (!fhandle || feof(fhandle)) {
        return false;
    }
    while((ch = fgetc(fhandle)) != EOF && !ferror(fhandle)) {
        if (ch == '\r') {
            continue;
        }
        else if (ch == separator) {
            if (has_any(TRIM_CELLS))
                cell.trim(ntbs::BOTH);
            add_cell(ndx++, cell);
            cell.clear();
        }
        else if (ch == '\n') {
            if (!ndx) {
                line.clear();
                if (!has_any(ALLOW_EMPTY_LINES)) {
                    char err_str[100];
                    snprintf(err_str, sizeof(err_str), "csv_reader::parse_next - empty line at %ld", line_no);
                    throw c4s_exception(err_str);
                }
            }
            break;
        }
        else
            cell += (char) ch;
    }
    if (ferror(fhandle)) {
        char err_str[100];
        snprintf(err_str, sizeof(err_str), "csv_reader::parse_next - read error on line %ld", line_no);
        throw c4s_exception(err_str);
    }
    if (ndx)
        add_cell(ndx, cell);
    else {
        line.clear();
        if (feof(fhandle)) {
            line_no--;
            return false;
        }
    }
    if (!feof(fhandle))
        line_no++;
    return true;
}

};