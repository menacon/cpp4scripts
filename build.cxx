/* Cpp4Scripts library make program.
 *
 * To compile this builder, please use egg.sh
 *
 * Runtime dependensies: Doxygen for publishing. Both must exist in path.
 * ---
 * This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#include <stdio.h>
// #include <list>

#include "exception.hpp" // -> ntbs.hpp & libconfig.hpp
#include "util.hpp"
#include "path.hpp"
#include "path_list.hpp"
#include "compiled_file.hpp"
#include "process.hpp"
#include "program_arguments.hpp"
#include "user.hpp"
#include "builder.hpp"
#ifdef __clang__
#include "builder_clang.hpp"
#else
#include "builder_gcc.hpp"
#endif

using namespace c4s;

program_arguments args;
#ifdef C4S_UNITTEST
FILE* utlog;
#endif

const char* cpp_list = "builder.cpp  builder_gcc.cpp builder_clang.cpp "
                       "exception.cpp file.cpp logger.cpp "
                       "path.cpp path_list.cpp program_arguments.cpp util.cpp "
                       "config.cpp process.cpp user.cpp "
                       "ring_buffer.cpp ntbs.cpp tm_datetime.cpp "
                       "csv_reader.cpp version.cpp";

int install(const ntbs& install_dir);

int documentation()
{
    puts("Creating documentation");
    try {
        if (process("doxygen", "c4s-doxygen.dg")()) {
            puts("Doxygen error.");
            return 1;
        }
    } catch (const c4s_exception& ce) {
        printf("Error: %s\n", ce.what());
        return 1;
    }
    puts("OK");
    return 0;
}

bool create_version_h()
{
    bool rv = true;
    ntbs c4s_version;

    FILE* vh = fopen("version.cpp", "wb");
    FILE* pv = fopen("doxy_prj_version.txt", "wb");
    if (!vh || !pv) {
        printf("Unable to create version files(%d)", errno);
        if (vh) fclose(vh);
        if (pv) fclose(pv);
        return false;
    }
    try {
        if (!process::query("git", "describe --tags --dirty", &c4s_version)) {
            c4s_version.trim();
            fprintf(vh, "namespace c4s {\n"
                    "const char* get_c4s_version()\n"
                    "{\n"
                    "    return \"%s\";\n"
                    "}\n"
                    "} // namespace\n" , c4s_version.get());
            fprintf(pv, "PROJECT_NUMBER = \"%s\"\n", c4s_version.get());
        } else {
            printf("Error: git describe result: %s\n", c4s_version.get());
            rv = false;
        }
    } catch (const c4s_exception& c4) {
        puts("Warning: git not available!");
        fputs("const char* get_c4s_version()\n"
              "{\n"
              "    return \"-.-.-\";\n"
              "}\n", vh);
        fputs("PROJECT_NUMBER = \"-.-.-\"\n", pv);
    }
    fclose(vh);
    fclose(pv);
    return rv;
}

int build(FILE* log)
{
    if (!create_version_h()) {
        puts("Error: unable to create version.hpp\n"
             "Please make sure that you have pulled tags from git.");
        return 2;
    }

    path_list cppFiles(cpp_list, ' ');
    path_list plmkc4s;
    plmkc4s += path("makec4s.cpp");
    ntbs_lg build_params;
    const char* debug_params;

    builder* br;
    builder* make;
    try {
#ifdef __clang__
        puts("Using clang compiler.");
        br = new builder_clang(ntbs("c4s"), BUILD_TYPE::LIB, log);
        br->add_sources(cppFiles);
        make = new builder_clang(ntbs("makec4s"), BUILD_TYPE::BIN, log);
        make->add_sources(plmkc4s);

        build_params = "-std=c++17 -march=native -pthread "
                       "-fno-rtti -fno-diagnostics-show-option -fno-diagnostics-fixit-info ";
        debug_params = "-O0 -glldb";
#ifndef __APPLE__
        build_params += "-stdlib=libc++ ";
#endif
#else
        br = new builder_gcc(cppFiles, ntbs("c4s"), log);
        make = new builder_gcc(plmkc4s, ntbs("makec4s"), log);
        build_params = "-fno-rtti -fexceptions -pthread -fuse-cxa-atexit -std=c++17 "
                       "-Wall -Wundef -Wno-unused-result -Wno-reorder";
        debug_params = "-O0 -ggdb";
#endif
    } catch (const c4s_exception& ce) {
        printf("Unable to create compiler: %s\n", ce.what());
        return 2;
    }

    br->add_comp(build_params.get());
    make->add_comp(build_params.get());
#if defined(__GNUC__) && !defined(__clang__)
    br->add_comp("-fcompare-debug-second");
    make->add_comp("-fcompare-debug-second");
#endif
    br->add_comp("-c");

    if (!args.is_set("-deb") && !args.is_set("-ut") && !args.is_set("-rel")) {
#ifdef __APPLE__
        // This piece of code is for Xcode which passes build options via environment.
        ntbs scheme;
        if (!get_env_var("DEBUGGING_SYMBOLS", scheme)) {
            puts("Missing target! Please specify DEBUGGING_SYMBOLS environment variable.");
            return 2;
        }
        if (scheme == "YES") {
            args += argument("-deb");
        }
#else
        puts("Missing build type: either -deb or -rel should be specified.");
        return 2;
#endif
    }
    if (args.is_set("-deb") || args.is_set("-ut")) {
        br->add_comp(debug_params);
        make->add_comp(debug_params);
        if (args.is_set("-ut")) {
            br->add_comp("-DC4S_UNITTEST");
            make->add_comp("-DC4S_UNITTEST");
        }
    } else if (args.is_set("-rel")) {
        br->add_comp("-DNDEBUG -O2");
        make->add_comp("-DNDEBUG -O2");
    }

    ntbs_xs build_dir(args.is_set("-rel") ? "./release" : "./debug");
    br->set_build_dir(build_dir);
    make->set_build_dir(build_dir);
    if (args.is_set("-V")) {
        br->add(BUILD_OPT::VERBOSE);
        make->add(BUILD_OPT::VERBOSE);
    }
    if (args.is_set("-export"))
        br->add(BUILD_OPT::EXPORT);

    if (!args.is_set("makec4s")) {
        puts("Building library.");
        try {
            if (builder::is_fail_status(br->build())) {
                puts("Build failed!");
                delete br;
                return 2;
            }
        } catch (const c4s_exception& ce) {
            printf("make build failed: %s\n", ce.what());
            return 1;
        }
    }
    if (args.is_set("-export")) {
        br->export_prj(args.get_value("-export"), args.exe, args.exe);
        delete br;
        return 0;
    }
    delete br;
    if (args.is_set("lib"))
        return 0;

    puts("\nBuilding makec4s");

    ntbs_sm lib_dir;
    lib_dir = "-L";
    lib_dir += make->get_build_dir();
    make->add_link("-lc4s");
    make->add_link(lib_dir.get());
    try {
        if (builder::is_fail_status(make->build())) {
            puts("\nBuild failed!");
            delete make;
            return 2;
        } else {
            puts("Compilation ready.");
#if defined AUTOINSTALL && !__APPLE__
            install(ntbs("/usr/local/"));
#endif
        }
    } catch (const c4s_exception& ce) {
        printf("makec4s build failed: %s\n", ce.what());
        delete make;
        return 1;
    }
    delete make;
    return 0;
}

int clean()
{
    puts("Cleaning build and temp files.");
    path("./debug/").rmdir(true);
    path("./release/").rmdir(true);

    path_list tmp(args.exe, ntbs("\\.log$"), ntbs());
    tmp.add(args.exe, ntbs("~$"), ntbs());
    tmp.add(args.exe, ntbs("\\.obj$"), ntbs());
    tmp.add(args.exe, ntbs("\\.ilk$"), ntbs());
    tmp.add(args.exe, ntbs("\\.pdb$"), ntbs());
    tmp.add(args.exe, ntbs("^makec4s-"), ntbs());
    tmp.rm_all();
    puts("Done!");
    return 0;
}

int install(const ntbs& install_dir)
{
    path lbin;
    path home(args.exe);
    path_iterator pi;
    path make_name;

    puts("Installing Cpp4Scripts");

    if (!args.is_set("-deb") && !args.is_set("-rel")) {
        puts("Targets (-deb | -rel) not specified. Nothing to do.");
        return 1;
    }

    path inst_root(install_dir);
    if (!inst_root.dirname_exists()) {
        printf("Installation root directory %s must exist.\n", inst_root.get_ccpath());
        return 1;
    }
    // Set up the target directories
    path inc(inst_root);
    inc += "include/cpp4scripts/";
    if (!inc.dirname_exists())
        inc.mkdir();

    path dlib("debug/libc4s.a");
    path rlib("release/libc4s.a");

    int lib_count = 0;
    if (args.is_set("-deb") && dlib.exists()) {
        path lib(inst_root);
        lib += "lib-d/";
        if (!lib.dirname_exists())
            lib.mkdir();
        dlib.cp(lib, PCF_FORCE);
        if (args.is_set("-V"))
            printf("Copied %s to %s\n", dlib.get_ccpath(), lib.get_ccpath());
        lib_count++;
        make_name.set("debug/makec4s");
    } else if (args.is_set("-rel") && rlib.exists()) {
        path lib(inst_root);
        lib += "lib/";
        if (!lib.dirname_exists())
            lib.mkdir();
        rlib.cp(lib, PCF_FORCE);
        if (args.is_set("-V"))
            printf("Copied %s to %s\n", dlib.get_ccpath(), lib.get_ccpath());
        lib_count++;
        make_name.set("release/makec4s");
    }
    if (lib_count == 0) {
        puts("Neither of the -deb or -rel library was found. Were they built?");
        return 2;
    }

    // Copy headers
    path inc_src(args.exe);
    path_list headers(inc_src, ntbs(".*hpp$"), ntbs());
    if (headers.size() == 0) {
        puts("No C4S headers found!");
        return 2;
    }
    headers.copy_to(inc, PCF_FORCE);

    // Copy makec4s utility
    if (!make_name.exists()) {
        puts("Unable to find makec4s!");
        return 2;
    }
    make_name.cp(path("/usr/local/bin/"), PCF_FORCE);

    puts("Completed");
    return 0;
}

int testfn()
{
    // process::global_log_open("build_g.log");
    create_version_h();
    // process::global_log_close();
    return 0;

    // puts("Nothing here at the moment :-)");
    // return 99;
}

int main(int argc, const char** argv)
{
    int rv = 0;

    args += argument("-deb", false, "Create debug version of library.");
    args += argument("-ut", false, "Create debug version with additional logging for unit test");
    args += argument("-rel", false, "Create release version of library.");
    args += argument("-export", true, "Export project files [ccdb|cmake]");
    args += argument("-doc", false, "Create docbook documentation only.");
    args += argument("-clean", false, "Clean up temporary files.");
    args += argument("-install", false, "Installs library under /usr/local/");
    args += argument("lib", false, "Build library only.");
    args += argument("makec4s", false, "Build the 'makec4s' program only.");
    args += argument("-V", false, "Verbose build mode.");
    args += argument("--version", false, "Creates version files.");
    args += argument("--help", false, "Shows this help");
    args += argument("--test", false, "Run test function");

    puts("CPP4Scripts Builder Program");
#ifdef C4S_UNITTEST
    utlog = fopen("build.log", "w");
#endif
    try {
        args.initialize(argc, argv);
        args.exe.cd();
        if (args.is_set("--test")) {
            rv = testfn();
            goto EXIT_MAIN;
        }
        if (args.is_set("--version")) {
            if (create_version_h())
                puts("Version files created.");
            goto EXIT_MAIN;
        }
        if (args.is_set("--help")) {
            args.usage();
            goto EXIT_MAIN;
        }
    } catch (const c4s_exception& ce) {
        printf("Error: %s\n\n", ce.what());
        args.usage();
        rv = 1;
        goto EXIT_MAIN;
    }
    try {
        if (args.is_set("-clean"))
            return clean();
        if (args.is_set("-install")) {
            // ntbs idir = args.get_value("-install");
            // append_slash(idir);
            rv = install(ntbs("/usr/local/"));
            goto EXIT_MAIN;
        }
    } catch (const c4s_exception& ce) {
        printf("Operation failed: %s\n", ce.what());
        rv = 1;
        goto EXIT_MAIN;
    }
    if (args.is_set("-doc")) {
        rv = documentation();
        goto EXIT_MAIN;
    }
    try {
        rv = build(stdout);
    } catch (const c4s_exception& ce) {
        printf("Build failed: %s\n", ce.what());
        rv = 2;
        goto EXIT_MAIN;
    }
EXIT_MAIN:
#ifdef C4S_UNITTEST
    fclose(utlog);
#endif
    return rv;
}
