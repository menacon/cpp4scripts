/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#ifndef C4S_PATH_HPP
#define C4S_PATH_HPP

#include <vector>
#include "ntbs.hpp"

namespace c4s {

class path_list;
class user;

/* Flags and options for path's copy function. */
const int PCF_NONE      = 0;    //!< Path-copy: No flags
const int PCF_FORCE     = 0x1;  //!< Path-copy: Overwrite if target exists.
const int PCF_MOVE      = 0x2;  //!< Path-copy: Move the file i.e. delete original.
const int PCF_APPEND    = 0x4;  //!< Path-copy: Append to possibly existing file.
const int PCF_ONAME     = 0x8;  //!< Path-copy: Use original base name for the target.
const int PCF_DEFPERM   = 0x10; //!< Path-copy: Use default file permissions given by operating system.
                                //!< Note,this also overrides possible user and permission settings for the target.
const int PCF_BACKUP    = 0x20; //!< Path-copy: Backup original if it exists.
const int PCF_RECURSIVE = 0x40; //!< Path-copy: Copy recursively. Valid only if source is a directory (i.e. base is empty).

//! Flags for path compare function.
const unsigned char CMP_DIR = 1;  //!< Compare Dir parts together
const unsigned char CMP_BASE = 2; //!< Compare Base parts together

/// Definitions for path ownership status
enum class OWNER_STATUS : unsigned short int
{
    OK,          //!< 0 - Owner+mode exists and matches actual owner+mode
    EMPTY,       //!< 1 - Owner not specified
    MISSING,     //!< 2 - Owner does not exist in system
    NOPATH,      //!< 3 - Path does not exist
    NOMATCH_UG,  //!< 4 - Actual owner/group does not match given user/group
    NOMATCH_MODE //!< 5 - Actual mode does not match given mode.
};

// -----------------------------------------------------------------------------------------------------------
//! Class that encapsulates a path to a file or directory.
/*! Path has directory part (dir) and file name part (base). File name includes the extension if
  there is one. Dir and base can be set and queried together or separately. If set together the
  library separates the dir part from base. Path can be relative.<br> Defines:<br>
  C4S_FORCE_NATIVE_PATH = If this define is set during compile time, it enforces native path
  separators to directory. It is recommended if the same code is to be used in different
  environments.
*/
class path
{
public:
    //! Default constructor that initializes empty path.
    path();
    //! Copy constructor.
    path(const path& p) { *this = p; }
    // path from single string.
    path(const char* p)
    {
        init_common();
        set(ntbs(p));
    }
    path(const ntbs& p);
    //! Constructs a path from directory and base
    path(const ntbs& d, const ntbs& b);
    //! Path constructor. Combines path from directory, base and extension.
    path(const ntbs& d, const ntbs& b, const ntbs& e);
    //! Constructs path from dir part and given base.
    path(const path& dir, const char* base, user* o = 0, int m = -1);
    //! Constructs path with user data
    path(const ntbs& d, const ntbs& b, user* o, int m = -1);
    //! Constructs path with user data
    path(const ntbs& p, user* o, int m = -1);

    //! Sets path so that it equals another path.
    void operator=(const path& p);
    //! Sets the path from pointer to const char.
    void operator=(const char* p) { set(ntbs(p)); }
    //! Sets the path from constant string.
    void operator=(const ntbs& p) { set(p); }
    //! Synonym for merge() function
    void operator+=(const path& p) { merge(p); }
    //! Synonym for merge() function
    void operator+=(const char* cp) { merge(path(ntbs(cp))); }

    //! Clears the path.
    void clear()
    {
        change_time = 0;
        dir.clear();
        base.clear();
    }
    //! Checks whether the path is clear (or empty). \retval bool True if empty.
    bool empty() { return dir.empty() && base.empty(); }

    //! Returns the directory part of full path
    ntbs get_dir() const { return dir; }
    //! Returns the directory part without the trailin slash.
    ntbs get_dir_plain() const { return dir.substr(0, dir.length() - 1); }
    //! Returns the directory part as array of sub-directories
    void get_dir_parts(std::vector<ntbs>& vs) const;
    //! Returns the base part with extension.
    ntbs get_base() const { return base; }
    //! Returns the base and swaps its extension to the one given as parameter.
    ntbs get_base(const ntbs& ext) const;
    //! Returns the base without the extension
    ntbs get_base_plain() const;
    //! Returns the base or if it is empty the last directory entry.
    ntbs get_base_or_dir();
    //! Returns the extension from base if there is any.
    ntbs get_ext() const;
    //! Returns the complete path.
    ntbs get_path() const { return dir + base; }
    void get_path(ntbs& full);
    //! Returns static ntbs content. Path is overwritten at each call. Don't store for long!
    const char* get_ccpath() const;
    const char* get_ccdir() const { return dir.get(); }
    const char* get_ccbase() const { return base.get(); }
    //! Returns the full path with quotes if the file name contains any spaces.
    ntbs get_path_quot() const;

    //! Sets the directory part of the path.
    void set_dir(const ntbs& d);
    //! Sets the directory part to user's home directory.
    void set_dir2home();
    //! Changes the base (=file name) part of the path.
    void set_base(const ntbs& b);
    //! Sets the extension for the file name.
    void set_ext(const ntbs& e);
    //! Sets the path components by parsing the given string
    void set(const ntbs& p);
    //! Sets path attributes from given directory name, base name
    void set(const ntbs& d, const ntbs& b);
    //! Sets path attributes from given directory name, base name and extension
    void set(const ntbs& d, const ntbs& b, const ntbs& e);
    //! Appends given string to base part (before extension)
    void base_append(const char* str);
    //! Prepends given string to base part
    void base_prepend(const char* str);

    //! Sets the user for this path. Does not commit change to disk.
    void set_owner(user* u) { owner = u; }
    //! Sets the file mode for this path. Does not commit change to disk.
    void set_mode(int _mode) { mode = _mode; }

    //! Changes current working directory to given path.
    static void cd(const char*);
    //! Changes current working directory to the directory stored in this object.
    void cd() const { cd(dir.get()); }
    //! Reads the current workd directory and sets it to dir-part. Base is not affected.
    void read_cwd();

    //! Verifies that owner exists and the is owner of this path.
    OWNER_STATUS owner_status();
    //! Reads the owner information from the path.
    void owner_read();
    //! Writes the current owner to disk, i.e. possibly changes ownership
    void owner_write();
    //! Returns true if the owner has bee defined for this path.
    user* get_owner() { return owner; }
    //! Returns current file mode
    int get_mode() { return mode; }
    //! Reads current path mode from file system.
    void read_mode();
    //! Changes file / directory permissions.
    void chmod(int mod = -1);

    //! Read both owner and mode.
    void read_owner_mode()
    {
        owner_read();
        read_mode();
    }
    // Changes both owner and mode for the path and commits changes to disk.
    void ch_owner_mode(user* _owner = 0, int _mode = -1)
    {
        if (_owner)
            owner = _owner;
        if (_mode != -1)
            mode = _mode;
        if (owner)
            owner_write();
        if (mode != -1)
            chmod();
    }

    //! Returns true if path has directory part.
    bool is_dir() const { return dir.empty() ? false : true; }
    //! Returns true if path has a base i.e. filename
    bool is_base() const { return base.empty() ? false : true; }
    //! Returns true if the path is absolute, false if not.
    bool is_absolute() const;
    //! Makes the path absolute if it is relative. Otherwice it does nothing.
    void make_absolute();
    //! Makes this path absolute based on a given root directory.
    void make_absolute(const ntbs& root);
    //! Makes the path relative to the current working directory.
    void make_relative();
    //! Makes the path relative to the given parent directory.
    void make_relative(const path&);
    //! Rewinds the directory down to its parent as many times as given in parameter.
    void rewind(int count = 1);
    //! Merges two paths.
    void merge(const path&);
    //! Appends the last directory from source to this directory.
    void append_last(const path&);
    //! Appends given directory to the directory part.
    void append_dir(const char*);
    //! Normalizes path
    void normalize();

    //! Checks if the base exists in any of the directories specified in the given environment
    //! variable.
    bool exists_in_env_path(const char* envar, bool set_dir = false);
    //! Returns true if the directory specified by the path exists in the file system. False if not.
    bool dirname_exists() const;
    //! Checks if the directory and base exists.
    bool exists() const;
    //! Returns true if path exists and updates the change time.
    bool get_stat();

    //! Sets a selection flag.
    void flag_set() { flag = true; }
    //! Toggles the selection flag.
    void flag_toggle() { flag = !flag; }
    //! Returns the current selection.
    bool flag_get() { return flag; }

    //! compares full path or just the base depending on flag
    int compare(const path& target, unsigned char flag) const;

    //! Creates the directory specified by the directory part of the path.
    void mkdir() const { mkdir(owner, mode); }
    void mkdir(c4s::user* owner, int mode) const;

    //! Removes the directory from the disk this path points to.
    void rmdir(bool recursive = false) const;

    //! Copy file pointed by path to a new location
    int cp(const char* to, int flags = PCF_NONE)
    {
        path target(to);
        return cp(target, flags);
    }
    //! Copy file pointed by path to a new location
    int cp(const path&, int flags = PCF_NONE) const;
    //! Concatenate file
    void cat(const path&);
    //! Rename the base part or the path
    void rename(const ntbs&, bool force = false);
    //! Rename = move the file within same file system
    void rename(const path&, bool force = false);
    //! Remove / delete file.
    bool rm() const;
    //! Makes named symbolic link to this path
    void symlink(const path&) const;

    //! Returns the last change time of this path
    time_t get_changetime() const { return change_time; }
    //! Compares this files timestamp to given targets timestamp.
    int compare_times(path&);

    //! Changes all directory separators from unix '/' to dos '\'.
    void unix2dos();
    //! Changes all directory separators from dos '\' to unix '/'.
    void dos2unix();
    // SIZE_T search_text(const std::string &needle);
    //! Print path attributes into a given stream
    void dump(std::ostream&);

private:
    //! Common functionality for all constructors
    void init_common();
    //! Copies attributes and permissions from this file to target.
    void copy_mode(const path& target) const;
    //! Recursive copy from this to target.
    int copy_recursive(const path&, int) const;

    c4s::user* owner;   //!< Pointer to User and group for this file's permissions
    int mode;           //!< Path/file access mode.
    time_t change_time; //!< Cached time that the file was last changed.
    ntbs dir;  //!< directory part of the path. Directory needs to end at the directory separator.
    ntbs base; //!< Base name (file name) part of the path.
    bool flag; //!< General purpose flag for application use.
    friend class path_list;
    friend bool compare_paths(c4s::path fp, c4s::path sp);
};

} // namespace c4s
#endif
