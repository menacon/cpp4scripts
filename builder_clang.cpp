/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#include <stdio.h>

#include "libconfig.hpp"
#include "exception.hpp"
#include "path.hpp"
#include "path_list.hpp"
#include "process.hpp"
#include "compiled_file.hpp"
#include "util.hpp"
#include "builder.hpp"
#include "builder_clang.hpp"

using namespace std;
using namespace c4s;

namespace c4s {

builder_clang::builder_clang(const ntbs& name, BUILD_TYPE type, FILE* log, const char *custom)
    : builder(name, type, "clang++", "clang++", log)
{
    if (custom) {
        compiler = custom;
        linker = custom;
    }
}

// -------------------------------------------------------------------------------------------------
void builder_clang::set_debug_params(const char* params)
{
    if (params == nullptr) {
        c_opts << "-glldb -O0 ";
    } else {
        c_opts << params << ' ';
    }
}

// -------------------------------------------------------------------------------------------------
BUILD_STATUS builder_clang::build()
{
    if (log && has_any(BUILD_OPT::VERBOSE))
        fprintf(log, "builder_clang::build - Initializing with %s as compiler\n", compiler.get_command().get_ccpath());
    if (type == BUILD_TYPE::LIB)
        linker = "ar";

    if (!sources.size())
        throw c4s_exception("builder_clang::build - no sources to build.");
    else if (log && has_any(BUILD_OPT::VERBOSE)) {
        fprintf(log, "  compiling %ld source files.\n", sources.size());
        if (build_dir.length())
            fprintf(log, "  build dir '%s'\n", build_dir.get());
        else
            fputc('\n', log);
    }

    // Determine the real target name.
    if (type == BUILD_TYPE::LIB) {
        l_opts << "-rcs ";
    } else if (type == BUILD_TYPE::SO) {
        c_opts << "-fpic ";
        l_opts << "-shared -fpic ";
    }
    if (has_any(BUILD_OPT::EXPORT))
        return BUILD_STATUS::OK;
    // Note: This produces too much output. TODO: Add separate flag for it.
    // if (has_any(BUILD_OPT::VERBOSE)) {
    //     c_opts << "-v ";
    //     l_opts << "-v ";
    // }
    // Make sure build dir exists
    path buildp(build_dir);
    if (!buildp.dirname_exists()) {
        if (log && has_any(BUILD_OPT::VERBOSE))
            fprintf(log, "builder_clang - created build directory: %s\n",buildp.get_ccpath());
        buildp.mkdir();
    }
    // Only one file?
    if (sources.size() == 1) {
        ntbs_lg params;
        path src = sources.front();
        params = c_opts.str();
        if (build_dir.length())
            params.addprint(" -o %s%s %s ", build_dir.get(), target.get(), src.get_ccbase());
        else
            params.addprint(" -o %s %s ", target.get(), src.get_ccbase());
        params += l_opts.str();
        try {
            if (log) {
                if (has_any(BUILD_OPT::VERBOSE)) {
                    fprintf(log, "Compiling %s:\n", src.get_ccbase());
                    fprintf(log, "Compile parameters: %s\n", params.get());
                }
                for (compiler.start(params.get()); compiler.is_running();) {
                    compiler.rb_out.read_into(log);
                    compiler.rb_err.read_into(log);
                }
            } else
                compiler(params.get());
            return compiler.last_return_value() == 0 ? BUILD_STATUS::OK : BUILD_STATUS::ERROR;
        } catch (const c4s_exception& ce) {
            if (log)
                fprintf(log, "builder_clang::build - Failed: %s\n", ce.what());
        }
        return BUILD_STATUS::ERROR;
    }
    // Call parent to do the job
    if (log && has_any(BUILD_OPT::VERBOSE))
        builder::print(log);
    BUILD_STATUS bs = builder::compile(".o", "-o ");
    if (bs == BUILD_STATUS::OK) {
        if (type == BUILD_TYPE::LIB)
            bs = builder::link(".o", 0);
        else
            bs = builder::link(".o", "-o ");
    }
    if (log && has_any(BUILD_OPT::VERBOSE))
        fprintf(log,"builder_clang::build - build status = %d\n", bs);
    return bs;
}

} // namespace c4s