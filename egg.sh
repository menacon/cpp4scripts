#!/bin/bash
# Script that compiles the library builder. It seemed counter productive to use existing make framework
# to compile this library since large portion of library code is about creating build systems i.e. make
# framework. This creates chicken-egg problem: how to build builder. This is solved by creating the egg
# first. A small script that creates build binary that can be used to build the library itself.
#
# Copyright (c) Menacon Oy
# License: http://www.gnu.org/licenses/lgpl-2.1.html
# Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
# any kind
#

SOURCES="exception.cpp ntbs.cpp builder.cpp builder_clang.cpp builder_gcc.cpp path.cpp path_list.cpp process.cpp program_arguments.cpp ring_buffer.cpp user.cpp util.cpp tm_datetime.cpp logger.cpp"

# apt install clang++
clang_build() {
	# -O0 -glldb
	clang++ -std=c++17 -stdlib=libc++\
	  -Wall -pthread -fcxx-exceptions -fno-rtti\
	  $SOURCES -o build build.cxx
}

# apt install g++
gcc_build() {
	# -O0 -ggdb
	g++ -std=c++17 -lstdc++\
     -Wall -Wno-reorder -Wno-unknown-pragmas\
     -pthread -fcompare-debug-second -fexceptions -fno-rtti -fuse-cxa-atexit \
     $SOURCES -o build build.cxx
}

clang_build