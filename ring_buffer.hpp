/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Note: This file is also part of libfcgi.
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#ifndef C4S_RINGBUFFER_HPP
#define C4S_RINGBUFFER_HPP

#include <cstdio>
#include <iostream>

#include "ntbs.hpp"

namespace c4s {

class ringb_callback
{
public:
    ringb_callback() {}
    virtual ~ringb_callback() {}
    virtual void init_push(size_t, void*) = 0;
    virtual void push_back(char ch) = 0;
    virtual void end_push() = 0;
};

/** Ring buffer allows continuous write-read cycles without the need to reset the buffer.
 * Buffer is typically used in communication interfaces to read asynchronous streams.
 */
class ring_buffer
{
public:
    /// Creates a ring buffer with named buffer size.
    ring_buffer(size_t max);
    /// Delete and release buffer memory
    ~ring_buffer();
    /// Clears the internal buffer by resetting read and write pointers to beginning.
    void clear();
    /// Changes the internal buffer size.
    void resize(size_t);

    /// Write null terminated string.
    size_t write(const char* data) { return write((void*)data, strlen(data)); }
    /// Write binary data.
    size_t write(const void*, size_t);
    /// Write data by reading given file.
    size_t write_from(int fd);
    /// Push single charactor into the buffer. If buffer is full the oldest character is discarded.
    void putc(int ch);
    /// Get single character from the buffer.
    int getc();

    /// Read and add null termination. Actual amount of data read is slen-1.
    size_t read(ntbs& target, size_t count);
    void read(ntbs&, bool realloc = false);
    /// Read binary data.
    size_t read_bin(void*, size_t);
    size_t read_into(std::string&);
    size_t read_into(std::ostream&);
    size_t read_into(int fd, size_t len);
    size_t read_into(std::FILE* fd, size_t len = SIZE_MAX);
    size_t read_line(std::ostream&, bool partial_ok = false);
    /// Read line into ntbs object
    size_t read_line(ntbs&, bool partial, bool realloc);
    size_t read_max(void*, size_t, size_t, bool);

    size_t peek(void*, size_t) const;
    bool compare(const ntbs&) const;

    size_t length() const;
    size_t max_size() const { return rb_max; }
    bool   eof() const { return eof_b; }
    bool   empty() const { return empty_b; }
    size_t capacity() const;

    /// Number of bytes read in last read operation
    size_t gcount() { return last_read; }
#ifndef NDEBUG
    /// Number of bytes written into buffer since start or last clean.
    size_t wcount() { return last_write; }
#endif
    void unread(size_t len = 0);
    size_t copy(ring_buffer&, size_t);
    size_t push_to(ringb_callback*, size_t count, void* cb_data);

    enum EXP_TYPE
    {
        TEXT,
        HEX
    };
    size_t exp_as_text(std::ostream&, size_t, EXP_TYPE);
    size_t discard(size_t);

    void dump(std::ostream&);

protected:
    void initialize(size_t);
    bool is_line_available() const;

    size_t rb_max, last_read;
#ifndef NDEBUG
    size_t last_write;
#endif
    char* ringb;
    char* read_ptr;
    char* write_ptr;
    char* end;
    bool eof_b;
    bool empty_b;
};

} // namespace c4s

namespace std {
ostream& operator<<(ostream& os, c4s::ring_buffer& rb);
}
#endif