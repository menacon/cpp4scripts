#include "libconfig.hpp"
#include "exception.hpp"

#include <stdio.h>

namespace c4s {

char c4s_exception::msg_buffer[C4S_EXCEPTION_BUFFER_SIZE];

c4s_exception::c4s_exception(const char* what)
{
    if (what) {
        strncpy(msg_buffer, what, sizeof(msg_buffer) - 1);
        msg_buffer[sizeof(msg_buffer) - 1] = 0;
    } else
        msg_buffer[0] = 0;
}
c4s_exception::c4s_exception(const ntbs& what)
{
    strncpy(msg_buffer, what.get(), sizeof(msg_buffer) - 1);
    msg_buffer[sizeof(msg_buffer) - 1] = 0;
}

rterror::rterror(const char* str, ...) :
    c4s_exception()
{
    va_list va;
    va_start(va, str);
    vsnprintf(msg_buffer, sizeof(msg_buffer) - 1, str, va);
    va_end(va);
};

syserror::syserror(int eid, const char* str, ...)
    :errid(eid)
{
    va_list va;
    va_start(va, str);
    vsnprintf(msg_buffer, sizeof(msg_buffer) - 1, str, va);
    va_end(va);
}

} // namespace c4s