# Introduction #

C++ for scripts (cpp4scripts or c4s for short), is a library of classes and functions that make it
easy to write medium and large size "scripts" with C++. Small add-hock scripts are more feasible to
write with Bash (or possibly Perl, Python, etc) but as the complexity of the script increases it
helps to have things like classes, exceptions, debuggers and logging to name a few. These scripts
still need to be compiled but with current compilers this takes only second or two. Trivial price
to pay for extra speed and power.

Using C++ does create more source code than your average script language as far as the line or
character count is concerned. This library tries to mitigate the gap and does pretty good job at
it. And if you work mostly with C++ then those few extra lines that are needed are faster to write
than trying to learn intricasies of what ever shell you are using.

# Building the library

C4S library includes classes to create custom make files or build scripts. Instead of providing
scripts or make files for this library we make use of the library's functionality to create build
executable that in turn can be used to create and install the library itself.

> ./egg.sh
> ./build -deb
> ./build -rel
> sudo ./build -install /usr/local/

# About ntbs-class

ntbs-class (Null Terminated Byte String) was a major reason for creating version 2.* of this library.
Class is intended to be used in places where `std::string` is not feasible. This is often case with
embeded systems and applications that make extensive use of C-library. It basically acts as a smart
pointer for `const char*` and `char*` types. Class stores only three things: maximum size reserved
for the string, type of allocation and the pointer to null terminated byte string.

Major differences with std::string :

- As with std::string, on assignment more memory is reserved if needed but class only reserves
  required amount i.e. no exponential growth like in std::string. If memory has been allocated, it
  will be automatically released once object goes out of scope.

- There are severel ntbs-classes with predifined sizes allowing alloacting space from stack instead
  of heap.

- ntbs-class does not have any support for standard library streams. Streams don't offer clear
  benefit over buffered streams and carry same memory disadvantage as std::strings.

Actual ntbs string can be accessed with get()-function. It returns pointer to allocated null
terminated memory. This memory can be manipulated just as regular char-buffers can be
manipulated. *Please note that assigning new value to ntbs-object may require a reservation of more
memory and thus invalidates previous pointer received by get()*.

## Samples
Basic declaration and copy

    ntbs foo;               // Initialize empty zero-length object
    ntbs bar(64);           // Reserve 64 byte buffer from heap
    ntbs cee("Init to const string");

    foo = "Hello world";    // Allocate memory for new value
    bar = foo;              // Copy value to existing buffer

Use stack memory and sprint

    ntbs_sm txt;
    txt.sprint("Current unix time = %ld", time());
    fputs(txt.get());

