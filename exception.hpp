/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#ifndef C4S_EXCEPTION_HPP
#define C4S_EXCEPTION_HPP

#include "libconfig.hpp"
#include "ntbs.hpp"

namespace c4s {

class c4s_exception
{
public:
    c4s_exception() {}
    c4s_exception(const char* what);
    c4s_exception(const ntbs& what);
    const char* what() const { return msg_buffer; }

protected:
    static char msg_buffer[C4S_EXCEPTION_BUFFER_SIZE];
};

// runtime error
class rterror : public c4s_exception
{
public:
    rterror(const char*, ...);
};

class syserror: public c4s_exception
{
public:
    syserror(int errid, const char*, ...);
    int get_id() const { return errid; }
protected:
    int errid;
};

} // namespace c4s

#endif
