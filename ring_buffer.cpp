/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Note: This file is also part of libfcgi.
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 */
#include <unistd.h>
#include <stdexcept>
#define __STDC_WANT_LIB_EXT1__ 1
#include <string.h>

#include "libconfig.hpp"
#include "exception.hpp"
#include "ring_buffer.hpp"
#include "util.hpp"

#ifdef C4S_UNITTEST
#include <stdio.h>
extern FILE* utlog;
#endif

namespace c4s {

ring_buffer::ring_buffer(size_t max)
    : rb_max(max)
    , ringb(nullptr)
{
    if (max) {
        ringb = new char[max];
        end = ringb + max;
    }
    clear();
}

ring_buffer::~ring_buffer()
{
    if (ringb)
        delete[] ringb;
}

void ring_buffer::clear()
{
    write_ptr = ringb;
    read_ptr = write_ptr;
    last_read = 0;
    eof_b = false;
    empty_b = true;
#ifndef NDEBUG
    last_write = 0;
#endif
}

/** New size can be smaller or bigger than previous. If new size is smaller the existing data cannot
 * be preserved. Moving to larger buffer the old data is preserved if it exists.
 */
void ring_buffer::resize(size_t max)
{
    if (max == rb_max)
        return;
    if (!ringb && max) {
        ringb = new char[max];
        end = ringb + max;
        rb_max = max;
        clear();
        return;
    }
    char* nbuf = new char[max];
    if (max < rb_max && !eof_b) {
        delete[] ringb;
        ringb = nbuf;
        rb_max = max;
        clear();
#ifdef C4S_UNITTEST
        fputs("..ring_buffer::resize - clear\n", utlog);
#endif
        return;
    }
    rb_max = max;
    eof_b = false;
    if (read_ptr < write_ptr) {
        size_t available = write_ptr - read_ptr;
        memcpy(nbuf, read_ptr, available);
        delete[] ringb;
        ringb = nbuf;
        read_ptr = ringb;
        write_ptr = read_ptr + available;
        end = ringb + rb_max;
#ifdef C4S_UNITTEST
        fputs("..ring_buffer::resize - simple\n", utlog);
#endif
        return;
    }
    size_t part1 = end - read_ptr;
    size_t part2 = write_ptr - ringb;
    memcpy(nbuf, read_ptr, part1);
    memcpy(nbuf + part1, ringb, part2);
    delete[] ringb;
    ringb = nbuf;
    read_ptr = ringb;
    write_ptr = ringb + part1 + part2;
    end = ringb + rb_max;
#ifdef C4S_UNITTEST
    fputs("..ring_buffer::resize - two part\n", utlog);
#endif
}

size_t ring_buffer::write(const void* input, size_t slen)
{
    if (!ringb || eof_b || !input || !slen)
        return 0;
    empty_b = false;
    size_t part1 = 0;
    if (write_ptr >= read_ptr) {
        part1 = end - write_ptr;
        if (slen <= part1) {
            memcpy(write_ptr, input, slen);
            write_ptr += slen;
            if (write_ptr == end)
                write_ptr = ringb;
            if (write_ptr == read_ptr)
                eof_b = true;
#ifndef NDEBUG
            last_write += slen;
#endif
            return slen;
        }
        memcpy(write_ptr, input, part1);
        write_ptr = ringb;
        slen -= part1;
    }
    size_t part2 = read_ptr - write_ptr;
    part2 = slen < part2 ? slen : part2;
    memcpy(write_ptr, (char*)input + part1, part2);
    write_ptr += part2;
    if (write_ptr == read_ptr)
        eof_b = true;
#ifndef NDEBUG
    last_write += part2;
#endif
    return part1 + part2;
}

size_t ring_buffer::write_from(int fd)
{
    ssize_t fd_bytes1 = 0;
    ssize_t fd_bytes2 = 0;
    if (!ringb || eof_b || !fd)
        return 0;
    if (write_ptr >= read_ptr) {
        fd_bytes1 = ::read(fd, write_ptr, end - write_ptr);
        if (fd_bytes1 == 0)
            return 0;
        if (fd_bytes1 < 0) {
            if (errno == EAGAIN)
                return 0;
            goto write_from_err;
        }
        empty_b = false;
        write_ptr += fd_bytes1;
        if (write_ptr == end)
            write_ptr = ringb;
        if (write_ptr == read_ptr) {
            eof_b = true;
#ifndef NDEBUG
            last_write += fd_bytes1;
#endif
            return fd_bytes1;
        }
    } else {
        fd_bytes2 = ::read(fd, write_ptr, read_ptr - write_ptr);
        if (fd_bytes2 == 0)
            return 0;
        if (fd_bytes2 < 0) {
            if (errno == EAGAIN)
                return 0;
            goto write_from_err;
        }
        empty_b = false;
        write_ptr += fd_bytes2;
        if (write_ptr == read_ptr)
            eof_b = true;
    }
#ifndef NDEBUG
    last_write += fd_bytes1 + fd_bytes2;
#endif
    return (size_t)(fd_bytes1 + fd_bytes2);

write_from_err:
    throw rterror("ring_buffer::write_from - file descriptor %d failed with err %d", fd, errno);
    return 0;
}

void ring_buffer::putc(int ch)
{
    *write_ptr++ = ch & 0xff;
    if (write_ptr == end)
        write_ptr = ringb;
    if (eof_b) {
        read_ptr++;
        if (read_ptr == end)
            read_ptr = ringb;
    } else if (write_ptr == read_ptr)
        eof_b = true;
    empty_b = false;
#ifndef NDEBUG
    last_write++;
#endif
}

int ring_buffer::getc()
{
    if (empty_b)
        return 0;
    int ch = *read_ptr++;
    if (read_ptr == end)
        read_ptr = ringb;
    if (write_ptr == read_ptr)
        empty_b = true;
    eof_b = false;
    return ch;
}

size_t ring_buffer::read_bin(void* store, size_t slen)
{
    if (empty_b || !slen || !store || !ringb) {
        return 0;
    }
    eof_b = false;
    size_t br1 = 0, br2 = 0;
    if (read_ptr >= write_ptr) {
        br1 = min_size(end - read_ptr, slen);
        memcpy(store, read_ptr, br1);
        read_ptr += br1;
        if (read_ptr == end)
            read_ptr = ringb;
        if (br1 == slen || read_ptr == write_ptr) {
            last_read = br1;
            if (read_ptr == write_ptr)
                empty_b = true;
            return br1;
        }
        slen -= br1;
    }
    br2 = min_size(write_ptr - read_ptr, slen);
    memcpy((char*)store + br1, read_ptr, br2);
    read_ptr += br2;
    if (read_ptr == write_ptr)
        empty_b = true;
    last_read = br1 + br2;
    return last_read;
}

size_t ring_buffer::read(ntbs& target, size_t slen)
{
    if (empty_b)
        return 0;
    target.realloc(slen, false);
    size_t bytes = read_bin((void*)target.get(), slen - 1);
    target.get()[bytes] = 0;
    target.reset();
    return bytes;
}

void ring_buffer::read(ntbs& output, bool realloc)
{
    if (empty_b || (!realloc && !output.max_size()))
        return;
    size_t offset = 0;
    size_t max = output.max_size();
    if (realloc) {
        offset = output.length();
        output.realloc(length() + offset, true);
        max = length() + 1;
    }
    size_t br = read_bin((void*)(output.get() + offset), max);
    output.get()[offset + br] = 0;
    output.reset();
}

size_t ring_buffer::read_into(std::string& output)
{
    if (empty_b)
        return 0;
    last_read = 0;
    output.reserve(length());
    while (read_ptr != write_ptr || eof_b) {
        output.push_back(*read_ptr);
        read_ptr++;
        last_read++;
        if (read_ptr == end)
            read_ptr = ringb;
        eof_b = false;
    }
    empty_b = true;
    return last_read;
}

size_t ring_buffer::read_into(std::ostream& output)
{
    if (empty_b)
        return 0;
    last_read = 0;
    while (read_ptr != write_ptr || eof_b) {
        output << *read_ptr;
        read_ptr++;
        last_read++;
        if (read_ptr == end)
            read_ptr = ringb;
        eof_b = false;
    }
    empty_b = true;
    return last_read;
}

size_t ring_buffer::read_into(int fd, size_t slen)
{
    if (empty_b || !slen || fd < 0) {
        return 0;
    }
    eof_b = false;
    size_t br1 = 0, br2 = 0;
    if (read_ptr >= write_ptr) {
        br1 = min_size(end - read_ptr, slen);
        ::write(fd, read_ptr, br1);
        if (read_ptr == end)
            read_ptr = ringb;
        if (br1 == slen || read_ptr == write_ptr) {
            last_read = br1;
            return br1;
        }
        slen -= br1;
    }
    br2 = min_size(write_ptr - read_ptr, slen);
    ::write(fd, read_ptr, br2);
    read_ptr += br2;
    if (read_ptr == write_ptr)
        empty_b = true;
    last_read = br1 + br2;
    return last_read;
}

size_t ring_buffer::read_into(std::FILE* fd, size_t slen)
{
    if (empty_b || !slen || !fd) {
        return 0;
    }
    eof_b = false;
    size_t br1 = 0, br2 = 0;
    if (read_ptr >= write_ptr) {
        br1 = min_size(end - read_ptr, slen);
        fwrite(read_ptr, br1, 1, fd);
        if (read_ptr == end)
            read_ptr = ringb;
        if (br1 == slen || read_ptr == write_ptr) {
            if (read_ptr == write_ptr)
                empty_b = true;
            last_read = br1;
            return br1;
        }
        slen -= br1;
    }
    br2 = min_size(write_ptr - read_ptr, slen);
    fwrite(read_ptr, br2, 1, fd);
    read_ptr += br2;
    if (read_ptr == write_ptr)
        empty_b = true;
    last_read = br1 + br2;
    return last_read;
}

size_t ring_buffer::push_to(ringb_callback* rcb, size_t tlen, void* cb_data)
{
    if (empty_b || !rcb || !tlen)
        return 0;
    last_read = 0;
    size_t available = length();
    if (!available)
        return 0;
    size_t push_max = available > tlen ? tlen : available;
    rcb->init_push(push_max, cb_data);
    while ((read_ptr != write_ptr || eof_b) && last_read < push_max) {
        rcb->push_back(*read_ptr);
        read_ptr++;
        last_read++;
        if (read_ptr == end)
            read_ptr = ringb;
        eof_b = false;
    }
    rcb->end_push();
    if (read_ptr == write_ptr)
        empty_b = true;
    return last_read;
}

size_t ring_buffer::read_line(std::ostream& output, bool partial_ok)
{
    last_read = 0;
    if (!ringb)
        return 0;
    if (!partial_ok && !is_line_available())
        return 0;
    while ((read_ptr != write_ptr || eof_b) && *read_ptr != '\n') {
        output.put(*read_ptr);
        read_ptr++;
        last_read++;
        if (read_ptr == end)
            read_ptr = ringb;
        eof_b = false;
    }
    if (*read_ptr == '\n') {
        read_ptr++;
        if (read_ptr == end)
            read_ptr = ringb;
    }
    if (read_ptr == write_ptr)
        empty_b = true;
    return last_read;
}

bool ring_buffer::is_line_available() const
{
    char* check = read_ptr;
    while (check != write_ptr && *check != '\n') {
        check++;
        if (check == end)
            check = ringb;
    }
    if (*check != '\n')
        return false;
    return true;
}

size_t ring_buffer::read_line(ntbs& line, bool partial, bool realloc)
{
    last_read = 0;
    size_t len = line.max_size() - 1;
    if (!ringb || len == 0 || empty_b)
        return 0;
    char* target = line.get();
    while ((read_ptr != write_ptr || eof_b) && *read_ptr != '\n') {
        *target = *read_ptr;
        target++;
        read_ptr++;
        last_read++;
        if (read_ptr == end)
            read_ptr = ringb;
        eof_b = false;
        if (last_read == len) {
            if (realloc) {
                size_t available = length();
                size_t offset = target - line.get();
                line.realloc(line.size() + available, true);
                target = line.get() + offset;
                len += available;
            } else
                break;
        }
    }
    if (*read_ptr == '\n') {
        read_ptr++;
        if (read_ptr == end)
            read_ptr = ringb;
    } else {
        if (!partial) {
            line.clear();
            unread();
            return 0;
        }
    }
    if (read_ptr == write_ptr)
        empty_b = true;
    if (last_read)
        *target = 0;
    line.reset();
    return last_read;
}

size_t ring_buffer::peek(void* store, size_t slen) const
{
    if (!slen || !store)
        return 0;
    size_t available = length();
    if (!available) {
        return 0;
    }
    if (slen > available)
        slen = available;
    size_t fp = end - read_ptr;
    if (slen < fp) {
        memcpy(store, read_ptr, slen);
    } else {
        memcpy(store, read_ptr, fp);
        memcpy((char*)store + fp, ringb, slen - fp);
    }
    return slen;
}

bool ring_buffer::compare(const ntbs& target) const
{
    size_t tlen = target.length();
    if (tlen > length())
        return false;
    char ch;
    char* read_end = write_ptr == ringb ? end - 1 : write_ptr - 1;
    for (size_t ndx = tlen; ndx > 0; ndx--) {
        ch = target.get(ndx - 1);
        if (*read_end != ch) {
            return false;
        }
        read_end--;
        if (read_end < ringb)
            read_end = end - 1;
    }
    return true;
}

size_t ring_buffer::exp_as_text(std::ostream& os, size_t slen, EXP_TYPE type)
{
    unsigned short int sich;
    char ch;
    if (!slen) {
        return 0;
    }
    size_t available = length();
    if (!available) {
        last_read = 0;
        return 0;
    }
    if (slen > available)
        slen = available;
    if (type == HEX)
        os << std::hex;
    while (read_ptr != write_ptr && slen) {
        ch = *read_ptr;
        if (type == HEX) {
            sich = 0xff & ((unsigned short)ch);
            os << sich << ',';
        } else
            os << ch;
        read_ptr++;
        slen--;
        if (read_ptr == end)
            read_ptr = ringb;
    }
    if (type == HEX)
        os << std::dec;
    eof_b = false;
    last_read = slen;
    return slen;
}

size_t ring_buffer::discard(size_t slen)
{
    if (!slen)
        return 0;
    if (slen == SIZE_MAX) {
        clear();
        return 0;
    }

    size_t available = length();
    if (!available) {
        return 0;
    }
    if (slen > available)
        slen = available;
    while ((read_ptr != write_ptr || eof_b) && slen) {
        read_ptr++;
        slen--;
        if (read_ptr == end)
            read_ptr = ringb;
        eof_b = false;
    }
    if (read_ptr == write_ptr)
        empty_b = true;
    return slen;
}

void ring_buffer::unread(size_t rev_request)
{
    if (!ringb || !last_read)
        return;
    if (!rev_request)
        rev_request = last_read;
    else if (rev_request > last_read)
        throw c4s_exception("ring_buffer::unread - unread size overflow");

    read_ptr -= rev_request;
    if (read_ptr < ringb)
        read_ptr = end - (ringb - read_ptr);
    if (last_read >= rev_request)
        last_read -= rev_request;
    eof_b = false;
}

//! Returns number of bytes waiting for reading.
size_t ring_buffer::length() const
{
    if (empty_b || !ringb)
        return 0;
    if (eof_b)
        return rb_max;
    if (write_ptr > read_ptr)
        return write_ptr - read_ptr;
    return (end - read_ptr) + (write_ptr - ringb);
}

//! Returns the remaining write size
size_t ring_buffer::capacity() const
{
    if (eof_b || !ringb)
        return 0;
    if (write_ptr == read_ptr)
        return rb_max;
    if (write_ptr > read_ptr)
        return (end - write_ptr) + (read_ptr - ringb);
    return read_ptr - write_ptr;
}

void ring_buffer::dump(std::ostream& os)
{
    if (!ringb)
        return;
    os << "ring_buffer:\n  begin:" << (const void*)ringb << "; read_ptr:" << (const void*)read_ptr
       << "; write_ptr:" << (const void*)write_ptr << ";\n";
    os << "  length:" << length() << "; capacity:" << capacity() << "; size:" << rb_max;
    os << "; eof:";
    if (eof_b)
        os << "true";
    else
        os << "false";
    os << "; empty:";
    if (empty_b)
        os << "true";
    else
        os << "false";

    if (rb_max < 100) {
        os << "\nchars:";
        for (char* ndx = ringb; ndx < end; ndx++) {
            if (*ndx > 31 && *ndx < 127)
                os << *ndx;
            else
                os << '.';
        }
    }
    os << '\n';
}

} // namespace c4s

std::ostream& std::operator<<(std::ostream& os, c4s::ring_buffer& rb)
{
    rb.read_into(os);
    return os;
}
