#ifndef C4S_NTBS_HPP
#define C4S_NTBS_HPP

#include <string.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <functional>

#include <iterator>
#include <cstddef>

#define NSCAST(T) static_cast<ntbs>(T)

#define NTBS_XS_MAX 24
#define NTBS_SM_MAX 64
#define NTBS_MD_MAX 128
#define NTBS_LG_MAX 256
#define NTBS_XL_MAX 512
#define NTBS_XXL_MAX 1024

namespace c4s {

class ntbs
{
public:
    enum TYPE
    {
        STACK,
        ALLOC,
        CONST
    };
    enum TRIM
    {
        BOTH,
        LEFT,
        RIGHT
    };
    //! Allocate empty string with _max capacity.
    ntbs(size_t _max = 0);
    //! Create CONST type of ntbs string
    ntbs(const char* src)
    {
        if (src) {
            type = CONST;
            len = strlen(src);
            max = len + 1;
            data.store = (char*) src;
        } else
            initialize(CONST);
    }
    //! Take ownership of existing char array
    ntbs(char* src, size_t _max = 0);
    //! Initialize given type of ntbs.
    ntbs(const char* buffer, size_t _max, TYPE t);

    //! Copy constructor
    ntbs(const ntbs& orig);

    //! Compatibility with std::string - create from string
    ntbs(const std::string&);
    //! Compatibility with std::string - assignment override
    void operator=(const std::string&);

    //! Reset internal len attribute after string has been manipulated by external functions.
    void reset();

    virtual ~ntbs()
    {
        if (type == ALLOC)
            delete[] data.store;
    }
    void clear();
    bool empty() const { return !max || data.store[0] == 0 ? true : false; }

    //! Reallocates more space if needed. CONST types are always reallocated and copied.
    void realloc(size_t req_bytes, bool copy = true);

    void operator=(const char*);
    void operator=(const ntbs&);
    void operator+=(const char*);
    void operator+=(char* source)
    {
        if (source)
            operator+=((const char*)source);
    }
    void operator+=(const ntbs& source)
    {
        if (source.size())
            operator+=((const char*)source.data.store);
    }
    void operator+=(char);
    ntbs operator+(const ntbs& right) const;

    // Comparison
    bool operator==(const char* target) const
    {
        if (!max || !target || !len)
            return false;
        return std::strncmp(data.store, target, len) ? false : true;
    }
    bool operator!=(const char* target) const { return !operator==(target); }
    bool operator==(const ntbs& target) const
    {
        if (!max || !target.max || !len || len != target.len)
            return false;
        return std::strncmp(data.store, target.data.store, len) ? false : true;
    }
    bool operator!=(const ntbs& target) const { return !operator==(target); }

    int compare(const ntbs& target) const
    {
        if (!max || !target.max)
            return -1;
        return std::strcmp(data.store, target.data.store);
    }
    int compare(size_t beg, size_t count, const ntbs& needle) const;

    size_t find(int ch, size_t offset = 0) const;
    size_t find(const char* target, size_t offset = 0) const;
    size_t find(const ntbs& target) const { return find(target.data.store); }
    size_t rfind(int ch, size_t offset = SIZE_MAX) const;

    int sprint(const char* fmt, ...);
    int addprint(const char* fmt, ...);

    char& operator[](size_t ndx);
    const char& operator[](size_t ndx) const;
    char* get() const { return max ? data.store : (char*)data.bytes; }
    char get(size_t ndx) const
    {
        size_t len = strlen(data.store);
        if (ndx == SIZE_MAX)
            return data.store[len - 1];
        return ndx < len ? data.store[ndx] : 0;
    }
    char get_last() const { return max ? data.store[len - 1] : 0; }

    TYPE get_type() const { return type; }
    ntbs substr(size_t beg, size_t count = SIZE_MAX) const;
    void substr(size_t beg, size_t count, ntbs& target) const;
    void assign(const ntbs& src, size_t beg, size_t count);
    void assign(const char* src, size_t count);
    void set_const(const char* src);
    void append(const char* src, size_t count);
    void prepend(const char* src, size_t count = SIZE_MAX);
    void replace(size_t beg, size_t count, const ntbs& source);
    void insert(size_t beg, const ntbs& source);
    void erase(size_t from, size_t count = SIZE_MAX);

    bool is_allocated() const { return type == ALLOC ? true : false; }
    size_t size() const { return len; }
    size_t length() const { return len; }
    size_t max_size() const { return max ? max - 1 : 0; }

    /// Copies data pointer from source and changes type to CONST.
    void copy_reference(const ntbs& source);

    void trim(TRIM tt = ntbs::BOTH);
    void trim_end(size_t count = 1);
    void pop_back() { trim_end(1); } // std::string compatibility

    int stoi(int base=0) const { return max ? strtoimax(data.store, nullptr, base) : 0; }
    long stol(int base=0) const { return max ? strtol(data.store, nullptr, base) : 0; }
    long long stoll(int base=0) const { return max ? strtoll(data.store, nullptr, base) : 0; }
    float stof() const { return max ? strtof(data.store, nullptr) : 0; }
    double stod() const { return max ? strtod(data.store, nullptr) : 0; }

    uint64_t hash_fnv(uint64_t salt = 0) const;

    // Allow basic iteration of Ntbs characters
    struct iterator
    {
        using iterator_category = std::forward_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = char;
        using pointer = char*;   // or also value_type*
        using reference = char&; // or also value_type&

        iterator(char* p)
            : cptr(p)
        {
        }

        reference operator*() { return *cptr; }
        pointer operator->() { return cptr; }
        iterator& operator++()
        {
            cptr++;
            return *this;
        }
        iterator operator++(int)
        {
            iterator tmp = *this;
            ++(*this);
            return tmp;
        }
        friend bool operator==(const iterator& a, const iterator& b) { return a.cptr == b.cptr; };
        friend bool operator!=(const iterator& a, const iterator& b) { return a.cptr != b.cptr; };

    private:
        char* cptr;
    };
    iterator begin() { return iterator(data.store); }
    iterator end() { return iterator(data.store + len); }

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wignored-reference-qualifiers"
    // Const iterator of characters
    struct const_iterator
    {
        using iterator_category = std::forward_iterator_tag;
        using difference_type = std::ptrdiff_t;
        using value_type = char;
        using pointer = char*;   // or also value_type*
        using reference = char&; // or also value_type&

        const_iterator(char* p)
            : cptr(p)
        {
        }

        const reference operator*() { return *cptr; }
        const pointer operator->() { return cptr; }
        const_iterator& operator++()
        {
            cptr++;
            return *this;
        }
        const_iterator operator++(int)
        {
            const_iterator tmp = *this;
            ++(*this);
            return tmp;
        }
        friend bool operator==(const const_iterator& a, const const_iterator& b)
        {
            return a.cptr == b.cptr;
        };
        friend bool operator!=(const const_iterator& a, const const_iterator& b)
        {
            return a.cptr != b.cptr;
        };

    private:
        char* cptr;
    };
    const_iterator begin() const { return const_iterator(data.store); }
    const_iterator end() const { return const_iterator(data.store + len); }
#pragma clang diagnostic pop

    void dump(FILE* out = nullptr) const;

protected:
    union
    {
        char bytes[sizeof(char*)];
        char* store;
    } data;

    void initialize(TYPE tt);

    size_t max;
    size_t len;
    TYPE type;
};

// -----------------------------------------------
class ntbs_xs : public ntbs
{
public:
    ntbs_xs()
        : ntbs(buffer, NTBS_XS_MAX)
    {
    }
    ntbs_xs(const char* val)
        : ntbs(buffer, NTBS_XS_MAX)
    {
        ntbs::operator=(val);
    }
    ntbs_xs(const ntbs& val)
        : ntbs(buffer, NTBS_XS_MAX)
    {
        ntbs::operator=(val);
    }
    void operator=(const char* origin) { ntbs::operator=(origin); }
    void operator=(const ntbs& origin) { ntbs::operator=(origin); }

protected:
    char buffer[NTBS_XS_MAX];
};

class ntbs_sm : public ntbs
{
public:
    ntbs_sm()
        : ntbs(buffer, NTBS_SM_MAX)
    {
    }
    ntbs_sm(const char* val)
        : ntbs(buffer, NTBS_SM_MAX)
    {
        ntbs::operator=(val);
    }
    ntbs_sm(const ntbs& val)
        : ntbs(buffer, NTBS_SM_MAX)
    {
        ntbs::operator=(val);
    }
    void operator=(const char* origin) { ntbs::operator=(origin); }
    void operator=(const ntbs& origin) { ntbs::operator=(origin); }

protected:
    char buffer[NTBS_SM_MAX];
};

class ntbs_md : public ntbs
{
public:
    ntbs_md()
        : ntbs(buffer, NTBS_MD_MAX)
    {
    }
    ntbs_md(const char* val)
        : ntbs(buffer, NTBS_MD_MAX)
    {
        ntbs::operator=(val);
    }
    ntbs_md(const ntbs& val)
        : ntbs(buffer, NTBS_MD_MAX)
    {
        ntbs::operator=(val);
    }
    void operator=(const char* origin) { ntbs::operator=(origin); }
    void operator=(const ntbs& origin) { ntbs::operator=(origin); }

protected:
    char buffer[NTBS_MD_MAX];
};

class ntbs_lg : public ntbs
{
public:
    ntbs_lg()
        : ntbs(buffer, NTBS_LG_MAX)
    {
    }
    ntbs_lg(const char* val)
        : ntbs(buffer, NTBS_LG_MAX)
    {
        ntbs::operator=(val);
    }
    ntbs_lg(const ntbs& val)
        : ntbs(buffer, NTBS_LG_MAX)
    {
        ntbs::operator=(val);
    }
    void operator=(const char* origin) { ntbs::operator=(origin); }
    void operator=(const ntbs& origin) { ntbs::operator=(origin); }

protected:
    char buffer[NTBS_LG_MAX];
};

class ntbs_xl : public ntbs
{
public:
    ntbs_xl()
        : ntbs(buffer, NTBS_XL_MAX)
    {
    }
    ntbs_xl(const char* val)
        : ntbs(buffer, NTBS_XL_MAX)
    {
        ntbs::operator=(val);
    }
    ntbs_xl(const ntbs& val)
        : ntbs(buffer, NTBS_XL_MAX)
    {
        ntbs::operator=(val);
    }
    void operator=(const char* origin) { ntbs::operator=(origin); }
    void operator=(const ntbs& origin) { ntbs::operator=(origin); }

protected:
    char buffer[NTBS_XL_MAX];
};

class ntbs_xxl : public ntbs
{
public:
    ntbs_xxl()
        : ntbs(buffer, NTBS_XXL_MAX)
    {
    }
    ntbs_xxl(const char* val)
        : ntbs(buffer, NTBS_XXL_MAX)
    {
        ntbs::operator=(val);
    }
    ntbs_xxl(const ntbs& val)
        : ntbs(buffer, NTBS_XXL_MAX)
    {
        ntbs::operator=(val);
    }
    void operator=(const char* origin) { ntbs::operator=(origin); }
    void operator=(const ntbs& origin) { ntbs::operator=(origin); }

protected:
    char buffer[NTBS_XXL_MAX];
};

} // namespace c4s

namespace std {
template<>
struct hash<c4s::ntbs>
{
    size_t operator()(const c4s::ntbs& target) const { return (size_t)target.hash_fnv(); }
};

ostream& operator<<(ostream& os, const c4s::ntbs& ns);
istream& operator>>(istream& is, c4s::ntbs& ns);

} // namespace std

#endif // C4S_NTBS_HPP