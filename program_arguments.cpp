/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#include <cstring>
#include <time.h>
#if defined(__linux)
#include <stdlib.h>
#elif defined(__APPLE__)
#include <mach-o/dyld.h>
#include <stdlib.h>
#endif
#include "libconfig.hpp"
#include "exception.hpp"
#include "path.hpp"
#include "program_arguments.hpp"
#include "user.hpp"

namespace c4s {

ntbs argument::empty_value;

void argument::set_value(const ntbs& val)
{
    set = true;
    two_part = true;
    if (val.get(0) == '\'')
        value = val.substr(1, val.length() - 2);
    else
        value = val;
}

/*! Valid arguments should have been added into this object before calling this function.
    Exception is thrown if the argument is not found.
  \param argc Parameter to main function = number of arguments in argv
  \param argv Parameter to main function = array of argument strings.
  \param min_args Number of arguments that should be specified in the command line.
                  Excluding the automatic first argument given by the system, i.e. value is zero
  based.
*/
void program_arguments::initialize(int argc, const char* argv[], int min_args)
{
    // Initialize program paths
    argv0 = argv[0];
    cwd.read_cwd();
    char link_path[512];
#if defined(__linux)
    char link_name[512];
    pid_t pid = getpid();
    sprintf(link_name, "/proc/%d/exe", pid);
    int rv = readlink(link_name, link_path, sizeof(link_path));
#elif defined(__APPLE__)
    uint32_t size = sizeof(link_path);
    int rv = _NSGetExecutablePath(link_path, &size) == 0 ? strlen(link_path) : 0;
#endif
    if (rv > 0) {
        link_path[rv] = 0;
        exe = link_path;
    } else
        exe = argv[0];

    // Initialize the random number generator in case we need to use it.
    srand((unsigned int)time(0));

    if (argc < min_args + 1)
        throw rterror("Arguments initialize - Too few arguments specified in command line.");

    // Check the arguments
    std::list<argument>::iterator pi;
    int argi = 1;
    while (argi < argc) {
        // For potential arguments:
        for (pi = arguments.begin(); pi != arguments.end(); pi++) {
            // If found:
            if (!strcmp(argv[argi], pi->get_text())) {
                // Set argument on, and copy value if it is twopart param.
                pi->set_on();
                if (pi->is_two_part()) {
                    if (argi + 1 >= argc) {
                        throw rterror("Arguments initialize - Missing value for argument: %s",
                                      pi->get_text());
                    }
                    pi->set_value(ntbs(argv[argi + 1]));
                    argi++;
                }
                break;
            }
        }
        if (pi == arguments.end()) {
            throw rterror("program_arguments::initialize - Unknown argument: %s\n", argv[argi]);
        }
        argi++;
    }
}

// -------------------------------------------------------------------------------------------------
/*!
  Prints the help on program arguments to stdout. Initialize function should have been called
  before this function.
  \param title A title to print before the arguments.
  \param info A informational message to be printed after the arguments.
*/
void program_arguments::usage()
{
    ntbs paramTxt;
    std::list<argument>::iterator pi;
    printf("Usage: %s", argv0.get_ccbase());
    if (arguments.size() > 0)
        printf(" [Options]\n");
    for (pi = arguments.begin(); pi != arguments.end(); pi++) {
        paramTxt = pi->get_text();
        if (pi->is_two_part())
            paramTxt += " {VAL}";
        printf("  %-20s %s\n", paramTxt.get(), pi->get_info());
    }
    putchar('\n');
}

// -------------------------------------------------------------------------------------------------
/*!
  \param param Pointer to parameter string.
  \retval bool True if argument is set, false if not.
*/
bool program_arguments::is_set(const char* param)
{
    std::list<argument>::iterator pi;
    for (pi = arguments.begin(); pi != arguments.end(); pi++) {
        if (*pi == param)
            return pi->is_on();
    }
    return false;
}

// -------------------------------------------------------------------------------------------------
/*!
  \param param Pointer to parameter string.
  \param value Pointer to value to check.
  \retval bool True if argument is set and correct, false if not.
*/
bool program_arguments::is_value(const char* param, const ntbs& value)
{
    std::list<argument>::iterator pi;
    for (pi = arguments.begin(); pi != arguments.end(); pi++) {
        if (*pi == param) {
            return pi->is_value(value);
        }
    }
    return false;
}

// -------------------------------------------------------------------------------------------------
/*!
   \param param Parameter name.
   \param choises Array of strings terminated by null pointer.
   \retval int Index of the matched choise. -1 if no match or argument is not value type.
*/
int program_arguments::get_value_index(const char* param, const char** choises)
{
    std::list<argument>::iterator pi;
    for (pi = arguments.begin(); pi != arguments.end(); pi++) {
        if (*pi == param) {
            for (int ndx = 0; choises[ndx]; ndx++) {
                if (pi->is_value(ntbs(choises[ndx])))
                    return ndx;
            }
        }
    }
    return -1;
}

// -------------------------------------------------------------------------------------------------
/*!
  \param param Pointer to parameter string
  \retval string Argument value. If value is not specified or argument is not two part type an empty
  string is returned.
*/
ntbs& program_arguments::get_value(const char* param)
{
    static ntbs empty;
    std::list<argument>::iterator pi;
    for (pi = arguments.begin(); pi != arguments.end(); pi++) {
        if (*pi == param && pi->is_two_part()) {
            return pi->get_value();
        }
    }
    return empty;
}

// -------------------------------------------------------------------------------------------------
/*!
   \param param Ptr to argument name
   \param value Ptr to value ntbs.
   \retval bool True on succes, false if argument is not found.
*/
bool program_arguments::set_value(const char* param, const ntbs& value)
{
    std::list<argument>::iterator pi;
    for (pi = arguments.begin(); pi != arguments.end(); pi++) {
        if (*pi == value) {
            pi->set_value(value);
            return true;
        }
    }
    return false;
}

// -------------------------------------------------------------------------------------------------
/*!
  \param param Ptr to argument name.
  \param value Value to append
  \retval bool True on succes, false if argument is not found.
*/
bool program_arguments::append_to_value(const char* param, const ntbs& value)
{
    std::list<argument>::iterator pi;
    for (pi = arguments.begin(); pi != arguments.end(); pi++) {
        if (*pi == param) {
            pi->append_value(value);
            return true;
        }
    }
    return false;
}

} // namespace c4s