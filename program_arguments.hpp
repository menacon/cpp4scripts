/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#ifndef C4S_PROBRAM_ARGUMENTS_HPP
#define C4S_PROBRAM_ARGUMENTS_HPP

namespace c4s {

// -----------------------------------------------------------------------------------------------------------
//! Single program argument given at the command line
class argument
{
public:
    //! Default constructor initializes all values to zero.
    argument()
    {
        two_part = false;
        set = false;
    }
    //! Defines possible program argument text, type and info
    /*! \param aTxt Argument text, e.g. "--type" or "-t"
      \param aTp Two part flag, if true then argument has second part, e.g. "--path /usr/local/src".
      \param aInfo Information text that can be shown about the argument to the user when help is
      requested. */
    argument(const char* aTxt, bool aTp, const char* aInfo)
        : text(aTxt)
        , info(aInfo)
    {
        two_part = aTp;
        set = false;
    }
    //! Add simple argument that is set by default.
    argument(const char* aTxt)
        : two_part(false)
        , set(true)
        , text(aTxt)
    {
    }

    //! Returns possible argument text.
    const char* get_text() const { return text.get(); }
    //! Returns argument information
    const char* get_info() const { return info.get(); }
    //! Returns the current value of this argument.
    const char* get_ccvalue() { return get_value().get(); }
    //! Returns the current value of this argument.
    ntbs& get_value() { return (set && two_part) ? value : empty_value; }

    //! Sets argument into ON state i.e. has been specified in the command line.
    void set_on() { set = true; }
    //! Sets argument's value
    void set_value(const ntbs& value);
    //! Appends the given string into the argument's value.
    void append_value(const ntbs& val) { value += val; }
    //! Returns true if the argument has two parts.
    bool is_two_part() const { return two_part; }
    //! Returns true if the argument is ON i.e specified in command line.
    bool is_on() const { return set; }
    //! Returns true if argument names match
    bool operator==(const char* t) const { return text == t; }
    bool operator==(const ntbs& t) const { return text == t; }
    //! Returns true if argument's value matches given string
    bool is_value(const ntbs& v) const { return set && value == v; }

private:
    bool two_part; //!< If true argument has value associated.
    bool set;      //!< If true argument has been specified in command line.
    ntbs text;     //!< Argument name or text.
    ntbs value;    //!< Value of the argument.
    ntbs info;     //!< Help information

    static ntbs empty_value;
};

// -----------------------------------------------------------------------------------------------------------
//! Manages all program arguments / arguments.
class program_arguments
{
public:
    //! Constructor initializes attributes to default values. Arg list will be empty.
    program_arguments() {}
    //! Initializes the program argument list by matching user parameters to stored argument list.
    void initialize(int argc, const char* argv[], int min_args = 0);
    //! Appends the given argument into the list.
    void append(const argument& arg) { arguments.push_back(arg); }
    //! Appends the given argument into the list.
    void operator+=(const argument& arg) { arguments.push_back(arg); }
    //! Returns number of arguments currently in the list.
    size_t get_size() { return arguments.size(); }
    //! Checks if the given argument has been given in the command line.
    bool is_set(const char*);
    //! Checks if the given argument has been given in the command line and has specified value.
    bool is_value(const char*, const ntbs&);
    //! Sets argument's value.
    bool set_value(const char*, const ntbs&);
    //! Appends a string to the end of the argument's current value.
    bool append_to_value(const char*, const ntbs&);
    //! Returs argument value if the argument was of two part type.
    const char* get_ccvalue(const char* name) { return get_value(name).get(); }
    //! Returns value of the argument if it was of two part type.
    ntbs& get_value(const char*);
    //! Returns the index of the choises for the argument value that matches given choises.
    int get_value_index(const char*, const char**);
    //! Stores the second part for the argument and sets argument on. Strips single quotes from the
    //! value before storing the value.
    void usage();

    typedef std::list<argument>::iterator pai_t;
    //! Returns the iterator to first argument.
    pai_t begin() { return arguments.begin(); }
    //! Returns iterator to the end of the list.
    pai_t end() { return arguments.end(); }

    path argv0; //<! Path to program as reported with argv[0] parameter
    path cwd;   //<! Current directory at the program start.
    path exe;   //<! Path to binary that was used to launch current process

protected:
    std::list<argument> arguments;
};

}; // namespace c4s
#endif
