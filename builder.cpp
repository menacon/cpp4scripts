/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */

#include <cstring>
#include <stdlib.h>

#include "libconfig.hpp"
#include "ntbs.hpp"
#include "exception.hpp"
#include "path.hpp"
#include "path_list.hpp"
#include "process.hpp"
#include "util.hpp"
#include "compiled_file.hpp"
#include "builder.hpp"
#include "program_arguments.hpp"

#ifdef C4S_UNITTEST
#include <stdio.h>
extern FILE* utlog;
#endif

using namespace std;

namespace c4s {

// -------------------------------------------------------------------------------------------------
/** builder base class constructor.
    Files are automatically read from 'git ls-files' command. Files with cpp-extension are
   considered part of of the project. \param _name Name of the target binary \param _log If
   specified, will receive compiler output.
*/
builder::builder(const ntbs& _name, BUILD_TYPE _type, const char *_compiler, const char *_linker, FILE* _log)
    : type(_type)
    , compiler(_compiler)
    , linker(_linker)
    , log(_log)
    , name(_name)
    , target(64)
    , build_dir(128)
    , ccdb_root(128)
{
    generate_target_name();
    if (log) {
        compiler.set_pipe_size(8192, 8192);
        linker.set_pipe_size(8192, 8192);
    }
    compiler.set_timeout(BUILDER_TIMEOUT);
    linker.set_timeout(BUILDER_TIMEOUT);
}
// -------------------------------------------------------------------------------------------------
void builder::generate_target_name()
{
    if (type == BUILD_TYPE::LIB) {
        target = "lib";
        target += name;
        target += ".a";
    } else {
        target = name;
        if (type == BUILD_TYPE::SO)
            target += ".so";
    }
#if 0
    // Windows:
    target = name;
    if (has_any(BUILD::LIB)) {
        linker.set_command("lib");
        target += ".lib";
    } else if (!has_any(BUILD::NOLINK)) {
        linker.set_command("link");
        target += ".exe";
    }
#endif
}
// -------------------------------------------------------------------------------------------------
void builder::add_git_files()
{
    bool verbose = log && has_any(BUILD_OPT::VERBOSE);
    bool nzrv_restore = process::nzrv_exception;
    process::nzrv_exception = true;
    try {
        ntbs_lg line;
        if (verbose) {
            path cwd;
            cwd.read_cwd();
            fprintf(log, "Add git files from: %s\n", cwd.get_ccpath());
        }
        process git("git", "ls-files", PIPE::LG);
        for (git.start(); git.is_running() || !git.rb_out.empty();) {
            while (git.rb_out.read_line(line, true, false)) {
                if (line.find(".cpp") != SIZE_MAX)
                    sources.add(path(line));
            }
        }
        if (verbose)
            fprintf(log, "Found %ld sources.\n", sources.size());
    } catch (const c4s_exception& ce) {
        if (log)
            fprintf(log,"Unable to read source list from git: %s\n", ce.what());
    }
    process::nzrv_exception = nzrv_restore;
}
// -------------------------------------------------------------------------------------------------
void builder::set_build_dir(const ntbs& src)
{
    build_dir = src;
    if (build_dir.get(SIZE_MAX) != C4S_DSEP)
        build_dir += C4S_DSEP;
}

// -------------------------------------------------------------------------------------------------
void builder::add_comp(const char* arg)
{
    if (arg)
        c_opts << arg << ' ';
}
void builder::add_comp(const ntbs& arg)
{
    if (!arg.empty())
        c_opts << arg.get() << ' ';
}

// -------------------------------------------------------------------------------------------------
void builder::add_link(const char* arg)
{
    if (arg)
        l_opts << arg << ' ';
}
void builder::add_link(const ntbs& arg)
{
    if (!arg.empty())
        l_opts << arg.get() << ' ';
}
void builder::add_link(const compiled_file& cf)
{
    extra_obj.add(cf.target);
}
// -------------------------------------------------------------------------------------------------
bool builder::check_includes(path& object, path& source, ntbs& offender, int rlevel)
{
    const char *srcpath = source.get_ccpath();
    FILE *sf = fopen(srcpath, "r");
    if (!sf) {
        if (log)
            fprintf(log, "Warning: Outdate check - Unable to find source file: %s\n", srcpath);
        return true;
    }

    char *end, line[250];
    int count = 0;
    while (count < 80 && !feof(sf)) {
        // Read next line from source code
        fgets(line, sizeof(line), sf);
        count++;
        // Check for start of first function
        if (line[0] == '{')
            break;
        if (strncmp("#include \"", line, 10))
            continue;
        end = strchr(line + 10, '\"');
        if (!end)
            continue;
        // Make path to include file
        *end = 0;
        path inc_path(source);
        inc_path += line + 10;
        // Compare file times with original source
        try {
            if (object.compare_times(inc_path) < 0) {
                fclose(sf);
                offender = inc_path.get_base();
                return true;
            }
        } catch (const c4s_exception& ce) {
            if (log)
                fprintf(log, "Warning: %s\n", ce.what());
            continue;
        }
        if (rlevel < C4S_BUILD_RECURSIVE_CHECK_MAX &&
            check_includes(object, inc_path, offender, rlevel + 1))
        {
            return true;
        }
    }
    fclose(sf);
    return false;
}
// -------------------------------------------------------------------------------------------------
BUILD_STATUS builder::compile(const char* out_ext, const char* out_arg)
{
    list<path>::iterator src;
    ntbs_xl options;
    ntbs_sm offender;
    bool exec = false;
    bool verbose = log && has_any(BUILD_OPT::VERBOSE);
    path current_obj;
    bool compile;

    if (!sources.size())
        throw c4s_exception("builder::compile - sources not defined!");
    if (build_dir.empty())
        throw c4s_exception("builder::compile - empty build directory not allowed.");
    path bdp(build_dir);
    if (!bdp.dirname_exists())
        bdp.mkdir();

    try {
        if (verbose) {
            fprintf(log, "Considering %ld source files for build.\n", sources.size());
            if(!has_any(BUILD_OPT::NOINCLUDES))
                fputs("Include files are checked!\n\n", log);
        }
        for (src = sources.begin(); src != sources.end(); src++) {
            current_obj.set(build_dir.get(), src->get_base_plain(), out_ext);
            if (current_obj.exists()) {
                if (src->compare_times(current_obj) > 0) {
                    if (verbose)
                        fprintf(log, "== %s (old target)\n", src->get_ccbase());
                    compile = true;
                } else if (!has_any(BUILD_OPT::NOINCLUDES)) {
                    if (check_includes(current_obj, *src, offender)) {
                        if (verbose)
                            fprintf(log, "== %s (changed include:%s)\n", src->get_ccbase(), offender.get());
                        compile = true;
                    } else
                        compile = false;
                } else
                    compile = false;
            } else {
                if (verbose)
                    fprintf(log, "== %s (no target)\n", src->get_ccbase());
                compile = true;
            }
            if (compile) {
                if (log && !has_any(BUILD_OPT::VERBOSE))
                    fprintf(log, "%s\n", src->get_ccbase());
                options = c_opts.str();
                options += ' ';
                options += out_arg;
                options += current_obj.get_ccpath();
                options += ' ';
                options += src->get_ccpath();
                if (log) {
                    if (has_any(BUILD_OPT::VERBOSE)) {
                        fputs(options.get(), log);
                        fputc('\n', log);
                    }
                    for (compiler.start(options.get()); compiler.is_running();) {
                        compiler.rb_err.read_into(log);
                    }
                } else {
                    compiler(options.get());
                }
                exec = true;
                if (compiler.last_return_value())
                    return BUILD_STATUS::ERROR;
            }
        }
        current_obj.clear();
        if (!exec) {
            path fullbin(build_dir, target);
            if (!fullbin.exists()) {
                if (verbose)
                    fputs("Missing target binary. Linking...\n", log);
                return BUILD_STATUS::OK;
            }
            if (has_any(BUILD_OPT::FORCELINK)) {
                if (verbose)
                    fputs("No outdated source files found but forcing link step.\n", log);
                return BUILD_STATUS::OK;
            }
            if (verbose)
                fputs("No outdated source files found.\n", log);
            return BUILD_STATUS::NOTHING_TO_DO;
        }
    } catch (const c4s_exception& ex) {
        if (log)
            fprintf(log, "builder::compile error: %s\n", ex.what());
        return BUILD_STATUS::ERROR;
    }
    if(verbose)
        fputc('\n', log);
    return compiler.last_return_value() ? BUILD_STATUS::ERROR : BUILD_STATUS::OK;
}
// -------------------------------------------------------------------------------------------------
BUILD_STATUS builder::link(const char* out_ext, const char* out_arg)
{
    ntbs_md respname;
    BUILD_STATUS bs;
    ostringstream options;
    if (!sources.size())
        throw c4s_exception("builder::link - sources not defined!");
    if (!out_ext)
        throw c4s_exception("builder::link - link file extenstion missing. Unable to link.");
    if (build_dir.empty())
        throw c4s_exception("builder::compile - empty build directory not allowed.");
    if (build_dir.get_last() != '/')
        build_dir += '/';

    path_list linkFiles(sources, build_dir.get(), out_ext);
    try {
        if (log && has_any(BUILD_OPT::VERBOSE)) {
            if (type == BUILD_TYPE::LIB)
                fprintf(log, "Making %s library\n", target.get());
            else
                fprintf(log, "Linking %s\n", target.get());
        }
        if (type == BUILD_TYPE::LIB) {
            options << ' ' << l_opts.str() << ' ';
        }
        if (out_arg)
            options << out_arg;
        options << build_dir.get() << target << ' ';
        if (has_any(BUILD_OPT::RESPFILE)) {
            respname = name;
            respname += ".resp";
            if (log && has_any(BUILD_OPT::VERBOSE))
                fprintf(log, "builder::link - using response file: %s\n", respname.get());
            ofstream respf(respname.get());
            if (!respf)
                throw c4s_exception("builder::link - Unable to open linker response file.\n");
            respf << linkFiles.str('\n', false);
            if (extra_obj.size())
                respf << extra_obj.str('\n', false);
            respf.close();
            options << '@' << respname;
        } else {
            options << linkFiles.str(' ', false);
            if (extra_obj.size())
                options << ' ' << extra_obj.str(' ', false);
        }
        if (type != BUILD_TYPE::LIB)
            options << ' ' << l_opts.str();
        if (log && has_any(BUILD_OPT::VERBOSE))
            fprintf(log, "Link options: %s\n", options.str().c_str());

        int rv = 0;
        if (log) {
            linker.set_args(options.str());
            for (linker.start(); linker.is_running();) {
                linker.rb_err.read_into(log);
            }
            rv = linker.last_return_value();
        } else
            rv = linker(options.str());
        bs = rv ? BUILD_STATUS::ERROR : BUILD_STATUS::OK;
    } catch (const c4s_exception& ce) {
        if (log)
            fprintf(log, "builder::link error: %s\n", ce.what());
        return BUILD_STATUS::ERROR;
    }
#ifdef NDEBUG
    if (has_any(BUILD_OPT::RESPFILE))
        path("cpp4scripts_link.resp").rm();
#endif
    return bs;
}
// -------------------------------------------------------------------------------------------------
/**
   \param out Reference to output stream.
   \param list_sources If true, lists source files as well.
 */
void builder::print(FILE* out, bool list_sources)
{
    using namespace std;
    fprintf(out, "Compiler: %s\n", compiler.get_command().get_ccpath());
    fprintf(out, "   options: %s\n", c_opts.str().c_str());
    fprintf(out, "Linker: %s\n", linker.get_command().get_ccpath());
    fprintf(out, "   options: %s\n", l_opts.str().c_str());

    if (list_sources) {
        fputs("Source files:\n", out);
        list<path>::iterator src;
        for (src = sources.begin(); src != sources.end(); src++) {
            fprintf(out, "    %s\n", src->get_ccbase());
        }
    }
}
// -------------------------------------------------------------------------------------------------
/** Function opens the named file and increments the last number seen in the file. No special tags
    are needed. File is expected to be very short, just a variable declaration with version
    string.
    \param filename Relative path to the version file.
 */
int builder::update_build_no(const char* filename)
{
    char *vbuffer, *tail, *head, *dummy, bno_str[24];
    ifstream fbn(filename);
    if (!fbn) {
        printf("builder::update_build_no - Unable to open %s for reading.", filename);
        return -1;
    }
    fbn.seekg(0, ios_base::end);
    size_t max = (size_t)fbn.tellg();
    if (max > 255) {
        // cout << "builder::update_build_no - Build number file is too large to handle.\n";
        fbn.close();
        return -2;
    }
    vbuffer = new char[max];
    fbn.seekg(0, ios_base::beg);
    fbn.read(vbuffer, max);
    tail = vbuffer + max - 1;
    fbn.close();

    // Search the last number.
    while (tail > vbuffer) {
        if (*tail >= '0' && *tail <= '9')
            break;
        tail--;
    }
    if (tail == vbuffer) {
        // cout << "builder::update_build_no - Number not found from build number file.\n";
        delete[] vbuffer;
        fbn.close();
        return -3;
    }
    head = tail++;
    while (head > vbuffer) {
        if (*head < '0' || *head > '9')
            break;
        head--;
    }
    if (head > vbuffer)
        head++;
    // Get and update number.
    long bno = strtol(head, &dummy, 10);
    int bno_len = snprintf(bno_str, sizeof(bno_str), "%ld", bno + 1);

    // Rewrite the number file.
    ofstream obn(filename, ios_base::out | ios_base::trunc);
    if (head > vbuffer)
        obn.write(vbuffer, head - vbuffer);
    obn.write(bno_str, bno_len);
    obn.write(tail, max - (tail - vbuffer));
    obn.close();
    delete[] vbuffer;
    return 0;
}
// -----------------------------------------------------------------------------------------------------------------
void builder::export_prj(const ntbs& type, path& exp, const path& ccd)
{
    path expdir(exp);
    expdir.append_dir("export");
    if (!expdir.dirname_exists())
        expdir.mkdir();
    cout << "Exporting project into:" << expdir.get_path() << "\n";
    if (type == "cmake")
        export_cmake(expdir);
    else if (type == "ccdb")
        export_compiler_commands(expdir, ccd);
    else
        cout << "Unknown export type.\n";
}
// -----------------------------------------------------------------------------------------------------------------
/// \todo Move export_compiler_commands function to compiler specific code.
void builder::export_compiler_commands(path& exp, const path& ccd)
{
    list<path>::iterator src;
    ostringstream options;
    bool first_ccdb = true;

    if (!sources.size()) {
        cout << "builder::export - sources not defined!\n";
        return;
    }
    exp.set_base("compile_commands.json");
    ofstream cc_db(exp.get_ccpath());
    if (!cc_db) {
        throw c4s_exception("builder::compile - unable to create compile_commands.json");
    }
    cc_db << "[\n";

    for (src = sources.begin(); src != sources.end(); src++) {
        path objfile(build_dir.get(), src->get_base_plain(), ".o");
        cout << src->get_base() << " >>\n";
        options.str("");
        options << c_opts.str();
        options << " -o " << objfile.get_path();
        options << ' ' << src->get_base();

        path srcdir(ccd);
        srcdir.merge(*src);

        if (first_ccdb)
            first_ccdb = false;
        else
            cc_db << ",\n";
        cc_db << "{\n";
        cc_db << "\"directory\":\"" << srcdir.get_dir_plain() << "\",\n";
        cc_db << "\"command\":\"" << compiler.get_command().get_path() << ' ' << options.str()
              << "\",\n";
        cc_db << "\"file\":\"" << src->get_base() << "\"\n}";
    }
    cc_db << "\n]\n";
    cc_db.close();
    cout << "Done.\n";
}
// -----------------------------------------------------------------------------------------------------------------
void builder::export_cmake(path& dir)
{
    list<path>::iterator src;
    ostringstream options;
    bool first = true;

    if (!sources.size()) {
        cout << "builder::export - sources not defined!\n";
        return;
    }
    dir.set_base("CMakeLists.txt");
    ofstream cml(dir.get_ccpath());
    if (!cml) {
        cout << "builder::compile - unable to create CMakeLists.txt.\n";
        return;
    }
    cml << "project(" << name << ")\n";
    cml << "cmake_minimum_required(VERSION 3.19)\n\n";
    cml << "add_executable(" << name << '\n';
    for (src = sources.begin(); src != sources.end(); src++) {
        if (first)
            first = false;
        else
            cml << '\n';
        cml << src->get_path();
    }
    cml << ")\n\n";
    cml << "target_include_directories(fcgi PUBLIC /usr/local/include/cpp4scripts)\n";
    cml.close();
    cout << "Done.\n";
}

} // namespace c4s