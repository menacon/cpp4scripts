/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#include <regex>
#include <stdexcept>
#include <string.h>
#if defined(__linux) || defined(__APPLE__)
#include <dirent.h>
#include <sys/stat.h>
#endif
#include "ntbs.hpp"
#include "libconfig.hpp"
#include "exception.hpp"
#include "user.hpp"
#include "path.hpp"
#include "path_list.hpp"
#include "util.hpp"

#ifdef C4S_UNITTEST
#include <stdio.h>
extern FILE* utlog;
#endif

using namespace std;
using namespace c4s;

path_list::path_list(const path& target, const ntbs& grep, const ntbs& exex, int plo)
{
    add(target, grep, exex, plo);
}

// -------------------------------------------------------------------------------------------------
/*! Only the base names are used from the source list.  All files in this list will have the
  same directory given as the given 'directory' parameter, and the same extension also given as a
  parameter. Function can be used to transform list of source files to the list of object
  files.

   \param source List of source files
   \param directory Directory for all files in the list.
   \param ext Extension for all files in the list. Optional, defaults to null. If not specified
  source file's extension is used. \retval size_t Number of paths added.
*/
size_t c4s::path_list::add(const path_list& source, const ntbs& directory, const char* ext)
{
    size_t count = plist.size();
    list<path>::const_iterator pi;
    for (pi = source.plist.begin(); pi != source.plist.end(); pi++)
        plist.push_back(path(directory, pi->get_base(ext)));
    return plist.size() - count;
}

// -------------------------------------------------------------------------------------------------
/*!  Default separator is C4S_PSEP. Newline \n is always taken as a separator in addition to the
  given one. \param str Pointer to the string with list of filenames \param separator Path
  separation character \retval size_t Number of paths added.
*/
size_t c4s::path_list::add(const char* str, const char separator)
{
    const char* point = str;
    const char* prev = str;
    size_t count = plist.size();
    while (*point) {
        if (*point == separator || *point == '\n') {
            plist.push_back(path(ntbs(prev, point - prev, ntbs::ALLOC)));
            point++;
            while (*point && (*point == separator || *point == '\n')) {
                point++;
            }
            if (!*point)
                return plist.size() - count;
            prev = point;
        }
        point++;
    }
    if (point - prev > 1)
        plist.push_back(path(ntbs(prev)));
    return plist.size() - count;
}

// -------------------------------------------------------------------------------------------------
/*! \param source Source path list
    \retval size_t Number of itmes added.
*/
size_t c4s::path_list::add(const path_list& source)
{
    for (list<path>::const_iterator pi = source.plist.begin(); pi != source.plist.end(); pi++) {
        plist.push_back(*pi);
    }
    return source.plist.size();
}

// -------------------------------------------------------------------------------------------------
/*!
  \param target Path to the target directory. Only dir-part is considered.
  \param greap extended regular expression to match included files. If null includes all files.
  \param plf Combination of add options. See add-constants.
  \param exex extended regular expression of files to exclude.
  \returns size_t number of files that have been added.
*/
size_t c4s::path_list::add(const path& target, const ntbs& grep, const ntbs& exex, int plf)
{
    regex grep_rx, exclude_rx;
    ntbs fname;
    size_t original_size = plist.size();
    bool include;

#ifdef C4S_UNITTEST
    fprintf(utlog, "-- path_list::add target:'%s' options:0x%x", target.get_ccpath(), plf);
    if (!grep.empty())
        fprintf(utlog, " grep:'%s'", grep.get());
    if (!exex.empty())
        fprintf(utlog, " exex:'%s'", exex.get());
    fputc('\n', utlog);
#endif
    // Open and read the directory
    ntbs dir;
    if (target.get_dir().empty())
        dir = "./";
    else
        dir = target.get_dir();
    DIR* source_dir = opendir(dir.get());
    if (!source_dir) {
#ifdef C4S_UNITTEST
        fputs("-- path_list::add - Unable to access directory\n", utlog);
#endif
        throw rterror("path_list::add - Unable to access directory:%s\n%s",
                      dir.get(), strerror(errno));
    }
    // Initialize regular expressions
    try {
        if (!grep.empty())
            grep_rx.assign(grep.get(), regex_constants::extended);
        if (!exex.empty())
            exclude_rx.assign(exex.get(), regex_constants::extended);
    } catch (const regex_error& re) {
        throw rterror("path_list::add - regex error (check syntax): %s;", re.what());
    }
    // Apply regular expressions to each file in give directory
    struct stat file_stat;
    struct dirent* de = readdir(source_dir);
    while (de) {
        include = false;
        if (!grep.empty()) {
            try {
                if (regex_search(de->d_name, grep_rx)) {
                    if (exex.empty() || !regex_search(de->d_name, exclude_rx))
                        include = true;
                }
            } catch (const regex_error& re) {
                throw rterror("path_list::add - regex error: %s", re.what());
            }
        } else
            include = true;
        if (include) {
            // Check the file/name type.
            fname = target.get_dir();
            fname += de->d_name;
            if (!lstat(fname.get(), &file_stat)) {
                if ((plf & PLF_NOREG) == 0 && S_ISREG(file_stat.st_mode)) {
                    if ((plf & PLF_NOSEARCHDIR) == 0)
                        plist.push_back(path(target.get_dir(), ntbs(de->d_name)));
                    else
                        plist.push_back(path(ntbs(de->d_name)));
                } else if ((plf & PLF_SYML) > 0 && S_ISLNK(file_stat.st_mode)) {
                    if ((plf & PLF_NOSEARCHDIR) == 0)
                        plist.push_back(path(target.get_dir(), ntbs(de->d_name)));
                    else
                        plist.push_back(path(ntbs(de->d_name)));
                } else if ((plf & PLF_DIRS) > 0 && S_ISDIR(file_stat.st_mode) &&
                           de->d_name[0] != '.') {
                    ntbs dirname;
                    if ((plf & PLF_NOSEARCHDIR) == 0) {
                        dirname = target.get_dir();
                        dirname += de->d_name;
                    } else {
                        dirname = de->d_name;
                    }
                    dirname += "/";
                    plist.push_back(path(dirname));
                }
            }
        }
        de = readdir(source_dir);
    }
#ifdef C4S_UNITTEST
    fprintf(utlog, "..path_list::add found items:%ld\n", plist.size() - original_size);
#endif
    return plist.size() - original_size;
}
// -------------------------------------------------------------------------------------------------
/*! Adds files recursively in recursive function.
    NOTE! Recurses only MAX_NESTING levels deep.
 */
size_t c4s::path_list::add_recursive(const path& p, const char* grep)
{
    static int nesting_level = 0;
    size_t count = 0;
#ifdef C4S_UNITTEST
    fprintf(utlog, "path_list::add_recursive - level=%d; %s\n", nesting_level, p.get_ccpath());
#endif
    if (nesting_level > MAX_NESTING)
        return 0;
    // First add the target directory itself
    add(p, ntbs(grep), ntbs());
    // Search subdirs
    path_list tmp(p, ntbs(), ntbs(), PLF_DIRS | PLF_NOREG);
    for (path_iterator pi = tmp.begin(); pi != tmp.end(); pi++) {
        nesting_level++;
        // Call recursively
        count += add_recursive(*pi, grep);
        nesting_level--;
    }
    return count;
}
// -------------------------------------------------------------------------------------------------
/*! Affects the list only, actual file is not removed from the disk.
   \retval bool True on succes, false on error.
*/
bool c4s::path_list::discard_matching(const ntbs& tbase)
{
    for (list<path>::iterator pi = plist.begin(); pi != plist.end(); pi++) {
        if (tbase == pi->base) {
            plist.erase(pi);
            return true;
        }
    }
    return false;
}

// -------------------------------------------------------------------------------------------------
/*! Targets base is ignored if it exists => path::ONAME flag is enforced. If path list
  contains directories and flags has RECURSIVE bit set then these directories are copied
  recursively. If the flag is missing then directories are disregarded.  In case of error this
  function will leave the copy partially done. Only existing files are copied.
  \param target path to target directory
  \param flags copy flags.
  \retval int Number of files copied.
*/
int c4s::path_list::copy_to(const path& target, int flags)
{
    int copy_count = 0;
    list<path>::iterator pi;
    flags |= PCF_ONAME;
    for (pi = plist.begin(); pi != plist.end(); pi++) {
        if (!pi->is_base()) {
            if ((flags & PCF_RECURSIVE) > 0) {
                path tmp_target(target);
                tmp_target.append_last(*pi);
                copy_count += pi->copy_recursive(tmp_target, flags);
            }
        } else if (pi->exists())
            copy_count += pi->cp(target, flags);
    }
#ifdef C4S_UNITTEST
    fprintf(utlog, "path_list::copy to %s copied %d\n", target.get_ccdir(), copy_count);
#endif
    return copy_count;
}

// -------------------------------------------------------------------------------------------------
/*!  \param mod Mode to set. \see path::chmod
 */
void c4s::path_list::chmod(int mod)
{
    list<path>::iterator pi;
    for (pi = plist.begin(); pi != plist.end(); pi++)
        pi->chmod(mod);
}

// -------------------------------------------------------------------------------------------------
/*! \param dir Directory to set.
 */
void c4s::path_list::set_dir(const ntbs& dir)
{
    list<path>::iterator pi;
    for (pi = plist.begin(); pi != plist.end(); pi++)
        pi->set_dir(dir);
}
// -------------------------------------------------------------------------------------------------
/*! \param ext New extension to set. Should include the period before the extension as well.
 */
void c4s::path_list::set_ext(const ntbs& ext)
{
    list<path>::iterator pi;
    for (pi = plist.begin(); pi != plist.end(); pi++)
        pi->set_ext(ext);
}
#if defined(__linux) || defined(__APPLE__)
// -------------------------------------------------------------------------------------------------
/*! \param uptr Pointer to the owner of the paths
    \param mode Mode that should be used for the paths.
*/
void c4s::path_list::set_usermode(user* uptr, const int mode)
{
    list<path>::iterator pi;
    for (pi = plist.begin(); pi != plist.end(); pi++)
        pi->ch_owner_mode(uptr, mode);
}
#endif
// -------------------------------------------------------------------------------------------------
/*!  If there are plain directories (i.e. no base defined) then the directory is removed
 * recursively. USE WITH CARE!!!
 */
void c4s::path_list::rm_all()
{
    for (list<path>::iterator pi = plist.begin(); pi != plist.end(); pi++) {
        if (pi->base.empty())
            pi->rmdir(true);
        else if (!pi->exists())
            continue;
        else if (!pi->rm()) {
            ostringstream os;
            os << "path_list::remove_all - unable to delete " << pi->get_path();
            throw c4s_exception(os.str());
        }
    }
}

// -------------------------------------------------------------------------------------------------
/*! This function supposes that this object is a list of source files and makes corresponding
  target paths (for a compiler) by taking the given dir, base name from this list and appending a
  given extension. E.g. if this list has files with .cpp files and dir is './release' and ext is
  '.o' for each cpp file in the this list a path with given dir, base name of cpp and .o extenstion
  is appended to target list.
 */
void c4s::path_list::create_targets(path_list& target, const ntbs& dir, const char* ext)
{
    list<path>::iterator pi;
    for (pi = plist.begin(); pi != plist.end(); pi++)
        target += path(dir, pi->get_base(ext));
}

// -------------------------------------------------------------------------------------------------
/*! \param separator Separator for paths
   \param baseonly If true (default) returns the base names only.
   \retval string a list of paths.
*/
ntbs c4s::path_list::str(const char separator, bool baseonly)
{
    ntbs bunch;
    list<path>::iterator pi = plist.begin();
    if (pi == plist.end())
        return bunch;
    bunch = baseonly ? pi->get_base_or_dir() : pi->get_path();
    pi++;
    while (pi != plist.end()) {
        bunch += separator;
        bunch += baseonly ? pi->get_base_or_dir() : pi->get_path();
        pi++;
    }
    return bunch;
}
// -------------------------------------------------------------------------------------------------
bool compare_bases(c4s::path& fp, c4s::path& sp)
{
    return fp.compare(sp, CMP_BASE) < 0 ? true : false;
}
bool compare_full(c4s::path& fp, c4s::path& sp)
{
    return fp.compare(sp, CMP_DIR | CMP_BASE) < 0 ? true : false;
}
void c4s::path_list::sort(SORTTYPE st)
{
    if (st == ST_FULL)
        plist.sort(::compare_full);
    else
        plist.sort(::compare_bases);
}

// -------------------------------------------------------------------------------------------------
void c4s::path_list::dump(ostream& out)
{
    list<path>::iterator pi;
    for (pi = plist.begin(); pi != plist.end(); pi++)
        pi->dump(out);
}
