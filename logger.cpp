/* This file is part of 'CPP for Scripts' C++ library (libc4s)
 * https://gitlab.com/menacon/cpp4scripts
 *
 * Copyright (c) Menacon Oy
 * License: http://www.gnu.org/licenses/lgpl-2.1.html
 * Disclaimer of Warranty: Work is provided on an "as is" basis, without warranties or conditions of
 * any kind
 */
#include <stdio.h>
#include <stdarg.h>
#include <syslog.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

#include "libconfig.hpp"
#include "ntbs.hpp"
#include "exception.hpp"
#include "user.hpp"
#include "path.hpp"
#include "logger.hpp"
#include "tm_datetime.hpp"

using namespace std;

namespace c4s {

const char* g_level_names[LL_MAX] = { "NONE",   "TRACE",   "DEBUG", "INFO",
                                      "NOTICE", "WARNING", "ERROR", "CRITICAL" };

logger* logger::thelog = 0;

// -------------------------------------------------------------------------------------------------
const char* log_sink::get_datetime(LOG_LEVEL ll)
{
    static char datetime[64]; // 2013-01-01 23:30:30 [CRITICAL]
    time_t now = time(0);
    struct tm* lt = localtime(&now);
    snprintf(datetime,
             sizeof(datetime),
             "%d-%02d-%02d %02d:%02d:%02d [%-8s] ",
             lt->tm_year + 1900,
             lt->tm_mon + 1,
             lt->tm_mday,
             lt->tm_hour,
             lt->tm_min,
             lt->tm_sec,
             g_level_names[ll]);
    return datetime;
}

// -------------------------------------------------------------------------------------------------
void logger::init_log(LOG_LEVEL ll, log_sink* sk) // static
{
    if (!thelog)
        thelog = new logger(ll, sk);
    else
        syslog(LOG_MAKEPRI(LOG_USER, LOG_WARNING), "Cpp4Scripts logger already initialized.");
}

// -------------------------------------------------------------------------------------------------
void logger::vaprt(LOG_LEVEL ll, const char* str, ...)
{
    static char vabuffer[C4S_LOG_VABUFFER_SIZE];
    va_list va;
    if (ll != LL_NONE && ll >= level) {
        va_start(va, str);
        vsnprintf(vabuffer, sizeof(vabuffer) - 1, str, va);
        va_end(va);
        sink->print(ll, vabuffer);
    }
}

// -------------------------------------------------------------------------------------------------
syslog_sink::syslog_sink(const char* name, const c4s::ntbs& facility)
{

    levelmap[0] = 0;
    levelmap[1] = 0;
    levelmap[2] = LOG_DEBUG;
    levelmap[3] = LOG_INFO;
    levelmap[4] = LOG_NOTICE;
    levelmap[5] = LOG_WARNING;
    levelmap[6] = LOG_ERR;
    levelmap[7] = LOG_CRIT;

    int facid = LOG_USER;
    int fac_mapping[8] = { LOG_LOCAL0, LOG_LOCAL1, LOG_LOCAL2, LOG_LOCAL3,
                           LOG_LOCAL4, LOG_LOCAL5, LOG_LOCAL6, LOG_LOCAL7};
    if (facility != "user") {
        facid = LOG_LOCAL5;
        if (facility.substr(0, 5) == "local") {
            int val = facility.get(5) - '0';
            if (val >= 0 && val <= 7)
                facid = fac_mapping[val];
        }
    }
    openlog(name, LOG_NDELAY, facid);
}

syslog_sink::~syslog_sink()
{
    closelog();
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
#pragma GCC diagnostic ignored "-Wformat-security"
void syslog_sink::print(LOG_LEVEL ll, const char* str)
{
    int slfac = levelmap[(int)ll];
    if (slfac)
        syslog(slfac, str);
}
#pragma GCC diagnostic pop

// -------------------------------------------------------------------------------------------------
file_sink::file_sink(const path& logp)
{
    bool append = false;
    if (logp.exists()) {
        log_file = fopen(logp.get_ccpath(), "a");
        append = true;
    } else
        log_file = fopen(logp.get_ccpath(), "w");
    if (!log_file)
        throw c4s_exception("file_sink::file_sink - unable to open given file for logging.");
    if (append)
        fprintf(log_file,
                "\n---------------------------------------------------------------------\n");
    tm_datetime ls;
    ls.now();
    fprintf(log_file, "=== Log started: %s ===\n", ls.printISOTimestamp());
    fflush(log_file);
}
file_sink::~file_sink()
{
    fclose(log_file);
}
void file_sink::print(LOG_LEVEL ll, const char* str)
{
    fprintf(log_file, "%s ", get_datetime(ll));
    fputs(str, log_file);
    fputc('\n', log_file);
    if (ll == LL_TRACE)
        fflush(log_file);
}
// -------------------------------------------------------------------------------------------------
void stdout_sink::print(LOG_LEVEL ll, const char* str)
{
    fputs(get_datetime(ll), stdout);
    fputs(str, stdout);
    fputc('\n', stdout);
}
// -------------------------------------------------------------------------------------------------
LOG_LEVEL logger::str2level(const ntbs& name)
{
    const int MAX = 8;
    LOG_LEVEL level_value[MAX] = { LL_NONE,   LL_TRACE,   LL_DEBUG, LL_INFO,
                                   LL_NOTICE, LL_WARNING, LL_ERROR, LL_CRITICAL };
    const char* nptr = name.get();
    for (int ndx = 0; ndx < MAX; ndx++) {
        if (!strcasecmp(nptr, g_level_names[ndx]))
            return level_value[ndx];
    }
    throw rterror("logger::str2level - Unknown log level string:%s", name.get());
}

} // namespace c4s